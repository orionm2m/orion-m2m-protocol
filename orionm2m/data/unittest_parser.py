from orionm2m.data.decoder import Decoder
import json


class TestDecoder:
    PAYLOAD_FORMATS = {
        0: 'Regular',
        1: 'ArchiveOld',
        2: 'Archive',
        3: 'ArchiveOld',
        4: 'Archive',
        5: 'ArchiveOld',
        6: 'Archive',
        7: 'Tamper',
        8: 'Service',
        19: 'HeatMeter',
        16: 'Lighting',
        17: 'Electricity',
        20: 'HeatMeterExtended',
        10: 'DiEvents',
        18: 'Sensors',
        32: 'Modbus',
        254: 'Various'
    }

    def __init__(self):
        filepath = 'test_packets'
        payload_ids = self.PAYLOAD_FORMATS.copy()
        payload_ids = {i: {'name': payload_ids[i], 'cnt': 0} for i in payload_ids}
        with open(filepath) as fp:
            line = fp.readline()
            cnt = 0
            success = 0
            exception_cnt = 0
            exception = payload_ids.copy()
            fail = 0
            while line:
                try:
                    param = line.strip()
                    payload_id = int(param[6:8], 16)
                    output_new = Decoder(param,'en')
                    out = output_new.final_output()
                    print(out)
                    if "error" in out:
                        exception_cnt += 1
                        exception[payload_id]['cnt'] += 1
                    else:
                        success += 1
                        payload_ids[payload_id]['cnt'] += 1
                except Exception as e:
                    print('exception', line.strip(), repr(e))
                    fail += 1
                finally:
                    cnt += 1

                line = fp.readline()
        print('from {} tested payloads:\nsuccess number:{},fail number:{},exceptions:{}'.format(cnt, success, fail,
                                                                                                exception_cnt))
        print('success payloads:')
        print(json.dumps({payload_ids[i]['name']: payload_ids[i]['cnt'] for i in payload_ids}, indent=4))
        if exception_cnt > 0:
            print('exception payloads:')
            print(json.dumps({exception[i]['name']: exception[i]['cnt'] for i in exception}, indent=4))


t = TestDecoder()
