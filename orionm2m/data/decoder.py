from __future__ import print_function, division
import binascii
import json
import struct
from datetime import datetime
from math import ceil
import sys
import io
import pytz
import os
import base64

if (sys.version_info < (3, 0)):
    reload(sys)
    sys.setdefaultencoding('utf-8')


class InvalidInputFormatError(Exception):
    pass


class ArchiveDecoderError(Exception):
    pass


class InvalidPayloadLengthError(Exception):
    pass


class ObsoleteFormatError(Exception):
    pass


class NotExistingParamError(Exception):
    pass


class BaseDecoder(object):
    default_time_offset = 946684800

    def __init__(self, payload, lang=None, payload_id=None):
        self.pos_offset = 0
        self.payload = payload
        self.lang = lang
        self.payload_id = payload_id

    def timestamp_to_utc(self, timestamp):
        return datetime.fromtimestamp(timestamp, pytz.utc).strftime("%Y-%m-%d %H:%M:%S")

    def unpack_bytes(self, payload, info, sign=0, integer24_trick=False):
        if len(payload[info['pos']: info['pos'] + info['size']]) == info['size']:
            if integer24_trick == False:
                return struct.unpack(info['format'],  payload[info['pos'] : info['pos'] + info['size']])
            input_data = payload[info['pos'] : info['pos'] + info['size']]
            if sign == 0:
                return struct.unpack(info['format'],  input_data + b'\0')
            else:
                return struct.unpack(info['format'],  input_data + (b'\0' if input_data[2] < 128 else b'\xff'))
        raise InvalidPayloadLengthError('Invalid payload length')

    def get_timestamp(self, payload, info):
        timestamp = self.unpack_bytes(self.payload, info)[0]
        return self.timestamp_to_utc(timestamp + self.default_time_offset)

    def unpack_bits(self, data, info):
        return (data & info['mask'] << info['pos']) >> info['pos']

    def unpack_payload(self):
        # For override
        pass


class Decoder(BaseDecoder):
    ID = {'size': 1, 'pos': 0, 'format': 'B'}
    SEQUENCE = {'size': 1, 'pos': 1, 'format': 'B'}
    FLAGS = {'size': 1, 'pos': 2, 'format': 'B'}
    PAYLOAD_FORMATS = {
        0: 'Regular',
        1: 'ArchiveOld',
        2: 'Archive',
        3: 'ArchiveOld',
        4: 'Archive',
        5: 'ArchiveOld',
        6: 'Archive',
        7: 'Tamper',
        8: 'Service',
        19: 'HeatMeter',
        16: 'Lighting',
        17: 'Electricity',
        20: 'HeatMeterExtended',
        10: 'DiEvents',
        18: 'Sensors',
        32: 'Modbus',
        21: 'MeterEvents',
        22: 'Tariffication',
        254: 'Various'
    }

    def __init__(self, payload, lang=None, payload_id=None):
        super(Decoder, self).__init__(payload, lang, payload_id)
        self.res = {}
        self.res_without_trans = {}
        if payload[0:2] != '78':
            raise InvalidInputFormatError('Not valid format of input packet')
        self.raw_data = payload
        try:
            self.byte_data = bytearray.fromhex(self.raw_data)
        except ValueError:
            raise InvalidInputFormatError('Not valid format of input packet - non-hex number found')
        self.id = self.unpack_bytes(self.byte_data, self.ID)[0]
        self.sequence = self.unpack_bytes(self.byte_data, self.SEQUENCE)[0]
        self.description_trans = {}
        self.payload = self.byte_data[3:]
        self.TRANS_LANG = {}
        self.description_trans = {}
        if lang:
            with io.open(
                    os.path.join(os.path.dirname(os.path.abspath(__file__)), 'lang',
                                 'data-{}.json'.format(self.lang)).replace('\\', '/'),
                    encoding='utf-8', errors='ignore') as json_file:
                self.TRANS_LANG = json.load(json_file)
                self.description_trans['0'] = self.TRANS_LANG['0']

    def unpack_flags(self):
        flags = self.unpack_bytes(self.byte_data, self.FLAGS)[0]
        flags_list = [i for i, bit in enumerate(bin(flags)[2:][::-1]) if bit == '1']

        flags_dict = {'1.{}'.format(i): 1 for i in flags_list}
        for i in flags_dict:
            if i in self.TRANS_LANG:
                self.description_trans[i] = self.TRANS_LANG[i]
        return flags_dict

    def _decoder_class_by_name(self, classname):
        return globals()[classname]

    def _unpack_payload(self):
        payload_id = self.unpack_bytes(self.payload, self.ID)[0]

        output = {}
        payload_data = self.payload[1:]
        decoder_classname = self.PAYLOAD_FORMATS[payload_id]
        decoder = self._decoder_class_by_name(decoder_classname)(payload_data, self.lang, payload_id)
        output.update(decoder.unpack_payload())

        offset = decoder.pos_offset
        self.payload = payload_data[int(offset):]
        return output

    def unpack_payloads(self):
        payloads_datas = []
        while self.payload:
            payloads_datas.append(self._unpack_payload())

        return payloads_datas

    def decode(self):
        try:
            result = {}
            result.update(self.unpack_flags())
            result.update({'0': self.sequence})
            result.update({'payloads': self.unpack_payloads()})


        # except KeyError:
        #     return {"error": 'Unknown payload id'}
        except ArchiveDecoderError as e:
            return {"error": str(e)}
        except InvalidPayloadLengthError as e:
            return {"error": str(e)}
        except ObsoleteFormatError as e:
            return {"error": str(e)}
        except NotExistingParamError as e:
            return {"error": str(e)}
        return result

    def __str__(self):

        # self.description_trans = {}

        return self.final_output()

    def insert_values_to_descr(self, descr, i_value=None, N_value=None, K_value=None):
        if i_value and '#i' in descr:
            descr = descr.replace('#i', '#{}'.format(i_value))
        if N_value and '#N' in descr:
            descr = descr.replace('#N', '#{}'.format(N_value))
        if K_value and '#K' in descr:
            descr = descr.replace('#K', '#{}'.format(K_value))
        return descr

    def get_descr_of_address(self, addr, addr_raw, value):
        splitted_addr = addr.split('.')
        indexed_addr = addr.split('.')
        i_value = splitted_addr[3]
        indexed_addr[3] = 'i'
        # Р РђРЎРЎРўРђР’РР›Р РРќР”Р•РљРЎР« Р Р’Р«Р’Р•Р›Р Р’РЎР• РЎРўРЈРџР•РќР
        for j in range(len(splitted_addr) - 1):
            index_to_cut = j if j != 0 else None
            if '.'.join(indexed_addr[:index_to_cut]) in self.TRANS_LANG:
                translation=self.insert_values_to_descr(
                    self.TRANS_LANG['.'.join(indexed_addr[:index_to_cut])], i_value=i_value)
                if '{}' not in translation:
                    self.description_trans['.'.join(splitted_addr[:index_to_cut])] = translation
        # 255 РѕР·РЅР°С‡Р°РµС‚ РєРѕР»РёС‡РµСЃС‚РІРѕ СЃРѕР±С‹С‚РёР№ РёР»Рё РёРІРµРЅС‚РѕРІ
        if splitted_addr[-1] == '255':
            self.description_trans[addr] = self.insert_values_to_descr(self.TRANS_LANG['.'.join(indexed_addr)],
                                                                       i_value=i_value)
        # РµСЃР»Рё РІСЃРµ РµС‰Рµ РЅРµ РЅР°С€Р»Рё РЅСѓР¶РЅС‹Р№ Р°РґСЂРµСЃ, Р·РЅР°С‡РёС‚ Р»РёР±Рѕ СЌС‚Рѕ СЌР»РµРєС‚СЂРёС‡РµСЃС‚РІРѕ, Р»РёР±Рѕ С‚Р°Рј РµСЃС‚СЊ N РёР»Рё Рљ
        if addr not in self.description_trans:
            # РџР°СЂР°РјРµС‚СЂС‹ СЌР»РµРєС‚СЂРёС‡РµСЃС‚РІР°
            if splitted_addr[1] == '20' and len(splitted_addr) == 9:
                channel_num = self.TRANS_LANG['heatmeter_extended_channel_num'][splitted_addr[5]]
                param_type = self.TRANS_LANG["heatmeter_extended_param_types"][splitted_addr[6]]
                archive_time = self.TRANS_LANG['heatmeter_extended_archive_times'][splitted_addr[7]] 
                data_types = self.TRANS_LANG['heatmeter_extended_data_types'][splitted_addr[8]]
                if splitted_addr[8] != '0': # BX-33815
                    temp_descr = self.TRANS_LANG["2.20.1.i.2.channel_num.param_types.archive_times.data_types"]
                    temp_descr = temp_descr.format(param_type,channel_num,archive_time,data_types)
                else:
                    temp_descr = self.TRANS_LANG["2.20.1.i.2.channel_num.param_types.data_types"]
                    temp_descr = temp_descr.format(param_type,channel_num,data_types)
                self.description_trans[addr] = self.insert_values_to_descr(temp_descr, i_value=splitted_addr[3])

            elif splitted_addr[1] == '17':
                param_id = splitted_addr[5]
                param_descr = self.TRANS_LANG['param_id'][param_id]
                if param_id == '1':
                    virt_input = splitted_addr[6]
                    temp_descr = self.TRANS_LANG["2.17.1.i.2.1.{}".format(virt_input)]
                    value_descr=self.TRANS_LANG['condition_virtual_input'][str(value)]
                    descr = temp_descr.format(value_descr)
                    self.description_trans[addr] = descr
                elif param_id not in ['0', '2']:
                    if len(splitted_addr) != 8:
                        temp_descr = self.TRANS_LANG['2.17.1.i.2.param_id.p_t']
                    else:
                        temp_descr = self.TRANS_LANG['2.17.1.i.2.param_id.p_t.k']
                    phase_tariff_id = splitted_addr[6]
                    phase_tariff_descr = self.TRANS_LANG['phase_id'][phase_tariff_id]
                    if len(splitted_addr) != 8:
                        self.description_trans[addr] = temp_descr.format(param_descr,
                                                                         phase_tariff_descr
                                                                         )
                    else:
                        self.description_trans[addr] = self.insert_values_to_descr(
                            temp_descr.format(param_descr, phase_tariff_descr),
                            K_value=splitted_addr[-1])
                else:
                    if len(splitted_addr) != 10:
                        temp_descr = self.TRANS_LANG['2.17.1.i.2.param_id.reverse.character_id.p_t']
                    else:
                        temp_descr = self.TRANS_LANG['2.17.1.i.2.param_id.reverse.character_id.p_t.k']
                    character_id = splitted_addr[7]
                    reverse_id = splitted_addr[6]
                    phase_tariff_id = splitted_addr[8]
                    if param_id == '2' or param_id == '8':
                        phase_tariff_descr = self.TRANS_LANG['tariff_id'][phase_tariff_id]
                    else:
                        phase_tariff_descr = self.TRANS_LANG['phase_id'][phase_tariff_id]
                    character_descr = self.TRANS_LANG['character_id'][character_id]
                    reverse_descr = self.TRANS_LANG['direction_id'][reverse_id]
                    if len(splitted_addr) != 10:
                        self.description_trans[addr] = temp_descr.format(param_descr, reverse_descr, character_descr,
                                                                         phase_tariff_descr)
                    else:
                        self.description_trans[addr] = self.insert_values_to_descr(
                            temp_descr.format(param_descr, reverse_descr, character_descr,
                                              phase_tariff_descr),
                            K_value=splitted_addr[-1])
            else:
                # РўР•РџР•Р Р¬ Р РђРЎРЎРўРђР’Р›РЇР•Рњ N
                indexed_addr[5] = 'N'

                indexed_addr_str = '.'.join(indexed_addr)
                # Р•СЃР»Рё РЅР°С€Р»Рё С‚Рѕ Р·Р°РїРёСЃС‹РІР°РµРј РµРіРѕ
                if indexed_addr_str in self.TRANS_LANG:
                    self.description_trans[addr] = self.insert_values_to_descr(self.TRANS_LANG[indexed_addr_str],
                                                                               i_value=i_value,
                                                                               N_value=splitted_addr[5])
                # РµСЃР»Рё РЅРµС‚, Р·РЅР°С‡РёС‚ С‚Р°Рј РµСЃС‚СЊ Рљ
                # РўР•РџР•Р Р¬ Р РђРЎРЎРўРђР’Р›РЇР•Рњ K
                else:
                    if len(splitted_addr) == 6:  # РµСЃР»Рё СЌС‚Рѕ di events 2.10.1.i.0.K (Р±РµР· N)
                        index_of_k = 5
                        n_value = None
                    else:  # РµСЃР»Рё СЌС‚Рѕ di events 2.10.1.i.2.N.1.K Р»Рё РјРѕРґР±Р°СЃ
                        index_of_k = 7
                        n_value = splitted_addr[5]
                    indexed_addr[index_of_k] = 'K'
                    indexed_addr_str = '.'.join(indexed_addr)
                    k_value = splitted_addr[index_of_k] if splitted_addr[1] == '32' else int(
                        splitted_addr[index_of_k]) + 1
                    if indexed_addr_str in self.TRANS_LANG:
                        self.description_trans[addr] = self.insert_values_to_descr(self.TRANS_LANG[indexed_addr_str],
                                                                                   i_value=i_value, N_value=n_value,
                                                                                   K_value=k_value)

    def set_sensor_coeffs(self, coeffs):

        for i in coeffs:
            param_addr = i[:-2]
            if param_addr in self.description_trans:
                self.description_trans[param_addr] = self.description_trans[param_addr].format(coeffs[i])

    def base64_output(self):
        _output = self.final_output()
        output = json.dumps(_output, indent=4, ensure_ascii=False).encode()
        return base64.b64encode(output).decode("utf-8")

    def final_output(self):
        res = self.decode()
        if 'error' in res:
            return {'error': res['error']}
        payloads = res.pop('payloads')
        # here we will count how many times we got each payload
        indexes = dict.fromkeys(self.PAYLOAD_FORMATS, 0)
        for payload in reversed(payloads):
            # get the first key from payload to analyze payload type
            payload_str = list(payload.keys())[0]
            payload_id = int(payload_str.split('.')[1])
            # pick all archives together under payload id = 1
            if payload_id == 2:
                payload_id = 1
            elif payload_id == 3 or payload_id == 4:
                payload_id = 3
            elif payload_id == 5 or payload_id == 6:
                payload_id = 5
            # insert index to payload keys
            for i in payload:
                i_splitted = i.split('.')
                i_splitted.insert(2, '1.{}'.format(indexes[payload_id]))
                address = '.'.join(i_splitted)
                if self.lang:
                    self.get_descr_of_address(address, i, payload[i])
                res[address] = payload[i]
            # count each payload type
            indexes[payload_id] += 1
        # also we must insert total count of each payload type
        for payload_type in indexes:
            if indexes[payload_type] > 0:

                count_of_payload_type = '2.{}.0'.format(payload_type)
                res[count_of_payload_type] = indexes[payload_type]
                if self.lang:
                    self.description_trans[count_of_payload_type] = self.TRANS_LANG[count_of_payload_type]

        # РµСЃР»Рё РµСЃС‚СЊ РїСЌР№Р»РѕРґС‹ РґР°С‚С‡РёРєРѕРІ

        if '2.18.0' in res:
            sensors_coeffs = {}
            for i in res:
                if i.startswith('2.18.1') and len(i.split('.')) == 7 and i.endswith('.2'):
                    sensors_coeffs[i] = res[i]
            if sensors_coeffs:
                [res.pop(e) for e in sensors_coeffs]
                if self.lang:
                    [self.description_trans.pop(e) for e in sensors_coeffs]
                    self.set_sensor_coeffs(sensors_coeffs)
        if self.lang:
            self.description_trans["0"] = self.TRANS_LANG["0"]
            # is_all_descrs_collected = all(elem in self.description_trans for elem in res)
            # print(is_all_descrs_collected)
            res['description'] = self.description_trans
        # res = json.dumps(res, indent=4, ensure_ascii=False)
        return res


class Electricity(BaseDecoder):
    PAYLOAD_FORMAT = 17
    TIMESTAMP = {'size': 4, 'pos': 0, 'format': 'I'}
    FLAGS = {'size': 1, 'pos': 4, 'format': 'B'}
    DEVICE_NUMBER = {'mask': 0x3, 'pos': 0}
    PARAMS_COUNT = {'mask': 0x1f, 'pos': 2}
    PARAM_HDR = {'size': 1, 'pos': 5, 'format': 'B'}
    PARAM_ID = {'mask': 0x7, 'pos': 0}
    CHARACTER = {'mask': 0x3, 'pos': 3}
    PHASE_TARIFF = {'mask': 0x7, 'pos': 5}
    params = {
        0: {'size': 2, 'format': 'h', 'phase_tariff': 'phase',
            },
        1: {'size': 1, 'format': 'B', 'phase_tariff': '', 'values': {0: {'mask': 0x3, 'pos': 0},
                                                                     1: {'mask': 0x3, 'pos': 2},
                                                                     2: {'mask': 0x3, 'pos': 4},
                                                                     }
            },
        2: {'size': 4, 'format': 'i', 'phase_tariff': 'tariff'},
        3: {'size': 2, 'format': 'H', 'phase_tariff': 'phase'},
        4: {'size': 2, 'format': 'H', 'phase_tariff': 'phase'},
        5: {'size': 1, 'format': 'B', 'phase_tariff': 'phase'},
        6: {'size': 2, 'format': 'H'},
    }

    def unpack_payload(self):
        timestamp = self.get_timestamp(self.payload, self.TIMESTAMP)
        flags = self.unpack_bytes(self.payload, self.FLAGS)[0]
        device_number = self.unpack_bits(flags, self.DEVICE_NUMBER)
        params_count = self.unpack_bits(flags, self.PARAMS_COUNT)
        params_count += 1
        self.pos_offset += self.PARAM_HDR['pos']
        payload_data = {
            '2.17.0': timestamp,
            '2.17.1': device_number
        }
        for i in range(params_count):
            reverse_id = 0
            param_hdr = self.unpack_bytes(self.payload[self.pos_offset - self.PARAM_HDR['pos']:], self.PARAM_HDR)[0]
            self.pos_offset += self.PARAM_HDR['size']
            param_id = self.unpack_bits(param_hdr, self.PARAM_ID)
            character = self.unpack_bits(param_hdr, self.CHARACTER)
            phase_tariff = self.unpack_bits(param_hdr, self.PHASE_TARIFF)
            param_info = self.params[param_id].copy()
            param_info['pos'] = self.pos_offset

            if phase_tariff == 4 and 'phase' in param_info['phase_tariff']:
                param_info['format'] *= 3
                param_info['size'] *= 3
            elif phase_tariff == 5 and 'tariff' in param_info['phase_tariff']:
                param_info['format'] *= 4
                param_info['size'] *= 4

            param_value = list(self.unpack_bytes(self.payload, param_info))

            if param_id in [0, 2]:
                par_v = param_value
                par_v = par_v[0]
                if par_v < 0:
                    reverse_id = 1
                for f in range(len(param_value)):
                    if param_value[f] == -2147483648 and param_id == 2:
                        if (len(param_value) == 1 and param_info['format'] == 'i') or (
                                len(param_value) > 1 and param_info['format'][f] == 'i'):
                            param_value[f] = 0
                    elif param_value[f] == -32768 and param_id == 0:
                        if (len(param_value) == 1 and param_info['format'] == 'h') or (
                                len(param_value) > 1 and param_info['format'][f] == 'h'):
                            param_value[f] = 0
                if len(param_value) == 1:
                    param_value = param_value[0]
                    payload_data.update({
                        "2.17.2.{}.{}.{}.{}".format(param_id, reverse_id, character, phase_tariff): param_value,
                    })
                else:
                    for f in range(len(param_value)):
                        payload_data.update({
                            "2.17.2.{}.{}.{}.{}.{}".format(param_id, reverse_id, character, phase_tariff, f):
                                param_value[f],
                        })
            elif param_id == 1:
                if len(param_value) > 0:
                    param_value = param_value[0]
                for ind in param_info['values']:
                    val = self.unpack_bits(param_value, param_info['values'][ind])
                    payload_data.update({
                        "2.17.2.{}.{}".format(param_id, str(ind)): val,
                    })
            else:
                if len(param_value) == 1:
                    if param_value[0] == -2147483648:
                        param_value = (0,)
                    param_value = param_value[0]
                    payload_data.update({
                        "2.17.2.{}.{}".format(param_id, phase_tariff): param_value,
                    })
                else:
                    for f in range(len(param_value)):
                        payload_data.update({
                            "2.17.2.{}.{}.{}".format(param_id, phase_tariff, f): param_value[f], })
            self.pos_offset += param_info['size']

        return payload_data


class ElectricityOld(BaseDecoder):
    PAYLOAD_FORMAT = 17
    TIMESTAMP = {'size': 4, 'pos': 0, 'format': 'I'}
    FLAGS = {'size': 1, 'pos': 4, 'format': 'B'}
    DEVICE_NUMBER = {'mask': 0x3, 'pos': 0}
    PARAMS_COUNT = {'mask': 0x1f, 'pos': 2}
    PARAM_HDR = {'size': 1, 'pos': 5, 'format': 'B'}
    PARAM_ID = {'mask': 0x7, 'pos': 0}
    CHARACTER = {'mask': 0x3, 'pos': 3}
    PHASE_TARIFF = {'mask': 0x7, 'pos': 5}
    params = {
        0: {'size': 2, 'format': 'h', 'phase_tariff': 'phase',
            },
        2: {'size': 4, 'format': 'i', 'phase_tariff': 'tariff'},
        3: {'size': 2, 'format': 'H', 'phase_tariff': 'phase'},
        4: {'size': 2, 'format': 'H', 'phase_tariff': 'phase'},
        5: {'size': 1, 'format': 'B', 'phase_tariff': 'phase'},
        6: {'size': 2, 'format': 'H'},
    }
    reverse_ids = {
        0: 7,
        2: 8
    }

    def unpack_payload(self):
        timestamp = self.get_timestamp(self.payload, self.TIMESTAMP)
        flags = self.unpack_bytes(self.payload, self.FLAGS)[0]
        device_number = self.unpack_bits(flags, self.DEVICE_NUMBER)
        params_count = self.unpack_bits(flags, self.PARAMS_COUNT)
        params_count += 1
        self.pos_offset += self.PARAM_HDR['pos']
        payload_data = {
            '2.17.0': timestamp,
            '2.17.1': device_number
        }
        for i in range(params_count):
            param_hdr = self.unpack_bytes(self.payload[self.pos_offset - self.PARAM_HDR['pos']:], self.PARAM_HDR)[0]
            self.pos_offset += self.PARAM_HDR['size']
            param_id = self.unpack_bits(param_hdr, self.PARAM_ID)
            if param_id == 1:
                raise ObsoleteFormatError('Parameter "full power" is not supported in new version')
            character = self.unpack_bits(param_hdr, self.CHARACTER)
            phase_tariff = self.unpack_bits(param_hdr, self.PHASE_TARIFF)
            param_info = self.params[param_id].copy()
            param_info['pos'] = self.pos_offset

            if phase_tariff == 4 and 'phase' in param_info['phase_tariff']:
                param_info['format'] *= 3
                param_info['size'] *= 3
            elif phase_tariff == 5 and 'tariff' in param_info['phase_tariff']:
                param_info['format'] *= 4
                param_info['size'] *= 4

            param_value = self.unpack_bytes(self.payload, param_info)
            param_id_upd = param_id
            if param_id in [0, 2]:
                par_v = param_value
                par_v = par_v[0]
                if par_v < 0:
                    param_id_upd = self.reverse_ids[param_id_upd]
            if len(param_value) == 1:
                if param_value[0] == -2147483648:
                    param_value = (0,)
                param_value = param_value[0]
                payload_data.update({
                    "2.17.2.{}.{}.{}".format(param_id_upd, character, phase_tariff): param_value,
                })
            else:
                for f in range(len(param_value)):
                    payload_data.update({
                        "2.17.2.{}.{}.{}.{}".format(param_id_upd, character, phase_tariff, f): param_value[f],
                    })
            self.pos_offset += param_info['size']

        return payload_data


class Tamper(BaseDecoder):
    PAYLOAD_FORMAT = 7
    EVENT_COUNT_STATE = {'size': 1, 'pos': 0, 'format': 'B'}
    EVENT_COUNT = {'mask': 0x3f, 'pos': 0}
    TIMESTAMP = {'size': 4, 'pos': 1, 'format': 'I'}
    EVENT = {'size': 1, 'pos': 5, 'format': 'B'}
    TS_DELTA = {'size': 2, 'pos': 6, 'format': 'H'}

    def unpack_payload(self):
        events_count_state = self.unpack_bytes(self.payload, self.EVENT_COUNT_STATE)[0]
        events_count = self.unpack_bits(events_count_state, self.EVENT_COUNT)
        timestamp = self.unpack_bytes(self.payload, self.TIMESTAMP)[0]
        timestamp += self.default_time_offset
        event_time = timestamp
        payload_data = {

            '2.7.1.255': events_count
        }

        for i in range(events_count):

            if i > 0:
                ts_delta = self.unpack_bytes(self.payload[self.pos_offset:], self.TS_DELTA)[0]
                self.pos_offset += self.EVENT['size'] + self.TS_DELTA['size']
                event_time = timestamp + ts_delta

            event = self.unpack_bytes(self.payload[self.pos_offset:], self.EVENT)[0]

            event_values = [e for e, v in enumerate('{0:08b}'.format(event)[::-1]) if v == '1']
            event_time_str = self.timestamp_to_utc(event_time)

            payload_data.update(
                {
                    '2.7.1.{}.0'.format(i): event_time_str

                }
            )
            for k in list(event_values):
                payload_data.update(
                    {
                        '2.7.1.{}.1.{}'.format(i, k): 1
                    })

        self.pos_offset += self.TS_DELTA['pos']

        return payload_data


class Archive(BaseDecoder):
    offset = 0
    PAYLOAD_FORMAT = 6
    TIMESTAMP = {"size": 4, "pos": 0, "format": "I"}
    INIT_VAL = {"size": 4, "pos": 4, "format": "i"}
    FLAGS = {"size": 2, "pos": 8, "format": "H"}
    DATA_BITS_COUNT = {"mask": 0x1f, "pos": 0},
    DIV10 = {"mask": 0x1, "pos": 5},
    RLE = {"mask": 0x1, "pos": 6},
    SIGNED = {"mask": 0x1, "pos": 7},
    REC_NUM = {"mask": 0x3f, "pos": 8},
    RESOL = {"mask": 0x1, "pos": 14},
    ALL_DIFF_ZERO = {"mask": 0x1, "pos": 15}
    PAYLOAD_ID_MAPPING = {
        1: 1,
        2: 1,
        3: 3,
        4: 3,
        5: 5,
        6: 5
    }

    def __init__(self, payload, lang=None, payload_id=None):
        super(Archive, self).__init__(payload, lang, payload_id)
        self.default_time_offset += self.offset

    def get_bits_value(self, data_bits, pos, size):
        return int(data_bits[pos:pos + size][::-1], 2)

    def unpack_payload(self):

        timestamp = self.get_timestamp(self.payload, self.TIMESTAMP)
        init_value = self.unpack_bytes(self.payload, self.INIT_VAL)[0]
        flags = self.unpack_bytes(self.payload, self.FLAGS)[0]
        data_bits_count = self.unpack_bits(flags, self.DATA_BITS_COUNT[0])
        div10 = self.unpack_bits(flags, self.DIV10[0])  # now not use
        rle = self.unpack_bits(flags, self.RLE[0])
        signed = self.unpack_bits(flags, self.SIGNED[0])
        rec_num = self.unpack_bits(flags, self.REC_NUM[0])
        payload_data = {
            '2.{}.0'.format(self.PAYLOAD_ID_MAPPING[self.payload_id]): timestamp,
        }

        resol = self.unpack_bits(flags, self.RESOL[0])
        all_diff_zero = self.unpack_bits(flags, self.ALL_DIFF_ZERO)
        individual = resol
        data_offset = self.TIMESTAMP['size'] + self.FLAGS['size'] + self.INIT_VAL['size']
        data = self.payload[data_offset:]
        data_bits = ''.join([format(b, '08b')[::-1] for b in data])
        bit_offset = 0
        bypass_individual_bit = False
        values = {'2.{}.1.0'.format(self.PAYLOAD_ID_MAPPING[self.payload_id]): init_value}
        while rec_num:
            if all_diff_zero:
                values.update({'2.{}.1.{}'.format(self.PAYLOAD_ID_MAPPING[self.payload_id], len(values)): init_value})
                rec_num -= 1
                continue

            if individual and not bypass_individual_bit:
                diff_len_flag = self.get_bits_value(data_bits, bit_offset, 1)
                bit_offset += 1
                if diff_len_flag != 0:
                    data_bits_count = self.get_bits_value(data_bits, bit_offset, 5)
                    bit_offset += 5

            bypass_individual_bit = False
            if rle:
                rle_value = self.get_bits_value(data_bits, bit_offset, 1)
                bit_offset += 1
                if rle_value == 1:
                    values.update({'2.{}.1.{}'.format(self.PAYLOAD_ID_MAPPING[self.payload_id], len(values)): init_value})
                    rec_num -= 1
                    repeat_values_count = self.get_bits_value(data_bits, bit_offset, 5)
                    bit_offset += 5
                    if rec_num < repeat_values_count:
                        raise ArchiveDecoderError('Archive parse error: rec_num < repeat_values_count')

                    for t in range(repeat_values_count):
                        values.update(
                            {'2.{}.1.{}'.format(self.PAYLOAD_ID_MAPPING[self.payload_id], len(values)): init_value})
                        rec_num -= 1

                    bypass_individual_bit = True
                    continue

            diff = self.get_bits_value(data_bits, bit_offset, data_bits_count + 1)
            bit_offset += data_bits_count + 1

            if signed and data_bits_count < 31:
                mask = 1 << data_bits_count
                diff = (diff ^ mask) - mask

            init_value += diff
            values.update({'2.{}.1.{}'.format(self.PAYLOAD_ID_MAPPING[self.payload_id], len(values)): init_value})
            rec_num -= 1

        pos_offset = self.TIMESTAMP['size'] + self.FLAGS['size'] + self.INIT_VAL['size'] + ceil(bit_offset / 8)
        self.pos_offset = pos_offset

        payload_data.update(values)
        return payload_data


class ArchiveOld(Archive):
    offset = 3600


class Lighting(BaseDecoder):
    PAYLOAD_FORMAT = 16
    TIMESTAMP = {'size': 4, 'pos': 0, 'format': 'I'}
    FLAGS = {"size": 1, "pos": 4, "format": "B"}
    PARAMS_COUNT = {"mask": 0xf, "pos": 3}
    PARAM_ID = {"size": 1, "pos": 5, "format": "B"}
    TYPE_POSITION_SIMPLE = 0
    TYPE_TEMPERATURE_MIN_MAX = 2
    TYPE_SCHEDULE_LIGHTING = 32
    SCHEDULE_LIGHTING_CELL_INDEX = {"size": 1, "pos": 0, "format": "B"}
    PARAMS = {
        0: {'size': 8, 'format': 'II'},
        1: {'size': 1, 'format': 'b'},
        2: {'size': 2, 'format': 'bb'},
        3: {'size': 3, 'format': 'BH'},
        4: {'size': 2, 'format': 'H'},
        5: {'size': 2, 'format': 'H'},
        6: {'size': 2, 'format': 'H'},
        7: {'size': 4, 'format': 'I'},
        8: {'size': 4, 'format': 'I'},
        9: {'size': 4, 'format': 'I'},
        10: {'size': 4, 'format': 'I'},
        21: {'size': 2, 'format': 'H'},
        22: {'size': 4, 'format': 'I'},
        23: {'size': 4, 'format': 'I'},
        32: {'size': 9, 'format': 'xxxxxxxbQ'},
    }

    def get_position(self, value):
        latitude_val = value[0] & 0xFFFFFFFF
        latitude = (latitude_val >> 23) + (latitude_val & 0x7FFFFF) / 10000.0 / 60
        longitude_val = value[1] & 0xFFFFFFFF
        longitude = (longitude_val >> 23) + (longitude_val & 0x7FFFFF) / 10000.0 / 60
        return latitude, longitude

    def unpack_payload(self):
        timestamp = self.unpack_bytes(self.payload, self.TIMESTAMP)[0]
        timestamp = self.timestamp_to_utc(timestamp + self.default_time_offset)
        flags = self.unpack_bytes(self.payload, self.FLAGS)[0]
        param_count = self.unpack_bits(flags, self.PARAMS_COUNT) + 1
        pos_offset = 0
        payload_data = {
            '2.16.0': timestamp,
        }
        for i in range(param_count):
            param_id = self.unpack_bytes(self.payload[pos_offset:], self.PARAM_ID)[0]

            if param_id == self.TYPE_SCHEDULE_LIGHTING:
                offset = self.TIMESTAMP['size'] + self.FLAGS['size'] + self.PARAM_ID['size']
                schedule_decoder = ScheduleLighting(
                    self.payload[pos_offset + offset + self.SCHEDULE_LIGHTING_CELL_INDEX['size']:], self.lang)
                self.pos_offset = self.PARAMS[param_id]['size'] + offset
                cell_index = self.unpack_bytes(self.payload[pos_offset + offset:], self.SCHEDULE_LIGHTING_CELL_INDEX)[0]

                payload_data = {'2.16.0': timestamp,
                                '2.16.1.32.254': cell_index
                                }
                payload_data.update(schedule_decoder.unpack_payload())
                return payload_data
            param = self.PARAMS[param_id]
            pos_offset += self.PARAM_ID['size']
            if param_id == 3:
                param['format'] = '=BH' #'=' + param['format']  otherwise data is incorrectly decoded
            value_info = {'size': param['size'], 'pos': pos_offset + self.FLAGS['size'] + self.TIMESTAMP['size'],
                          'format': param['format']}
            value = self.unpack_bytes(self.payload, value_info)
            pos_offset += param['size']
            if len(param['format']) == 1:
                value = value[0]
            if param_id == self.TYPE_POSITION_SIMPLE:
                latitude, longtitude = self.get_position(value)
                payload_data['2.16.1.0.1'] = latitude
                payload_data['2.16.1.0.2'] = longtitude
            elif param_id == self.TYPE_TEMPERATURE_MIN_MAX and type(value) == tuple and len(value) == 2:
                for index_temp in range(2):
                    payload_data['2.16.1.{}.{}'.format(param_id, index_temp + 1)] = value[index_temp]
            else:

                payload_data.update({
                    '2.16.1.{}'.format(param_id): value
                })

        self.pos_offset = pos_offset + self.TIMESTAMP['size'] + self.FLAGS['size']
        return payload_data


class Regular(BaseDecoder):
    PAYLOAD_FORMAT = 0
    FLAGS_INFO = {"size": 1, 'pos': 0, "format": "B"}
    TIMESTAMP = {"size": 4, "pos": 1, "format": "I"}
    COUNTER_SIZE = 3
    CHANNEL1 = {'mask': 0x1, 'pos': 0}
    CHANNEL2 = {'mask': 0x1, 'pos': 1}
    CHANNEL3 = {'mask': 0x1, 'pos': 2}
    CHANNEL1_4bytes = {'mask': 0x1, 'pos': 3}
    CHANNEL2_4bytes = {'mask': 0x1, 'pos': 4}
    CHANNEL3_4bytes = {'mask': 0x1, 'pos': 5}
    COUNTER_INIT = {'mask': 0x1, 'pos': 7}

    def unpack_payload(self):
        flags = self.unpack_bytes(self.payload, self.FLAGS_INFO)[0]
        timestamp = self.unpack_bytes(self.payload, self.TIMESTAMP)[0]
        timestamp = self.timestamp_to_utc(timestamp + self.default_time_offset)
        pos_offset = 5
        payload_data = {
            "2.0.0": timestamp,
        }
        for k in range(3):
            channel = self.unpack_bits(flags, getattr(self, 'CHANNEL{}'.format(k + 1)))
            if channel == 1:
                channel_4bytes = self.unpack_bits(flags, getattr(self, 'CHANNEL{}_4bytes'.format(k + 1)))
                counter_size = self.COUNTER_SIZE
                if channel_4bytes == 1: # int32 or int24
                    counter_size += 1
                counter_bytes = self.payload[pos_offset:pos_offset + counter_size]
                if channel_4bytes == 0: # for int24
                    counter_bytes += (b'\0' if counter_bytes[2] < 128 else b'\xff') 
                if len(counter_bytes) != 4:
                    raise InvalidPayloadLengthError('Invalid payload length')
                counter = struct.unpack('i', counter_bytes)[0]
                pos_offset += counter_size
                payload_data['2.0.1.{}'.format(k)] = counter
        counter_init = self.unpack_bits(flags, self.COUNTER_INIT)
        payload_data['2.0.2'] = counter_init
        self.pos_offset = pos_offset
        return payload_data


class Modbus(BaseDecoder):
    PAYLOAD_FORMAT = 32
    TIMESTAMP = {'size': 4, 'pos': 0, 'format': 'I'}
    FLAGS = {"size": 1, "pos": 4, "format": "B"}
    DEVICE_NUMBER = {'mask': 0x3, 'pos': 0}
    PARAMS_COUNT = {'mask': 0x1f, 'pos': 2}
    PARAM_ID = {"size": 4, "pos": 5, "format": "I"}
    MODBUS_FUNC = {'mask': 0x3, 'pos': 0}
    NUM_OF_MODBUS_ELEMENTS = {'size': 1, 'pos': 6, 'format': 'B'}
    ADDRESS_OF_FIRST = {'size': 2, 'pos': 7, 'format': 'H'}

    def unpack_payload(self):
        timestamp = self.unpack_bytes(self.payload, self.TIMESTAMP)[0]
        timestamp = self.timestamp_to_utc(timestamp + self.default_time_offset)
        flags = self.unpack_bytes(self.payload, self.FLAGS)[0]
        device_number = self.unpack_bits(flags, self.DEVICE_NUMBER)
        params_count = self.unpack_bits(flags, self.PARAMS_COUNT) + 1
        pos_offset = self.TIMESTAMP['size'] + self.FLAGS['size']
        offset = 0
        payload_data = {
            '2.32.0': timestamp,
            "2.32.1": device_number,
            "2.32.2.255": params_count
        }
        for k in range(params_count):
            param_id = self.unpack_bytes(self.payload[offset:], self.PARAM_ID)[0]
            modbus_func = self.unpack_bits(param_id, self.MODBUS_FUNC) + 1
            num_of_elements = self.unpack_bytes(self.payload[offset:], self.NUM_OF_MODBUS_ELEMENTS)[0]
            address_of_first = self.unpack_bytes(self.payload[offset:], self.ADDRESS_OF_FIRST)[0]
            pos_offset += self.PARAM_ID['size']
            if modbus_func in [3, 4]:
                modbus_func_size = int(num_of_elements * 2)
            else:
                modbus_func_size = int(ceil(num_of_elements / 8))
            modbus_data_hex = binascii.hexlify(self.payload[pos_offset:pos_offset + modbus_func_size]).decode("utf-8")
            pos_offset += modbus_func_size
            payload_data.update({
                '2.32.2.{}.0'.format(k): modbus_func,
                '2.32.2.{}.1'.format(k): num_of_elements,
            })
            # OLD VERSION
            # payload_data.update({
            #     '2.32.2.{}'.format(k): modbus_data_hex,
            #     '2.32.2.{}.2'.format(k): address_of_first,
            #     # 'param':params
            #             })

            # NEW VERSION
            for i in range(0, 2 * modbus_func_size, 4):
                payload_data["2.32.2.{}.2.{}".format(k, address_of_first)] = modbus_data_hex[i:i + 4]
                address_of_first += 1

            offset += self.PARAM_ID['size'] + modbus_func_size
        self.pos_offset = pos_offset
        return payload_data


class DiEvents(BaseDecoder):
    PAYLOAD_FORMAT = 10
    TIMESTAMP = {'size': 4, 'pos': 1, 'format': 'I'}
    FLAGS = {"size": 1, "pos": 0, "format": "B"}
    DIFF_EVENT_FIRST = {'size': 1, 'pos': 5, 'format': 'B'}
    DIFF_EVENTS_INFO = {'size': 2, 'pos': 6, 'format': 'H'}
    DIFF_EVENTS_STAGES = {'mask': 0x3f, 'pos': 0}
    DIFF_EVENTS_TIMESTAMP = {'mask': 0x3ff, 'pos': 6}
    INPUT1 = {'mask': 0x1, 'pos': 0}
    INPUT2 = {'mask': 0x1, 'pos': 1}
    INPUT3 = {'mask': 0x1, 'pos': 2}
    EVENTS_COUNT = {'mask': 0x1f, 'pos': 3}

    def unpack_payload(self):
        flags = self.unpack_bytes(self.payload, self.FLAGS)[0]
        payload_data = {}
        for i in range(3):
            input_value = self.unpack_bits(flags, getattr(self, 'INPUT{}'.format(i + 1)))
            payload_data.update({
                '2.10.0.{}'.format(i): input_value
            })
        events_count = self.unpack_bits(flags, self.EVENTS_COUNT)
        pos_offset = self.FLAGS['size']
        payload_data.update({
            '2.10.2.255': events_count
            })
        if events_count > 0:
            timestamp_value = self.unpack_bytes(self.payload, self.TIMESTAMP)[0]
            timestamp = self.timestamp_to_utc(timestamp_value + self.default_time_offset)
            pos_offset += self.TIMESTAMP['size'] + self.DIFF_EVENT_FIRST['size']
            for j in range(events_count):
                if j == 0:
                    event_info = self.unpack_bytes(self.payload, self.DIFF_EVENT_FIRST)[0]
                    event_timestamp = timestamp
                else:
                    event_info = self.unpack_bytes(self.payload[pos_offset:], self.DIFF_EVENTS_INFO)[0]
                    pos_offset += self.DIFF_EVENTS_INFO['size']
                    event_timestamp = self.unpack_bits(event_info, self.DIFF_EVENTS_TIMESTAMP)
                    event_timestamp = self.timestamp_to_utc(
                        self.default_time_offset + timestamp_value + event_timestamp)
                event_stages_info = self.unpack_bits(event_info, self.DIFF_EVENTS_STAGES)
                event_stages = [int(v) for e, v in enumerate(format(event_stages_info, '08b')[::-1])]
                for i in range(3):
                    if event_stages[i] == 1:
                        payload_data.update({
                            "2.10.2.{}.1.{}".format(j, i): 0
                        })
                    elif event_stages[i + 3] == 1:
                        payload_data.update({
                            "2.10.2.{}.1.{}".format(j, i): 1
                        })
                payload_data.update({
                    "2.10.2.{}.0".format(j): event_timestamp
                })
        self.pos_offset = pos_offset
        return payload_data


class Service(BaseDecoder):
    PAYLOAD_FORMAT = 8
    SERVICE_TYPE = {'size': 1, 'pos': 0, 'format': 'B'}
    TYPE_FW_VER = 0
    FW_VER_SIZE = 13
    TYPE_ENERGY_INFO = 1
    ENERGY_INFO_SIZE = 15
    TYPE_SFG_RESULT = 2
    SFG_RESULT_SIZE = 3
    TYPE_SFG_ID = 3
    SFG_ID_SIZE = 3
    TYPE_RSSI_SNR_INFO = 6
    RSSI_SNR_INFO_SIZE = 4
    TYPE_SERIAL_DRIVER_EVENT = 7
    SERIAL_DRIVER_EVENT_SIZE = 9
    TYPE_CALL_DATA = 8
    CALL_DATA_SIZE = 16
    GIT_SHA1 = {'size': 4, 'pos': 0, 'format': 'I'}
    FW_VER = {'size': 4, 'pos': 4, 'format': 'I'}
    RESET_SOURCE = {'size': 1, 'pos': 8, 'format': 'B'}
    ADDR_BYTES = {'size': 3, 'pos': 9, 'format': 'I'}
    ADDR_FAULT = {'mask': 0x1, 'pos': 23}
    ADDRESS = {'mask': 0x7FFFFF, 'pos': 0}
    CONSUMED_CHARGE = {'size': 5, 'pos': 0, 'format': 'Q'}
    TRANSMITTED_PACKET_NUM = {'size': 3, 'pos': 5, 'format': 'I'}
    TRANSMITTER_TIME = {'size': 4, 'pos': 8, 'format': 'I'}
    VOLTAGE_ON_BATTERY = {'size': 2, 'pos': 12, 'format': 'H'}
    CFG_RESULT = {'size': 2, 'pos': 0, 'format': 'H'}
    BLOCK_ID = {'size': 1, 'pos': 0, 'format': 'B'}
    CFG_ID = {'size': 1, 'pos': 1, 'format': 'B'}
    RSSI = {'size': 2, 'pos': 0, 'format': 'H'}
    SNR = {'size': 1, 'pos': 2, 'format': 'B'}
    EVENT_TIMESTAMP = {'size': 4, 'pos': 0, 'format': 'I'}
    DRVID_DEVID = {'size': 2, 'pos': 4, 'format': 'H'}
    CODE = {'size': 1, 'pos': 6, 'format': 'B'}
    SUBCODE = {'size': 1, 'pos': 7, 'format': 'B'}
    DRVID = {'mask': 0xFFF, 'pos': 0}
    DEVID = {'mask': 0x3, 'pos': 12}
    DEVICE_TYPE = {'size': 1, 'pos': 0, 'format': 'B'}
    CHANNEL_LIMITS = {'size': 2, 'pos': 1, 'format': 'H'}
    CALIBRATION_FLAGS = {'size': 1, 'pos': 14, 'format': 'B'}

    def unpack_payload(self):
        type_id = self.unpack_bytes(self.payload, self.SERVICE_TYPE)[0]
        service_info = {
        }
        self.payload = self.payload[1:]
        service_value = {}
        if type_id == self.TYPE_FW_VER:
            if (sys.version_info < (3, 0)):
                git_sha1 = str(self.payload[self.GIT_SHA1['pos']:self.GIT_SHA1['pos'] + self.GIT_SHA1['size']]).encode(
                    'hex')
                fw_ver = str(self.payload[self.FW_VER['pos']:self.FW_VER['pos'] + self.FW_VER['size']]).encode('hex')
            else:
                git_sha1 = self.payload[self.GIT_SHA1['pos']:self.GIT_SHA1['pos'] + self.GIT_SHA1['size']].hex()
                fw_ver = self.payload[self.FW_VER['pos']:self.FW_VER['pos'] + self.FW_VER['size']].hex()
            reset_source_info = self.unpack_bytes(self.payload, self.RESET_SOURCE)[0]

            reset_source = [e for e, v in enumerate(format(reset_source_info, '08b')[::-1]) if v == '1']
            service_info.update({
                '2.8.0.0': git_sha1,
                '2.8.0.1': fw_ver,
            })
            for r in reset_source:
                service_info.update({
                    '2.8.0.2.{}'.format(r): 1
                })
            address_info = self.payload[self.ADDR_BYTES['pos']:self.ADDR_BYTES['pos'] + self.ADDR_BYTES['size']]
            address_info.append(0x00)
            # because format is I
            if len(address_info) != 4:
                raise InvalidPayloadLengthError('Invalid payload length')
            address_info = struct.unpack(self.ADDR_BYTES['format'], address_info)[0]
            addr_fault = self.unpack_bits(address_info, self.ADDR_FAULT)
            address = self.unpack_bits(address_info, self.ADDRESS)
            service_info.update({
                '2.8.0.3': addr_fault,
                '2.8.0.4': address
            })

            self.pos_offset = self.FW_VER_SIZE
        elif type_id == self.TYPE_ENERGY_INFO:
            consumed_charge_payload = self.payload[
                                      self.CONSUMED_CHARGE['pos']:self.CONSUMED_CHARGE['pos'] + self.CONSUMED_CHARGE[
                                          'size']]

            consumed_charge_payload.extend([0x00, 0x00, 0x00])
            if len(consumed_charge_payload) != 8:
                raise InvalidPayloadLengthError('Invalid payload length')
            consumed_charge = struct.unpack(self.CONSUMED_CHARGE['format'], consumed_charge_payload)[0]

            transmitted_packet_num_payload = self.payload[
                                             self.TRANSMITTED_PACKET_NUM['pos']:self.TRANSMITTED_PACKET_NUM['pos'] +
                                                                                self.TRANSMITTED_PACKET_NUM['size']]
            transmitted_packet_num_payload.append(0x00)
            if len(transmitted_packet_num_payload) != 4:
                raise InvalidPayloadLengthError('Invalid payload length')
            transmitted_packet_num = struct.unpack(self.TRANSMITTED_PACKET_NUM['format'],
                                                   transmitted_packet_num_payload)[0]
            transmitter_time = self.unpack_bytes(self.payload, self.TRANSMITTER_TIME)[0]
            voltage_on_battery = self.unpack_bytes(self.payload, self.VOLTAGE_ON_BATTERY)[0]

            service_info.update({
                '2.8.1.0': consumed_charge,
                '2.8.1.1': transmitted_packet_num,
                '2.8.1.2': transmitter_time,
                '2.8.1.3': voltage_on_battery,
            })
            self.pos_offset = self.ENERGY_INFO_SIZE
        elif type_id == self.TYPE_SFG_RESULT:
            data = self.unpack_bytes(self.payload, self.CFG_RESULT)[0]
            data_list = list(format(data, '016b')[::-1])
            for k in range(len(data_list)):
                service_info['2.8.2.{}'.format(k)] = data_list[k]

            self.pos_offset = self.SFG_RESULT_SIZE
        elif type_id == self.TYPE_SFG_ID:
            block_id = self.unpack_bytes(self.payload, self.BLOCK_ID)[0]
            cfg_id = self.unpack_bytes(self.payload, self.CFG_ID)[0]
            service_info.update({
                '2.8.3.0': block_id,
                '2.8.3.1': cfg_id
            })
            self.pos_offset = self.SFG_ID_SIZE
        elif type_id == self.TYPE_RSSI_SNR_INFO:
            rssi = self.unpack_bytes(self.payload, self.RSSI)[0]
            snr = self.unpack_bytes(self.payload, self.SNR)[0]
            service_info.update({
                '2.8.6.0': rssi,
                '2.8.6.1': snr
            })

            self.pos_offset = self.RSSI_SNR_INFO_SIZE
        elif type_id == self.TYPE_SERIAL_DRIVER_EVENT:
            timestamp = self.unpack_bytes(self.payload, self.EVENT_TIMESTAMP)[0]
            timestamp = self.timestamp_to_utc(timestamp + self.default_time_offset)
            drvid_devid = self.unpack_bytes(self.payload, self.DRVID_DEVID)[0]
            code = self.unpack_bytes(self.payload, self.CODE)[0]
            self.pos_offset = self.SERIAL_DRIVER_EVENT_SIZE
            subcode = self.unpack_bytes(self.payload, self.SUBCODE)[0]
            drvid = self.unpack_bits(drvid_devid, self.DRVID)
            devid = self.unpack_bits(drvid_devid, self.DEVID)
            service_info.update({
                '2.8.7.0': timestamp,
                '2.8.7.1': drvid,
                '2.8.7.2': devid,
                '2.8.7.3': code,
                '2.8.7.4': subcode
            })
        elif type_id == self.TYPE_CALL_DATA:
            device_type = self.unpack_bytes(self.payload, self.DEVICE_TYPE)[0]
            service_info['2.8.8.0'] = device_type
            for i in range(3):
                offset = i * 2
                upper_limit = self.unpack_bytes(self.payload[offset:], self.CHANNEL_LIMITS)[0]
                lower_limit = self.unpack_bytes(self.payload[offset + 6:], self.CHANNEL_LIMITS)[0]
                service_info['2.8.8.{}.0'.format(i)] = upper_limit
                service_info['2.8.8.{}.1'.format(i)] = lower_limit
                offset += 2
            flags = self.unpack_bytes(self.payload, self.CALIBRATION_FLAGS)[0]
            calibration_flags = [e for e, v in enumerate(format(flags, '08b')[::-1]) if v == '1']
            for e in calibration_flags:
                service_info['2.8.8.3.{}'.format(e)] = 1

            self.pos_offset = self.CALL_DATA_SIZE

        return service_info


class ScheduleLighting(BaseDecoder):
    PROP = {'size': 1, 'pos': 0, 'format': 'B'}
    DIM = {'size': 1, 'pos': 1, 'format': 'B'}
    WEEK_MONTH = {'size': 1, 'pos': 4, 'format': 'B'}
    DAY_OF_WEEK = {'size': 1, 'pos': 5, 'format': 'B'}
    DAY_OF_WEEK_TIME_OFFSET = {'size': 3, 'pos': 5, 'format': 'I'}
    PROP_MODE = {'mask': 0x3, 'pos': 0}
    PROP_IFNOGEO = {'mask': 0x1, 'pos': 2}
    PROP_PRIORITY = {'mask': 0x3, 'pos': 3}
    PROP_COMMAND = {'mask': 0x7, 'pos': 5}
    DIMMING = {'mask': 0x7F, 'pos': 0}
    DATE_FORMAT = {'mask': 0x3, 'pos': 0}
    WEEK_MONTH_VAL = {'mask': 0x3F, 'pos': 2}

    def unpack_payload(self):
        prop = self.unpack_bytes(self.payload, self.PROP)[0]
        dim = self.unpack_bytes(self.payload, self.DIM)[0]
        week_month = self.unpack_bytes(self.payload, self.WEEK_MONTH)[0]
        day_of_week = self.unpack_bytes(self.payload, self.DAY_OF_WEEK)[0]
        prop_mode = self.unpack_bits(prop, self.PROP_MODE)
        prop_ifnogeo = self.unpack_bits(prop, self.PROP_IFNOGEO)
        prop_priority = self.unpack_bits(prop, self.PROP_PRIORITY)
        prop_command = self.unpack_bits(prop, self.PROP_COMMAND)
        dimming = self.unpack_bits(dim, self.DIMMING)
        date_format = self.unpack_bits(week_month, self.DATE_FORMAT)
        week_month_val = self.unpack_bits(week_month, self.WEEK_MONTH_VAL)
        week_no = {}
        month = {}
        dow = {}
        date = {}
        if date_format in [1, 2]:
            if week_month_val <= 51:
                week_no = {'week_no': week_month_val}
            else:
                month = {'month': week_month_val}
        if date_format < 3:
            dow = format(day_of_week & 0x7f, '08b')[:0:-1]
            dow = [int(d) for d in dow]
            dow = {'day_of_week': dow}
        else:
            date = {'date': day_of_week & 0xf}

        high_bit = 0x1 if day_of_week > 127 else 0x0
        time_offset_hex = self.payload[6:]
        time_offset_hex.extend([high_bit, 0x0])
        if len(time_offset_hex) != 4:
            raise InvalidPayloadLengthError('Invalid payload length')
        time_offset = struct.unpack('i', time_offset_hex)[0]

        if prop_mode in [2, 3] and time_offset > 65535:
            time_offset = (131072 - time_offset) * -1

        scheduler = {
            '2.16.1.32.0': prop_mode,
            '2.16.1.32.1': prop_ifnogeo,
            '2.16.1.32.2': prop_priority,
            '2.16.1.32.3': prop_command,
            '2.16.1.32.4': dimming,
            '2.16.1.32.5': date_format,
            '2.16.1.32.6': time_offset,
        }
        if week_no:
            scheduler['2.16.1.32.7'] = week_no['week_no']
        if month:
            scheduler['2.16.1.32.8'] = month['month']
        if dow:
            scheduler['2.16.1.32.9'] = dow['day_of_week']
        if date:
            scheduler['2.16.1.32.10'] = date['date']
        return scheduler


class Sensors(BaseDecoder):
    SIZE_FORMATS = {
        0: 'B',
        1: 'H',
        2: 'I',
        3: 'I'
    }
    TIMESTAMP = {'size': 4, 'pos': 0, 'format': 'I'}
    FLAGS = {'size': 1, 'pos': 4, 'format': 'B'}
    PARAM_HDR = {'size': 1, 'pos': 5, 'format': 'B'}
    OUT_OF_BOUND_INFO = {'size': 1, 'pos': 6, 'format': 'B'}
    PARAM_VALUE = {'pos': 6}
    SENSOR_NUMBER = {'mask': 0x3, 'pos': 0}
    PARAMS_COUNT = {'mask': 0x7, 'pos': 2}
    OUT_OF_BOUND_STATUS_POS = 6
    PAR_ID_TYPE = {'mask': 0x1, 'pos': 7}
    PAR_ID = {'mask': 0x7, 'pos': 0}
    PARAM_LENGTH = {'mask': 0x3, 'pos': 3}
    COEFFICIENT = {'mask': 0x3, 'pos': 5}
    SIGN = {'mask': 0x1, 'pos': 7}
    OUT_OF_BOUND_VALUE = {'mask': 0x7, 'pos': 0}
    COEFFICIENTS = {
        0: 1,
        1: 0.1,
        2: 0.01,
        3: 0.001
    }

    def unpack_payload(self):
        timestamp_value = self.unpack_bytes(self.payload, self.TIMESTAMP)[0]
        timestamp = self.timestamp_to_utc(timestamp_value + self.default_time_offset)
        flags = self.unpack_bytes(self.payload, self.FLAGS)[0]

        sensor_number = self.unpack_bits(flags, self.SENSOR_NUMBER)
        params_count = self.unpack_bits(flags, self.PARAMS_COUNT) + 1
        out_of_bound_status = int('{0:08b}'.format(flags)[7 - self.OUT_OF_BOUND_STATUS_POS])

        pos_offset = 0
        payload_data = {
            '2.18.0': timestamp,
            '2.18.1': sensor_number,
        }

        for i in range(params_count):

            param_hdr = self.unpack_bytes(self.payload[pos_offset:], self.PARAM_HDR)[0]
            par_id = self.unpack_bits(param_hdr, self.PAR_ID)
            param_length = self.unpack_bits(param_hdr, self.PARAM_LENGTH) + 1

            coefficient = self.unpack_bits(param_hdr, self.COEFFICIENT)
            sign = self.unpack_bits(param_hdr, self.SIGN)
            if out_of_bound_status:
                out_of_bound_value = self.unpack_bytes(self.payload[pos_offset:], self.OUT_OF_BOUND_INFO)[0]
                out_of_bound_list = [i for i, bit in enumerate(bin(out_of_bound_value)[::-1]) if bit == '1']
                if out_of_bound_list:
                    out_of_bound_list = out_of_bound_list[0]
                    payload_data['2.18.2.{}.1'.format(par_id)] = out_of_bound_list
                pos_offset += self.OUT_OF_BOUND_INFO['size']

            param_data = self.payload[pos_offset + self.OUT_OF_BOUND_INFO['pos']:pos_offset + self.OUT_OF_BOUND_INFO[
                'pos'] + param_length]

            param_data_info = {'size': len(param_data), 'pos': pos_offset + self.OUT_OF_BOUND_INFO['pos'],
                                                                    'format': self.SIZE_FORMATS[param_length - 1]}

            if sign == 1:
                param_data_info['format'] = param_data_info['format'].lower()

            param_data = self.unpack_bytes(self.payload, param_data_info, sign, integer24_trick=(param_length==3))[0]

            payload_data.update({
                '2.18.2.{}'.format(par_id): param_data,
                '2.18.2.{}.2'.format(par_id): self.COEFFICIENTS[coefficient],
            })
            pos_offset += (param_length + self.PARAM_HDR['size'])
        self.pos_offset = pos_offset + self.PARAM_HDR['pos']
        return payload_data


class HeatMeter(BaseDecoder):
    SIZE_FORMATS = {1: 'B', 2: 'H', 4: 'I'}
    TIMESTAMP = {'size': 4, 'pos': 0, 'format': 'I'}
    FLAGS = {'size': 1, 'pos': 4, 'format': 'B'}
    PARAM_HDR = {'size': 1, 'pos': 5, 'format': 'B'}
    PARAM_VALUE = {'pos': 6}
    DEVICE_NUM = {'mask': 0x3, 'pos': 0}
    PARAMS_COUNT = {'mask': 0x1F, 'pos': 2}
    PARAMS = {
        0: {'size': 4, 'format': 'f'},
        1: {'size': 8, 'format': 'd'},
        2: {'size': 2, 'format': 'h'},
        3: {'size': 2, 'format': 'h'},
        4: {'size': 2, 'format': 'h'},
        5: {'size': 2, 'format': 'h'},
        6: {'size': 4, 'format': 'I'},
        7: {'size': 4, 'format': 'I'},
        8: {'size': 2, 'format': 'H'},
        9: {'size': 4, 'format': 'I'},
        10: {'size': 4, 'format': 'I'},
    }
    PARAM_TYPE = {'mask': 0x1F, 'pos': 0}
    CHANNEL = {'mask': 0x3, 'pos': 5}
    BRANCH = {'mask': 0x1, 'pos': 7}

    def unpack_payload(self):
        timestamp_value = self.unpack_bytes(self.payload, self.TIMESTAMP)[0]
        timestamp = self.timestamp_to_utc(timestamp_value + self.default_time_offset)
        flags = self.unpack_bytes(self.payload, self.FLAGS)[0]
        pos_offset = 0
        device_num = self.unpack_bits(flags, self.DEVICE_NUM)
        param_count = self.unpack_bits(flags, self.PARAMS_COUNT) + 1
        payload_data = {
            '2.19.0': timestamp,
            '2.19.1': device_num,
        }
        for i in range(param_count):
            param_hdr = self.unpack_bytes(self.payload[pos_offset:], self.PARAM_HDR)[0]
            param_type = self.unpack_bits(param_hdr, self.PARAM_TYPE)
            if param_type > 10:
                raise NotExistingParamError('HeatMeter: No such parameter')
            param = self.PARAMS[param_type]
            param.update(self.PARAM_VALUE)
            branch = self.unpack_bits(param_hdr, self.BRANCH) + 1
            channel = self.unpack_bits(param_hdr, self.CHANNEL) + 1
            param_value = self.unpack_bytes(self.payload[pos_offset:], param)[0]
            payload_data.update({
                '2.19.2.{}.0.{}'.format(i, param_type): param_value,
                '2.19.2.{}.1'.format(i): branch,
                '2.19.2.{}.2'.format(i): channel,
            })
            pos_offset += param['size'] + self.PARAM_HDR['size']
        pos_offset += 5
        self.pos_offset = pos_offset
        return payload_data


class HeatMeterExtended(BaseDecoder):
    TIMESTAMP = {'size': 4, 'pos': 0, 'format': 'I'}
    FLAGS = {'size': 2, 'pos': 4, 'format': 'H'}
    PARAM_HDR = {'size': 1, 'pos': 6, 'format': 'B'}
    DEVICE_NUM = {'mask': 0x3, 'pos': 0}
    PARAMS_COUNT = {'mask': 0x1F, 'pos': 2}
    DATA_TYPE = {'mask': 0x3, 'pos': 7}
    ARCHIVING_TIME = {'mask': 0x7, 'pos': 9}
    PARAM_TYPE = {'mask': 0x1F, 'pos': 0}
    CHANNEL = {'mask': 0x7, 'pos': 5}
    PARAMS = {
        0: {'size': 4, 'pos': 7, 'format': 'f'},
        1: {'size': 8, 'pos': 7, 'format': 'd'},
        2: {'size': 2, 'pos': 7, 'format': 'h'},
        3: {'size': 2, 'pos': 7, 'format': 'h'},
        4: {'size': 2, 'pos': 7, 'format': 'h'},
        5: {'size': 2, 'pos': 7, 'format': 'h'},
        6: {'size': 4, 'pos': 7, 'format': 'I'},
        7: {'size': 4, 'pos': 7, 'format': 'I'},
        8: {'size': 2, 'pos': 7, 'format': 'H'},
        9: {'size': 4, 'pos': 7, 'format': 'I'},
        10: {'size': 4, 'pos': 7, 'format': 'I'},
        11: {'size': 4, 'pos': 7, 'format': 'I'},
        12: {'size': 4, 'pos': 7, 'format': 'f'},
        13: {'size': 4, 'pos': 7, 'format': 'I'},
        14: {'size': 4, 'pos': 7, 'format': 'I'},
        15: {'size': 2, 'pos': 7, 'format': 'h'},
        16: {'size': 2, 'pos': 7, 'format': 'h'},
        17: {'size': 4, 'pos': 7, 'format': 'I'},
        18: {'size': 4, 'pos': 7, 'format': 'I'},
        19: {'size': 4, 'pos': 7, 'format': 'I'},
        20: {'size': 4, 'pos': 7, 'format': 'I'},
        21: {'size': 4, 'pos': 7, 'format': 'I'},
        22: {'size': 4, 'pos': 7, 'format': 'I'},
        23: {'size': 1, 'pos': 7, 'format': 'B'},
        24: {'size': 1, 'pos': 7, 'format': 'B'},
        25: {'size': 1, 'pos': 7, 'format': 'B'},
    }

    def unpack_payload(self):
        timestamp_value = self.unpack_bytes(self.payload, self.TIMESTAMP)[0]
        timestamp = self.timestamp_to_utc(timestamp_value + self.default_time_offset)
        flags = self.unpack_bytes(self.payload, self.FLAGS)[0]
        pos_offset = 0
        device_num = self.unpack_bits(flags, self.DEVICE_NUM)
        param_count = self.unpack_bits(flags, self.PARAMS_COUNT) + 1
        data_type = self.unpack_bits(flags, self.DATA_TYPE)
        archiving_time = self.unpack_bits(flags, self.ARCHIVING_TIME)
        payload_data = {
            '2.20.0': timestamp,
            '2.20.1': device_num
        }
        for i in range(param_count):
            param_hdr = self.unpack_bytes(self.payload[pos_offset:], self.PARAM_HDR)[0]
            param_type = self.unpack_bits(param_hdr, self.PARAM_TYPE)
            if param_type > 25:
                raise NotExistingParamError('HeatMeter: No such parameter')
            param_channel = self.unpack_bits(param_hdr, self.CHANNEL) + 1
            param = self.PARAMS[param_type]
            param_value = self.unpack_bytes(self.payload[pos_offset:], param)[0]
            payload_data.update(
                {
                    '2.20.2.{}.{}.{}.{}'.format(param_channel, param_type,
                                                archiving_time, data_type, ): param_value
                }
            )
            pos_offset += param['size'] + self.PARAM_HDR['size']
        pos_offset += self.TIMESTAMP['size'] + self.FLAGS['size']
        self.pos_offset = pos_offset
        return payload_data


class MeterEvents(BaseDecoder):
    PAYLOAD_FORMAT = 21
    FLAGS = {"size": 1, "pos": 0, "format": "B"}
    EVENT_ID = {'size': 1, 'pos': 1, 'format': 'B'}
    EVENT_FLAGS = {'size': 1, 'pos': 1, 'format': 'B'}
    TIMESTAMP_EVENT = {'size': 4, 'pos': 3, 'format': 'I'}
    DATA_EVENT = {'pos': 7}
    DEVICE_NO = {'mask': 0x3, 'pos': 0}
    EVENT_COUNT = {'mask': 0x1f, 'pos': 2}
    DATA_TYPE = {'mask': 0x7, 'pos': 0}
    DATA_TYPE_DEFINITION = {
        0: {'size': 1, 'format': 'B'},
        1: {'size': 1, 'format': 'b'},
        2: {'size': 2, 'format': 'H'},
        3: {'size': 2, 'format': 'h'},
        4: {'size': 4, 'format': 'I'},
        5: {'size': 4, 'format': 'i'},
        6: {'size': 4, 'format': 'f'},
        7: {'size': 8, 'format': 'd'}
    }
    DATA_FORMAT = {'mask': 0x3, 'pos': 3}
    DATA_FORMAT_DEFINITION = {
        0: None,
        1: 'single_value',
        2: 'array'
    }
    EVENT_FORMAT = {'mask': 0x1, 'pos': 7}
    DATA_SINGLE = {"pos": 7}
    DATA_ARRAY_LEN = {"size": 1, "pos": 7, "format": "B"}

    def unpack_payload(self):
        payload_flags = self.unpack_bytes(self.payload, self.FLAGS)[0]
        dev_no = self.unpack_bits(payload_flags, self.DEVICE_NO)
        event_count = self.unpack_bits(payload_flags, self.EVENT_COUNT)
        payload_data = {}
        pos_offset = 0
        for i in range(event_count):
            event_id = self.unpack_bytes(self.payload[pos_offset:], self.EVENT_ID)[0]
            event_flags = self.unpack_bytes(self.payload[pos_offset:], self.EVENT_FLAGS)[0]
            data_format = self.unpack_bits(event_flags, self.DATA_FORMAT)
            event_format = self.unpack_bits(event_flags, self.EVENT_FORMAT)
            timestamp_event = self.unpack_bytes(self.payload[pos_offset:], self.TIMESTAMP_EVENT)[0]
            pos_offset += self.EVENT_ID['size'] + self.EVENT_FLAGS['size'] + self.TIMESTAMP_EVENT['size']
            data=None
            if data_format == 1:
                data_type = self.unpack_bits(event_flags, self.DATA_TYPE)
                data_single_info = {"size": self.DATA_TYPE_DEFINITION[data_type]['size'],
                                    "format": self.DATA_TYPE_DEFINITION[data_type]['format'],
                                    'pos': self.DATA_SINGLE['pos']}
                data = self.unpack_bytes(self.payload[pos_offset:], data_single_info)[0]
                pos_offset +=data_single_info['size']
            elif data_format == 2:
                data_len = self.unpack_bytes(self.payload[pos_offset:], self.DATA_ARRAY_LEN)[0]
                data = []
                pos = self.DATA_ARRAY_LEN['pos'] + 1
                for d in range(data_len):
                    data_array_element_info = {"size": 1, "pos": pos + d, "format": "B"}
                    data.append(self.unpack_bytes(self.payload[pos_offset:], data_array_element_info)[0])
                pos_offset += self.DATA_ARRAY_LEN['size'] + data_len + 1

        self.pos_offset = pos_offset
        return payload_data


class TarifficationData(BaseDecoder):
    PAYLOAD_FORMAT = 22
    FLAGS = {"size": 1, "pos": 0, "format": "B"}
    EVENT_ID = {'size': 1, 'pos': 1, 'format': 'B'}
    EVENT_FLAGS = {'size': 1, 'pos': 1, 'format': 'B'}
    TIMESTAMP_EVENT = {'size': 4, 'pos': 3, 'format': 'I'}
    DATA_EVENT = {'pos': 7}
    DEVICE_NO = {'mask': 0x3, 'pos': 0}
    EVENT_COUNT = {'mask': 0x1f, 'pos': 2}
    DATA_TYPE = {'mask': 0x7, 'pos': 0}
    DATA_TYPE_DEFINITION = {
        0: {'size': 1, 'format': 'B'},
        1: {'size': 1, 'format': 'b'},
        2: {'size': 2, 'format': 'H'},
        3: {'size': 2, 'format': 'h'},
        4: {'size': 4, 'format': 'I'},
        5: {'size': 4, 'format': 'i'},
        6: {'size': 4, 'format': 'f'},
        7: {'size': 8, 'format': 'd'}
    }
    DATA_FORMAT = {'mask': 0x3, 'pos': 3}
    DATA_FORMAT_DEFINITION = {
        0: None,
        1: 'single_value',
        2: 'array'
    }
    EVENT_FORMAT = {'mask': 0x1, 'pos': 7}
    DATA_SINGLE = {"pos": 7}
    DATA_ARRAY_LEN = {"size": 1, "pos": 7, "format": "B"}

    def unpack_payload(self):
        payload_flags = self.unpack_bytes(self.payload, self.FLAGS)[0]
        dev_no = self.unpack_bits(payload_flags, self.DEVICE_NO)
        event_count = self.unpack_bits(payload_flags, self.EVENT_COUNT)
        payload_data = {}
        pos_offset = 0
        for i in range(event_count):
            event_id = self.unpack_bytes(self.payload[pos_offset:], self.EVENT_ID)[0]
            event_flags = self.unpack_bytes(self.payload[pos_offset:], self.EVENT_FLAGS)[0]
            data_format = self.unpack_bits(event_flags, self.DATA_FORMAT)
            event_format = self.unpack_bits(event_flags, self.EVENT_FORMAT)
            timestamp_event = self.unpack_bytes(self.payload[pos_offset:], self.TIMESTAMP_EVENT)[0]
            pos_offset += self.EVENT_ID['size'] + self.EVENT_FLAGS['size'] + self.TIMESTAMP_EVENT['size']
            data = None
            if data_format == 1:
                data_type = self.unpack_bits(event_flags, self.DATA_TYPE)
                data_single_info = {"size": self.DATA_TYPE_DEFINITION[data_type]['size'],
                                    "format": self.DATA_TYPE_DEFINITION[data_type]['format'],
                                    'pos': self.DATA_SINGLE['pos']}
                data = self.unpack_bytes(self.payload[pos_offset:], data_single_info)[0]
                pos_offset += data_single_info['size']
            elif data_format == 2:
                data_len = self.unpack_bytes(self.payload[pos_offset:], self.DATA_ARRAY_LEN)[0]
                data = []
                pos = self.DATA_ARRAY_LEN['pos'] + 1
                for d in range(data_len):
                    data_array_element_info = {"size": 1, "pos": pos + d, "format": "B"}
                    data.append(self.unpack_bytes(self.payload[pos_offset:], data_array_element_info)[0])
                pos_offset += self.DATA_ARRAY_LEN['size'] + data_len + 1

        self.pos_offset = pos_offset
        return payload_data


