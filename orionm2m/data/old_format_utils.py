# -- coding: utf-8 --

from orionm2m.data.decoder import Decoder
import json
import datetime
import pytz


def set_flags(payload_data):
    PACKET_FLAGS = {
        '0': u"Батарея разряжена",
        '1': u"Низкая температура",
        '2': u"Высокая температура",
        '3': u"Геркон, состояние",
        '4': u"Датчик вскрытия, состояние",
        '5': u"Время устройства требует обновления"
    }

    flags_list = [i[2] for i in payload_data.keys()]
    return {'packet_flags': {i: PACKET_FLAGS[i] for i in flags_list}}


def set_electricity(payload_data):
    ELECTRICITY_CHARACTER = {
        'DESCR': 'Характер',
        'values': {
            0: {'name': 'A', 'descr': 'Активная'},
            1: {'name': 'F', 'descr': 'Полная'},
            2: {'name': 'R', 'descr': 'Реактивная'},
            3: {'name': 'n/a', 'descr': 'RFU'},
        }
    }
    ELECTRICITY_PHASE_TARIFF = {
        'DESCR': 'Фаза/Тариф',
        "PHASE_VALUES": {
            0: {'name': '', 'descr': 'однофазный / сумма или не применимо понятие "фаза"'},
            1: {'name': '_A', 'descr': 'A'},
            2: {'name': '_B', 'descr': 'B'},
            3: {'name': '_C', 'descr': 'C'},
            4: {'name': '_ABC', 'descr': 'ABC (в value передается массив из 3-х значений)'},
        },
        "TARIFF_VALUES": {
            0: {'name': '', 'descr': 'Без тарифа'},
            1: {'name': '1', 'descr': 'Тариф 1'},
            2: {'name': '2', 'descr': 'Тариф 2'},
            3: {'name': '3', 'descr': 'Тариф 3'},
            4: {'name': '4', 'descr': 'Тариф 4'},
            5: {'name': '1/2/3/4', 'descr': 'T1/2/3/4 (в value передается массив из 4-х значений)'},
        }
    }
    ELECTRICITY_PARAMS = {
        0: {'name': 'power', 'phase_tariff': 'phase',
            'template': 'CURP%(character)s%(phase)s',
            'descr': 'Мощность, Вт'},
        2: {'name': 'energy', 'phase_tariff': 'tariff',
            'template': 'TOTAE%(tariff)s_%(character)s', 'descr': 'Энергия, Вт*ч / ВАр*ч'},
        3: {'name': 'voltage', 'phase_tariff': 'phase',
            'template': 'CURV%(phase)s',
            'descr': 'Напряжение, В'},
        4: {'name': 'amperage', 'phase_tariff': 'phase',
            'template': 'CURI%(phase)s',
            'descr': 'Ток, А'},
        5: {'name': 'power_factor', 'phase_tariff': 'phase',
            'template': 'COS_PHI%(phase)s',
            'descr': 'Коэффициент мощности (cos phi), %'},
        6: {'name': 'frequency', 'template': 'FREQ', 'descr': 'Частота, Гц'},
    }
    timestamp = payload_data.pop('2.17.0')
    device = payload_data.pop('2.17.1')
    param_old = {}
    for i in payload_data:
        i_splitted = list(i.split('.'))
        param_id = int(i_splitted[3])
        character = int(i_splitted[4])
        phase_tariff = int(i_splitted[5])
        value = payload_data[i]
        param_info = ELECTRICITY_PARAMS[param_id]
        value_trans = {
            'name': param_info['name'],
            'character': ELECTRICITY_CHARACTER['values'][character]['name'] if character < 3 else None,
            'phase': ELECTRICITY_PHASE_TARIFF['PHASE_VALUES'][phase_tariff]['name'],
            'tariff': ELECTRICITY_PHASE_TARIFF['TARIFF_VALUES'][phase_tariff]['name'],
        }

        value_name = param_info['template'] % value_trans
        param_old[value_name] = value
    return {'electric': {
        'TIMESTAMP': timestamp,
        'PARAMS': param_old,

    }}


def set_tamper(payload_data):
    TAMPER_EVENT = {
        0: u"Геркон замкнут",
        1: u"Крышка открыта",
        4: u"Геркон разомкнут",
        5: u"Крышка закрыта"
    }
    events_count = payload_data.pop('2.7.1.255')
    events = []
    for i in range(int(events_count)):
        event_time = payload_data['2.7.1.{}.0'.format(i)]
        events_list = [j for j in range(6) if '2.7.1.{}.1.{}'.format(i, j) in payload_data]
        events_dict = {k: TAMPER_EVENT[k] for k in events_list}
        events.append({'events': events_dict, 'timestamp': event_time})
    return {'tamper': {
        'event_count': events_count,
        'events': events
    }}


def set_archive(payload_data):
    cnt_id = list(list(payload_data.keys())[0].split('.'))[1]
    timestamp = payload_data.pop('2.{}.0'.format(cnt_id))
    a = datetime.datetime.strptime(timestamp, '%Y-%m-%d %H:%M:%S')
    timestamp = datetime.datetime(a.year, a.month, a.day, a.hour, a.minute, a.second, tzinfo=pytz.UTC)
    values = [0] * 24
    for i in payload_data:
        i_split = list(i.split('.'))
        values[int(i_split[-1])] = payload_data[i]
    return {
        'archive, day, CNT{}'.format(int(cnt_id) - 1): {
            'timestamp': timestamp,
            'values': values
        }
    }


def set_lighting(payload_data):
    PARAMS_OLD = {
        0: {'size': 8, 'name': 'position_simple', 'format': 'II', 'descr': u'packed data'},
        1: {'size': 1, 'name': 'temperature', 'format': 'b', 'descr': u'degC'},
        2: {'size': 2, 'name': 'temp_min_max', 'format': 'bb', 'descr': u'degC'},
        3: {'size': 2, 'name': 'phase', 'format': 'BH', 'descr': u'мкс относительно 1PPS на начало часа'},
        4: {'size': 2, 'name': 'frequency', 'format': 'H', 'descr': u'Гц, коэффициент 0,01', 'factor': 100},
        5: {'size': 2, 'name': 'voltage0', 'format': 'H', 'descr': u'В, коэффициент x0.1', 'factor': 10},
        6: {'size': 2, 'name': 'current0', 'format': 'H', 'descr': u'мА'},
        7: {'size': 4, 'name': 'energy_A+', 'format': 'I', 'descr': u'кВт, коэффициент 0,01', 'factor': 100},
        8: {'size': 4, 'name': 'energy_A-', 'format': 'I', 'descr': u'кВт, коэффициент 0,01', 'factor': 100},
        9: {'size': 4, 'name': 'energy_R+', 'format': 'I', 'descr': u'кВАр, коэффициент 0,01', 'factor': 100},
        10: {'size': 4, 'name': 'energy_R-', 'format': 'I', 'descr': u'кВАр, коэффициент 0,01', 'factor': 100},
        21: {'size': 2, 'name': 'illumination', 'format': 'H', 'descr': u'попугаи, uint16_t'},
        22: {'size': 4, 'name': 'worktime_lighting', 'format': 'I', 'descr': u'сек'},
        23: {'size': 4, 'name': 'worktime_lamp', 'format': 'I', 'descr': u'сек'},
        32: {'size': 9, 'name': 'schedule_lighting', 'format': 'xxxxxxxbQ', 'descr': u'packed data'},
    }

    timestamp = payload_data.pop('2.16.0')
    a = datetime.datetime.strptime(timestamp, '%Y-%m-%d %H:%M:%S')
    timestamp = datetime.datetime(a.year, a.month, a.day, a.hour, a.minute, a.second, tzinfo=pytz.UTC)
    params = {}
    params['position_simple'] = {}
    param_id = list(payload_data.keys())[0].split('.')[3]
    if param_id == '32':
        cell_index = payload_data.pop('2.16.1.32.254')
        params['schedule_lighting'] = {
            'schedule': set_schedule(payload_data),
            'cell_index': cell_index
        }
    else:
        for i in payload_data:
            i_splitted = list(i.split('.'))
            param_id = int(i_splitted[3])

            if i == '2.16.1.0.1':
                params['position_simple'].update({
                    'latitude': payload_data[i]
                })
            elif i == '2.16.1.0.2':
                params['position_simple'].update({
                    'longitude': payload_data[i]
                })
            else:
                factor = PARAMS_OLD[param_id].get('factor', 1)
                params[PARAMS_OLD[param_id]['name']] = payload_data[i] * factor
    params_res = {k: v for k, v in params.items() if v}
    return {
        'lighting': {
            'TIMESTAMP': timestamp,
            'PARAMS': params_res
        }
    }


def set_regular(payload_data):
    timestamp = payload_data.pop('2.0.0')
    a = datetime.datetime.strptime(timestamp, '%Y-%m-%d %H:%M:%S')
    timestamp = datetime.datetime(a.year, a.month, a.day, a.hour, a.minute, a.second, tzinfo=pytz.UTC)
    res = {
        'TIMESTAMP': timestamp
    }
    for i in payload_data:
        i_splitted = list(i.split('.'))
        counter_id = int(i_splitted[3])
        res['COUNTER{}'.format(counter_id + 1)] = payload_data[i]
    return {
        'regular': res
    }


def set_service(payload_data):
    SERVICE_TYPE_OLD = {
        0: u"Информация о версии FW и причина перезагрузки, этот payload отправляется при включении или перезагрузке",
        1: u"Информация о времени работы в режиме передачи, отправляется по запросу",
        2: u"CFG RESULT, отправляется как ответ на входящий пакет с конфигурацией",
        3: u"CFG_ID, отправляется как ответ на входящий пакет с конфигурацией, которая имеет идентификатор (версию)",
        4: u"CUR FW, отправляется как ответ (состояние и запрос следующего блока) на входящий пакет обновления FW",
        5: u"Значение напряжения батареи и температура, отправляется по запросу",
        6: u"RSSI_SNR_INFO, информация об RSSI и SNR для последнеого принятого пакета",
        # 7: u"Serial driver event ( событие от драйвера последовательного порта ( обычно это информация об ошибке ))",
        # 8: u"Калибровочные данные счетчиков с оптическим методом съема"
    }
    FW_VER_RESET_SOURCE = {
        0: u"Firewall",
        1: u"Options Clrd",
        2: u"Reset Pin",
        3: u"POR",
        4: u"By software",
        5: u"I-watchdog",
        6: u"W-watchdog",
        7: u"low power"
    }
    CFG_PL_HEADER_FORMAT = {
        0: {"name": "SYSTEM_CFG", "descr": u"Время, перезагрузка и т.д."},
        1: {"name": "SCHEDULERS", "descr": u"Планировщики (циклические и др., которые не относятся ко входам)"},
        2: {"name": "DI_CFG", "descr": u"Конфиг. дискретных/счетных входов"},
        3: {"name": "DO_CFG", "descr": u"Конфиг. дискретных/импульсных выходов"},
        4: {"name": "AI_CFG", "descr": u"Конфиг. аналоговых входов"},
        5: {"name": "AO_CFG", "descr": u"Конфиг. аналоговых выходов"},
        6: {"name": "SERIAL_CFG", "descr": u"Конфиг. устройств на последовательном порту (включая RS485)"},
        7: {"name": "ARCHIVES", "descr": u"Конфигурация архивирования"},
        8: {"name": "LIGHT_CONTROL", "descr": u"Конфиг. управления освещением"},
        16: {"name": "REQUEST_ARC", "descr": u"Запрос данных из архивов"}
    }
    SERVICE_NAMES = {
        '2.8.0.0': 'git_sha1',
        '2.8.0.1': 'fw_ver',
        '2.8.0.4': 'address',
        '2.8.3.0': 'block_id',
        '2.8.3.1': 'cfg_id',
        '2.8.6.0': 'rssi',
        '2.8.6.1': 'snr'
    }
    service_value = {}
    type_i = None
    type_id = list(payload_data.keys())[0].split('.')[2]
    service_value[type_id] = {}
    if type_id == "0":
        service_value[type_id]['reset_source'] = {i[-1]: FW_VER_RESET_SOURCE[int(i[-1])] for i in
                                                  list(payload_data.keys()) if
                                                  i.startswith('2.8.0.2.')}
        service_value[type_id]['git_sha1'] = payload_data['2.8.0.0']
        service_value[type_id]['fw_ver'] = payload_data['2.8.0.1']
        service_value[type_id]['address'] = payload_data['2.8.0.4']
    elif type_id == "2":
        service_value[type_id]['data'] = [0] * 16
        for i in payload_data:
            service_value[type_id]['data'][int(i.split('.').pop())] = payload_data[i]
    elif type_id == "3":
        service_value[type_id]['block_id'] = payload_data['2.8.3.0']
        service_value[type_id]['cfg_id'] = payload_data['2.8.3.1']
        service_value[type_id]['format'] = CFG_PL_HEADER_FORMAT[payload_data['2.8.3.0']]
    elif type_id == "6":
        service_value[type_id]['rssi'] = payload_data['2.8.6.0']
        service_value[type_id]['snr'] = payload_data['2.8.6.1']
    service_value['descr'] = SERVICE_TYPE_OLD[int(type_id)]
    return {'service': service_value}


def set_schedule(payload_data):
    SCHEDULE_PARAMS = {
        '2.16.1.32.0': 'prop_mode',
        '2.16.1.32.1': "prop_ifnogeo",
        '2.16.1.32.2': "prop_priority",
        '2.16.1.32.3': "prop_command",
        '2.16.1.32.4': "dimming",
        '2.16.1.32.5': "date_format",
        '2.16.1.32.6': "time_offset",
        '2.16.1.32.7': "week_no",
        '2.16.1.32.8': "month",
        '2.16.1.32.9': "day_of_week",
        '2.16.1.32.10': "date",
    }
    scheduler = {}

    for i in list(payload_data.keys()):
        scheduler[SCHEDULE_PARAMS[i]] = payload_data[i]
    return scheduler


class OldFormatDecoder(object):
    payload_functions = {
        '0': set_regular,
        '1': set_archive,
        '2': set_archive,
        '3': set_archive,
        '7': set_tamper,
        '8': set_service,
        '16': set_lighting,
        '17': set_electricity
    }

    def __init__(self, payload):
        decoder = Decoder(payload)
        decoded_payloads = decoder.decode()
        decoded_payloads.pop('0')

        payloads = decoded_payloads.pop('payloads')
        result = set_flags(decoded_payloads)
        for i in payloads:
            result.update(self.payload_functions[list(i.keys())[0].split('.')[1]](i))
        print(result)
