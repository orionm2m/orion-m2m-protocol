from orionm2m.data.decoder import Decoder
import json
import pprint
import io
import os
import re


class Interpreter(object):

    def __init__(self, payload, lang='en'):
        decoder = Decoder(payload, lang=lang)
        self.decoded_payloads = json.loads(decoder.final_output())
        self.decoded_payloads.pop('description')

        if lang:
            with io.open(
                    os.path.join(os.path.dirname(os.path.abspath(__file__)), "lang/interpret-{}.json".format(lang)),
                    encoding='utf-8', errors='ignore') as json_file:
                self.lang_interpr = json.load(json_file)
        self.electr_descr = {d: self.lang_interpr.pop(d) for d in self.lang_interpr.copy() if
                             len(d.split('.')) > 1 and d.split('.')[1] == '17'}

        self.output = {
            'sequence': self.decoded_payloads.pop("0"),
            'flags': [],
            'payloads': []
        }
        self.interpret_flags()
        self.interpret_payloads()
        pprint.pprint(self.output)

    def to_regular_string(self, simplestring, type_storage=':'):
        simplestring = simplestring.replace('.', '\.')
        simplestring = simplestring.replace('i', '(\d+)')
        simplestring = simplestring.replace('N', '(\d+)')
        simplestring = simplestring.replace('K', '(\d+)')
        return '"({})"{}'.format(simplestring, type_storage)

    def interpret_flags(self):
        flags = self.lang_interpr.pop('Flags')
        for flag_pat in flags:
            flag_pat = self.to_regular_string(flag_pat)
            all_flags = re.findall(flag_pat, json.dumps(self.decoded_payloads))
            for f in all_flags:
                self.decoded_payloads.pop(f)
                self.output['flags'].append(flags[f])

    def interpret_payloads(self):
        result = {
            'payloads': []
        }
        pat = r'"(2\.(\d+)\.(\d+)\.?(\d+)?\.?(\d+)?\.?(\d+)?\.?(\d+)?\.?(\d+)?\.?(\d+)?\.?(\d+)?)": '
        all_plds = re.findall(pat, json.dumps(self.decoded_payloads))
        payloads_sorted = {}
        [payloads_sorted.setdefault((a[1], a[2], a[3]), []).append(a[0]) for a in all_plds]
        stg = []
        new_payloads = {}
        for i in payloads_sorted:
            for pattern_payload in self.lang_interpr:
                pattern_payload1 = self.to_regular_string(pattern_payload, "")
                all_res = re.findall(pattern_payload1, json.dumps(payloads_sorted[i]))

                if all_res:
                    for j in all_res:
                        val = j if isinstance(j, str) else j[0]
                        new_payloads.setdefault(i, {}).update({self.lang_interpr[pattern_payload]: j})
        for np in new_payloads:
            if np[1] == '0':
                result.update({key: self.decoded_payloads[new_payloads[np][key]] for key in new_payloads[np]})
            else:
                payload_type = self.lang_interpr['2.{}'.format(np[0])]
                result['payloads'].append(
                    [(key, self.decoded_payloads[new_payloads[np][key][0]]) for key in new_payloads[np]
                     ])
                result['payloads'][-1].insert(0, ('payload_type', payload_type))
        self.output.update(
            result
        )


