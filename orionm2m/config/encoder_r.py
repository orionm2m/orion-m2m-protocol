# = -*- coding: utf-8 -*-
from __future__ import print_function, division

import binascii
import io
import json
import logging
import os
import re
import struct
from collections import defaultdict
from datetime import datetime
import pytz


class InvalidAddressError(Exception):
    pass


class InvalidFormatOrValueError(Exception):
    pass


class InvalidHexInputError(Exception):
    pass


class RepeatedIndexesError(Exception):
    pass


class Encoder(object):
    PACKET_FLAGS = r'"(0.1.(\d+))":'
    PAYLOAD_ADDRESS_PATTERN = r'"(1\.\d+.({}).*?)":'
    PAYLOADS = {
        '0': 'SystemConfig',
        '2': 'DIConfig',
        '6': 'SerialDriverConfig',
        '7': 'SerialPortConfig',
        '8': 'LightControl',
        '9': 'SensorsConfig',
        '16': 'RequestArchiveConfig',
        '17': 'RequestServiceInfoConfig',
    }

    def __init__(self, input_dict=None):
        if not input_dict:
            with io.open(
                    os.path.join(os.path.dirname(os.path.abspath(__file__)), "input.json"),
                    encoding='utf-8', errors='ignore') as json_file:
                json_file = json.load(json_file, encoding='utf-8')
        else:
            json_file = input_dict
        # if there isn't sequence number in input data
        if '0.0' not in json_file:
            raise InvalidFormatOrValueError('Sequence of the packet is not defined')
        seq = json_file.pop('0.0')
        self.all_addresses = json_file
        self.input = json.dumps(json_file)
        # common beginning of the packet. 78 - protocol number
        self.output = '78' + format(seq, '02x') + self.encode_flags()
        self.payloads_with_indexes = {}

    def encode_flags(self):
        # get list of flags and convert them all to integer
        flags_data = {key: self.all_addresses[key[0]] for key in re.findall(self.PACKET_FLAGS, self.input)}
        flags_list = []
        # if value of flag !=1
        for i in flags_data:
            if flags_data[i] == 1:
                flags_list.append(int(i[1]))
        flags_bin = ['0'] * 8
        for i in flags_list:
            # max length of flags is 1 byte
            if i > 7:
                raise InvalidAddressError('Not valid flag id: {}'.format(i))
            # according to protocol only flags with id 0 and 1 are in use for now.
            elif i not in [0, 1]:
                logging.warning('Flag id {} is not used'.format(i))
            flags_bin[i] = '1'
        flags_bin = flags_bin[::-1]
        flags_bin = ''.join(flags_bin)
        flags = int(flags_bin, 2)
        return format(flags, '02x')

    def list_received_payloads(self):
        # get payload id of all input commands and remove duplicates
        all_payload_ids = list(self.PAYLOADS.keys())

        payload_ids = list(
            set([x[1] for x in re.findall(self.PAYLOAD_ADDRESS_PATTERN.format('|'.join(all_payload_ids)), self.input)]))

        return payload_ids

    def retrieve_payload_data(self):
        payload_ids = self.list_received_payloads()
        # iterate through existing payload id
        for i in payload_ids:
            # throw exception if there is no such payload id
            if i not in self.PAYLOADS:
                raise InvalidAddressError('No such payload id: {}'.format(i))
            payload_name = self.PAYLOADS[i]
            # retrieve all addresses of specified payload id and pass it to its encoder class
            payload_addresses = dict(re.findall(self.PAYLOAD_ADDRESS_PATTERN.format(i), self.input))
            payload_data = {key: self.all_addresses[key] for key in payload_addresses}

            encoder_class = globals()[payload_name](payload_data)
            # encoder class will return dict with payload indexes as keys and their packed data as values
            self.payloads_with_indexes.update(encoder_class.payload_commands)

    def encode(self):
        # main function that sets packed data to each payload
        self.retrieve_payload_data()
        # sort payloads by indexes and append their values to output string
        for ind, value in sorted(self.payloads_with_indexes.items()):
            self.output += value
        return self.output.upper()

    def __str__(self):
        return self.encode()


class BasePayload(object):
    COMMANDS_DATA = {}
    command_pattern = ''
    BITS_IN_FORMATS = {
        'I': 32,
        'B': 8,
        'H': 16
    }
    DEFAULT_TIMESTAMP_OFFSET = 946684800
    DEFAULT_DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S'

    def __init__(self, all_addresses):
        self.all_addresses = all_addresses
        self.payload_commands = {}

    def bytes_to_hex(self, b):
        return binascii.hexlify(b).decode('utf-8')

    def ncd_integer(self, value, command_info=None, param_format=None):
        if not param_format:
            param_format = self.COMMANDS_DATA[command_info[-1]]['format']
        try:
            return self.bytes_to_hex(struct.pack(param_format, int(value)))
        except struct.error:
            raise InvalidFormatOrValueError(
                'Not valid value for format. Value - {}, expected format - {}'.format(value, param_format))

    def ncd_none(self, value, command_info):
        return ''

    def pack_bits_or_bytes(self, param_format, param_value, address=None):
        # if format is bits format (like '02b')
        if len(param_format) > 1:
            return format(param_value, param_format)[::-1]
        # if bytes format (like 'B')
        return self.bytes_to_hex(self.pack_bytes(param_format, param_value, address))

    def handle_errors(self, command, payload_index, command_index, command_id, value):
        if payload_index in self.payload_commands and command_index in self.payload_commands[
            payload_index] and format(command_id, '02x') != self.payload_commands[payload_index][
                                                                command_index][:2]:
            raise RepeatedIndexesError('Command with same address as {} already exists'.format(command))

    def check_allowed_value(self, value, allowed_values_range, address, allowed_values_special=None):
        allowed_values_special = [] if not allowed_values_special else allowed_values_special
        if value not in allowed_values_special:
            if value < allowed_values_range[0] or value > allowed_values_range[1]:
                raise InvalidFormatOrValueError('Not valid value for address {}'.format(address))

    def build_output_string(self):
        pass

    def pack_bytes(self, format, value, address=None):
        try:
            return struct.pack(format, value)
        except struct.error:
            raise InvalidFormatOrValueError('Not valid value for address {}'.format(address))

    def pack_dev_cfg(self, addresses, data, command_id_location=1):
        value_bin = []
        for i in addresses:
            allowed_values_range = data['commands'][i[command_id_location]]['allowed_values_range']
            allowed_values_special = data['commands'][i[command_id_location]][
                'allowed_values_special'] if 'allowed_values_special' in \
                                             data['commands'][
                                                 i[command_id_location]] else None
            self.check_allowed_value(i[-1], allowed_values_range, i[0], allowed_values_special)
            value_bin.append(format(i[-1], data['commands'][i[command_id_location]]['format'])[::-1])

        if 'rfu' in data:
            value_bin.insert(data['rfu'][0], '0' * data['rfu'][1])
        return self.ncd_integer(int(''.join(value_bin)[::-1], 2), param_format=data['format'])

    def pack_addr(self, addresses, command_id):
        if isinstance(command_id, dict):
            param_format = command_id['format']
        else:
            param_format = self.COMMANDS_DATA[command_id]['format']
        return self.ncd_integer(addresses[0][-1], None, param_format)

    def commands_pack(self):
        for command in self.all_addresses:
            # enclose address of command in single quotes to define start and end of string
            address = re.findall(self.command_pattern, "'{}'".format(command))
            # if the command address does not match pattern then this address is incorrect
            if not address:
                raise InvalidAddressError('Not valid address: {}'.format(command))
            # the processing of command. Retrieved indexes and id of command
            payload_index, command_index, command_id = [int(v) for v in address[0]]
            # if command id is not valid
            if command_id not in self.COMMANDS_DATA:
                raise InvalidAddressError('Not valid command id: {} in address {}'.format(command_id, command))
            # if command id is ok, get command info and value
            command_info = self.COMMANDS_DATA[command_id]
            value = self.all_addresses[command]
            # check for possible exceptions. if there is any exception, program will raise it and terminate
            self.handle_errors(command, payload_index, command_index, command_id,
                               value)
            # handler function returns packed string, which is then concatenated with command id
            value_packed = format(command_id, '02x') + getattr(self, command_info['handler'])(
                value, [payload_index, command_index, command_id])
            # set packed value to its place according to its payload index and command index
            if command_index not in self.payload_commands.setdefault(payload_index, {}):
                self.payload_commands[payload_index][command_index] = value_packed
        self.build_output_string()


class SystemConfig(BasePayload):
    COMMANDS_DATA = {
        0: {'format': 'I', 'handler': 'ncd_integer', 'name': 'SET DATETIME'},
        1: {'format': '1', 'handler': 'ncd_none', 'name': 'REBOOT', 'allowed_values': [1]},
        2: {'format': 'I', 'handler': 'ncd_hex', 'name': 'DEACTIVATE'},
        3: {'format': 'H', 'handler': 'ncd_integer', 'name': 'BEACON_ITV'},
        4: {'format': 'bb', 'handler': 'ncd_list', 'name': 'TEMPER_THR', 'child_commands': ['0', '1']},
        5: {'format': 'B', 'handler': 'ncd_integer', 'name': 'TAMPER_RST'},
        6: {'format': 'b', 'handler': 'ncd_integer', 'name': 'UTC_OFFSET'},
        8: {'format': 'H', 'handler': 'ncd_integer', 'name': 'REBOOT_AND_FSK_ACTIVATE'},
        9: {'format': 'II', 'handler': 'ncd_list', 'name': 'SET_GEOPOS', 'child_commands': ['0', '1']},
        10: {'format': '1', 'handler': 'ncd_none', 'name': 'RESET_TMINMAX', 'allowed_values': [1]},
        11: {'format': 'I', 'handler': 'ncd_hex', 'name': 'FACTORY_RESET'},
        12: {'format': 'B', 'handler': 'ncd_integer', 'name': 'BATTERY_CAPACITY_MEASUREMENT',
             'allowed_values': [0, 1]},
        13: {'format': '1', 'handler': 'ncd_none', 'name': 'FORCE_RECAL', 'allowed_values': [1]}
    }
    command_pattern = r"'1.(\d+).0.(\d+).(\d+).?\d?'"

    def __init__(self, all_addresses):
        super(SystemConfig, self).__init__(all_addresses)
        self.commands_pack()

    def ncd_hex(self, value, command_info):
        if re.search(r'^[0-9A-Fa-f]{8}$', value):
            hex_value = int(value, 16)
            return self.ncd_integer(hex_value, command_info)
        raise InvalidHexInputError('Invalid value for 4-byte hex {}'.format(value))

    def ncd_list(self, value, command_info):
        f = self.COMMANDS_DATA[command_info[-1]]['format']
        command_id_pattern = r'"(1.{0}.0.{1}.{2}.(\d+))":'.format(*command_info)
        # got tuples of relative address and its last digit. Sorted them by last digit (4.0 should come before 4.1)
        relatives = sorted(re.findall(command_id_pattern, json.dumps(self.all_addresses)),
                           key=lambda x: int(x[1]))
        # if some of relative addresses are not valid then exception will throw
        not_valid_addrs = []
        for addr in relatives:
            if addr[1] not in self.COMMANDS_DATA[command_info[2]]['child_commands']:
                not_valid_addrs.append(addr[0])
        if not_valid_addrs:
            raise InvalidAddressError('Not valid addresses: {}'.format(not_valid_addrs))
        # got values of addresses, converted them to int and packed
        relatives_addresses = [k[0] for k in relatives]
        values = [self.all_addresses[relative] for relative in relatives_addresses]
        try:
            return self.bytes_to_hex(struct.pack(f, *values))
        except struct.error:
            raise InvalidFormatOrValueError(
                'Not valid value for format. Value - {}, expected format - {}'.format(values, f))

    def handle_errors(self, command, payload_index, command_index, command_id, value):
        # if value of command are not valid:
        if 'allowed_values' in self.COMMANDS_DATA[command_id]:
            if value not in self.COMMANDS_DATA[command_id]['allowed_values']:
                raise InvalidFormatOrValueError('Not allowed value for address', command)
        super(SystemConfig, self).handle_errors(command, payload_index, command_index, command_id, value)

    def build_output_string(self):
        # sort commands inside each payload by command index

        self.payload_commands = {key: ''.join([i[1] for i in sorted(self.payload_commands[key].items())]) for key in
                                 self.payload_commands}
        # final output string, containing payload id (00 for SystemConfig), data length in bytes and packed value
        self.payload_commands = {key: '00' + format(int(len(self.payload_commands[key]) / 2), '02x') +
                                      self.payload_commands[key] for key in self.payload_commands}


class DIConfig(BasePayload):
    COMMANDS_DATA = {
        0: {'format': 'i', 'name': 'SET CNT ABSOLUTE', 'handler': 'ncd_integer'},
        1: {'format': 'i', 'name': 'SET CNT RELATIVE', 'handler': 'ncd_integer'},
        2: {'format': 'I', 'name': 'SET SEND INTERVAL', 'handler': 'ncd_integer24bit',
            'allowed_values_range': [0, 16777215]},
        3: {'format': 'H', 'name': 'SET SAVE INTERVAL', 'handler': 'ncd_integer'},
        4: {'format': 'B', 'name': 'SET INPUT TYPE',
            'child_commands': {'0': '02b', '1': '02b', '2': '02b', '3': '02b'},
            'handler': 'ncd_bits', 'allowed_values_range': {'0': [0, 3], '1': [0, 3], '2': [0, 3], '3': [0, 3]}},
        6: {'format': 'H', 'name': 'COUNTER COEFF', 'child_commands': {'0': '015b', '1': 'b'},
            'handler': 'ncd_bits', 'allowed_values_range': {'0': [0, 32767], '1': [0, 1]}},
        10: {'format': 'B', 'name': 'ACTIVATION', 'handler': 'ncd_bits_exclusive'},
    }
    command_pattern = r"'1.(\d+).2.\d+.\d+.(\d+).(\d+).?\d?'"

    def __init__(self, all_addresses):
        super(DIConfig, self).__init__(all_addresses)
        self.input_nums = {}
        self.cfg_ids = {}
        self.commands_pack()

    def ncd_integer24bit(self, value, command_info):
        value = int(value)
        command = '1.{}.2.{}.{}.{}'.format(command_info[0], self.input_nums[command_info[0]],
                                           self.cfg_ids[command_info[0]], command_info[1])
        param_format = self.COMMANDS_DATA[command_info[-1]]['format']
        allowed_value_range = self.COMMANDS_DATA[command_info[-1]]['allowed_values_range']
        self.check_allowed_value(value, allowed_value_range, command)
        return self.bytes_to_hex(struct.pack(param_format, value)[:-1])

    def ncd_bits(self, value, command_info):
        command_id_pattern = r'"(1.{0}.2.{1}.{2}.{3}.{4}.(\d+))":'.format(command_info[0],
                                                                          self.input_nums[command_info[0]],
                                                                          self.cfg_ids[command_info[0]],
                                                                          command_info[1],
                                                                          command_info[-1])
        # got tuples of relative address and its last digit. Sorted them by last digit (4.0 should come before 4.1)
        relatives = sorted(re.findall(command_id_pattern, json.dumps(self.all_addresses)), key=lambda x: int(x[1]))
        values = []
        # if not all child commands are found (example: 4.0, 4.1, etc)
        if len(relatives) != len(self.COMMANDS_DATA[command_info[-1]]['child_commands']):
            raise InvalidAddressError('Not enough parameters for command {}'.format(command_info[-1]))

        # get format of each child command and get binary of its values
        for r in relatives:
            if r[1] in self.COMMANDS_DATA[command_info[-1]]['child_commands']:
                # check if value is in range of allowed values
                allowed_values_range = self.COMMANDS_DATA[command_info[-1]]['allowed_values_range'][r[1]]
                value = int(self.all_addresses[r[0]])
                self.check_allowed_value(value, allowed_values_range, r[0])
                values.append(format(value, self.COMMANDS_DATA[command_info[-1]]['child_commands'][r[1]])[::-1])
        value = ''.join(values)[::-1]
        value = int(value, 2)
        return self.ncd_integer(value, command_info)

    def ncd_bits_exclusive(self, value, command_info):
        value_bin = ['0'] * 8
        value = int(value)
        # because range of parameter is 1 byte
        if value > 7:
            raise InvalidFormatOrValueError('Activation param id should be in range 0-7')
        # all bits after 2 are RFU
        if value > 2:
            logging.warning('Activation param id {} id not in use yet'.format(value))
        value_bin[value] = '1'
        value = int(''.join(value_bin[::-1]), 2)
        return self.ncd_integer(value, command_info)

    def handle_errors(self, command, payload_index, command_index, command_id, value):
        input_num, cfg_id = [int(v) for v in
                             re.findall(r"'1.\d+.2.(\d+).(\d+).\d+.\d+.?\d?'", "'{}'".format(command))[0]]
        # if input number or cfg id is out of range
        if input_num < 0 or input_num > 2:
            raise InvalidAddressError(
                'Not valid input number in address {}. Input number is 0 for Arzamas and [0...2] to Meter'.format(
                    command))
        if cfg_id < 0 or cfg_id > 255:
            raise InvalidAddressError(
                'Not valid cfg id in address {}.'.format(
                    command))
        if cfg_id == 0:
            logging.warning('Current config will not be applied')
        # if this payload has another input num
        if payload_index in self.input_nums and input_num != self.input_nums[payload_index]:
            raise InvalidAddressError(
                'Not valid input number in address {}. Payload #{} has input number = {}. If it is required to assign '
                'different input numbers it is necessary to separate config payloads.'.format(command,
                                                                                              payload_index,
                                                                                              self.input_nums[
                                                                                                  payload_index]))
        # if this payload has another cfg id
        if payload_index in self.cfg_ids and cfg_id != self.cfg_ids[payload_index]:
            raise InvalidAddressError(
                'Not valid cfg id in address {}. Payload #{} has cfg id = {}. If it is required to assign different '
                'cfg id it is necessary to separate config payloads.'.format(command, payload_index,
                                                                             self.cfg_ids[payload_index]))
        # check if another command with same payload index and command index already exists.
        super(DIConfig, self).handle_errors(command, payload_index, command_index, command_id, value)
        self.input_nums[payload_index] = input_num
        self.cfg_ids[payload_index] = cfg_id

    def build_output_string(self):
        # sort commands inside each payload by command index
        self.payload_commands = {key: sorted(self.payload_commands[key].items()) for key in
                                 self.payload_commands}
        # construct final output string for each payload index
        for payload_index in self.payload_commands:
            output = ''
            for command_index, command_data in self.payload_commands[payload_index]:
                output += command_data
            # adding payload id (02 for DI), length of string, input num, cfg id and output string for each payload
            self.payload_commands[payload_index] = '02' + format(int(len(output) / 2) + 2, '02x') + format(
                self.input_nums[payload_index], '02x') + format(self.cfg_ids[payload_index], '02x') + output


class LightControl(BasePayload):
    command_pattern = r'"(1.(\d+).8.(\d+).(\d+).(\d+).(\d+).?\d*.?\d*)":'
    VALID_LIGHTING_PARAMS = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 21, 22, 23, 32]
    COMMANDS_DATA = {
        0: {'handler': 'pack_schedule_record',
            'child_commands_handlers': {
                ('pack_cmd', 0): {'0': {'format': '02b', 'allowed_values_range': [1, 3]},
                                  '1': {'format': '01b', 'allowed_values_range': [0, 1]},
                                  '2': {'format': '02b', 'allowed_values_range': [0, 3]},
                                  '3': {'format': '03b', 'allowed_values_range': [0, 2]}, 'format': 'B',
                                  'optional': False},
                ('pack_flags_time', 1): {'4': {'format': '01b', 'allowed_values_range': [0, 1]},
                                         '5': {'format': '01b', 'allowed_values_range': [0, 1]},
                                         '6': {'format': '02b', 'allowed_values_range': [0, 3]},
                                         '7': {'format': '017b', 'allowed_values_range': [0, 86399]}, '0': None,
                                         'format': 'B', 'optional': False},
                ('pack_date', 2): {'8': {'format': '05b', 'allowed_values_range': [0, 30]}, '6': None, 'format': 'B',
                                   'optional': False},
                ('pack_weekno_month', 3): {'9': {'format': '06b', 'allowed_values_range': [0, 63]}, 'format': 'B',
                                           'optional': False},
                ('pack_dim_level', 4): {'10': {'format': '07b', 'allowed_values_range': [0, 127]}, '5': None,
                                        'format': 'B',
                                        'optional': True},
                ('pack_cell_num', 5): {'11': {'allowed_values_range': [0, 255]}, '4': None, 'format': 'B',
                                       'optional': True}},
            'format': 'B'
            },
        2: {'handler': 'pack_schedule_command', 'format': 'B',
            'child_commands': {'0': {'format': '05b', 'allowed_values_range': [0, 5]},
                               '1': {'format': 'B', 'allowed_values_range': [0, 255]},
                               '2': {'format': 'B', 'allowed_values_range': [0, 255]}}},
        3: {'handler': 'pack_send_interval', 'format': 'I'},
        4: {'handler': 'pack_array'},
        5: {'handler': 'pack_array'},
        7: {'handler': 'pack_hour_int', 'format': 'B'},
        16: {'handler': 'pack_control',
             'child_commands_handlers': {
                 ('pack_remote_control_cmd', 0): {'0': {'format': '03b', 'allowed_values_range': [0, 2]},
                                                  '1': {'format': '01b', 'allowed_values_range': [0, 1]}, 'format': 'B',
                                                  'optional': False},
                 ('pack_stop_scheduler', 1): {'2': {'format': 'H', 'allowed_values_range': [0, 2048]},
                                              'optional': False},
                 ('pack_dim_level', 2): {'3': {'format': '07b', 'allowed_values_range': [0, 127]}, '1': None,
                                         'format': 'B', 'optional': True}},
             'format': None}
    }

    def pack_array(self, data, command_id):
        command_pattern = '"(1.\d+.8.\d+.\d+.\d+.{}.?(\d*))"'.format(command_id)
        command_params = re.findall(command_pattern, json.dumps(data))
        param_ids_list = [int(c[1]) for c in command_params if c[1] and self.all_addresses[c[0]] == 1]
        value_bin = ['0'] * 32
        for param_id in param_ids_list:
            value_bin[param_id] = '1'
        value_int = int(''.join(value_bin)[::-1], 2)
        res = self.ncd_integer(value_int, param_format='I')
        return res

    def __init__(self, all_addresses):
        super(LightControl, self).__init__(all_addresses)
        self.input_nums = {}
        self.cfg_ids = {}
        self.commands_pack()

    def check_all_child(self, data, child_list):
        opt = child_list.pop('optional')
        f = child_list.pop('format') if 'format' in child_list else None

        for child in child_list:
            if child not in [i[1] for i in data]:
                raise InvalidAddressError('Not enough data: Missed command {}'.format(child))

    def check_allowed_value_lighting(self, addresses, param_formats):
        for i in addresses:
            if param_formats[i[1]] and 'allowed_values_range' in param_formats[i[1]]:
                value = i[2]
                allowed_values = param_formats[i[1]]['allowed_values_range']
                self.check_allowed_value(value, allowed_values, i[0])

    def pack_cmd(self, addresses, param_formats):
        value_bin = ''
        for i in addresses:
            value_int = i[2]
            value_bin += format(value_int, param_formats[i[1]]['format'])[::-1]
        return self.bytes_to_hex(self.pack_bytes(param_formats['format'], int(value_bin[::-1], 2), i[0]))

    def pack_flags_time(self, addresses, param_formats):
        value_bin = []
        time_value = None
        schedule_mode = None
        for i in addresses:
            value_int = i[2]
            if i[1] == '0':
                schedule_mode = value_int
                continue
            value_command_bin = format(value_int, param_formats[i[1]]['format'])
            if i[1] == '7':
                time_value = value_command_bin
                break
            value_bin += value_command_bin[::-1]
        value_bin.insert(2, '000')
        value_bin += time_value[0]
        time_value_format = '>H' if schedule_mode == 1 else '>h'
        flags_packed = self.bytes_to_hex(
            self.pack_bytes(param_formats['format'], int(''.join(value_bin[::-1]), 2), [i[0] for i in addresses]))
        time_packed = self.bytes_to_hex(
            self.pack_bytes(time_value_format, int(''.join(time_value[1:]), 2), [i[0] for i in addresses]))
        return flags_packed + time_packed

    def pack_date(self, addresses, param_formats):
        date_format_info, date_info = addresses
        if date_format_info[2] == 3:
            if date_info[2] > 30:
                raise InvalidFormatOrValueError('Not valid value for address {}'.format(date_info[0]))
            value_bin = format(date_info[2], param_formats[date_info[1]]['format'])[::-1] + '000'
            return self.bytes_to_hex(self.pack_bytes('B', int(value_bin[::-1], 2), date_info[0]))
        else:
            value_bin = ['0'] * 8
            value_bin[date_info[2]] = '1'
            return self.bytes_to_hex(
                self.pack_bytes(param_formats['format'], int(''.join(value_bin[::-1]), 2), date_info[0]))

    def pack_weekno_month(self, addresses, param_formats):
        value_bin = '00' + format(addresses[0][2], param_formats[addresses[0][1]]['format'])[::-1]
        # need to check for valid date format
        return self.bytes_to_hex(self.pack_bytes(param_formats['format'], int(value_bin[::-1], 2), addresses[0][0]))

    def pack_dim_level(self, addresses, param_formats):
        dim_exists = addresses[0][2]
        if dim_exists == 1:
            if len(addresses) == 1:
                raise InvalidFormatOrValueError('Dimming level is not defined')
            dim_level_int = addresses[1][2]
            dim_level_bin = format(dim_level_int, param_formats[addresses[1][1]]['format'])[::-1] + '0'

            return self.bytes_to_hex(
                self.pack_bytes(param_formats['format'], int(dim_level_bin[::-1], 2), addresses[1][0]))
        elif len(addresses) > 1:
            logging.warning('Dimming level should not be specified, because dimming flag did not come')
        return ''

    def pack_cell_num(self, addresses, param_formats):
        cell_num_exists = addresses[0][2]
        if cell_num_exists == 1:
            if len(addresses) == 1:
                raise InvalidFormatOrValueError('Cell number is not defined')
            cell_num_int = addresses[1][2]

            return self.bytes_to_hex(self.pack_bytes(param_formats['format'], cell_num_int, addresses[1][0]))
        return ''

    def pack_schedule_record(self, data, command_id):
        # there may be multiple schedule records, so we first need to separate addresses with respect to their index
        child_commands_pattern = r"(1.\d+.8.\d+.\d+.\d+.\d+.?(\d+).?(\d+))"
        command_index_with_data = {}
        # regular string will return tuple of whole address, record index and child command id
        # data is saved as dict where keys are record indexes, values are list of addresses with their child command ids
        for i in re.findall(child_commands_pattern, str(data)):
            command_index_with_data.setdefault(i[1], []).append((i[2], i[0]))

        child_commands_handlers = self.COMMANDS_DATA[command_id]['child_commands_handlers']
        # iterate over all records
        for c in command_index_with_data:
            addresses_of_schedule = command_index_with_data[c]
            command_index_with_data[c] = {}
            # process all child commands
            # iterate over child commands handlers and pass needed addresses to them
            for i in child_commands_handlers:
                # get list of required child commands to the handler
                child_commands_list = [k for k in
                                       child_commands_handlers[i].copy().keys() if
                                       k.isdigit()]
                addresses_for_handler_pattern = r'(1.\d+.8.\d+.\d+.\d+.\d+.\d+.({}))"'.format(
                    '|'.join(list(child_commands_list)))
                # get needed addresses
                addresses_for_handler = re.findall(addresses_for_handler_pattern, json.dumps(addresses_of_schedule))
                param_formats = child_commands_handlers[i]
                # sort child commands by their ids. get tuple (full address, child command id)
                addresses_sorted = sorted(addresses_for_handler, key=lambda x: int(x[1]))
                # add value of command to the tuple
                addresses_tuple = [(address, cmd_id, self.all_addresses[address]) for address, cmd_id in
                                   addresses_sorted]
                # check if all child commands exists
                if not param_formats['optional']:
                    self.check_all_child(addresses_tuple, param_formats.copy())
                # check value of command is valid
                self.check_allowed_value_lighting(addresses_tuple, param_formats)
                # store value with handler index
                command_index_with_data[c][i[1]] = getattr(self, i[0])(addresses_tuple,
                                                                       param_formats)
            # sort child commands and join
            command_index_with_data[c] = ''.join([i[1] for i in sorted(command_index_with_data[c].items())])
        value_schedule = ''
        # join all records i correct order
        for i in sorted(command_index_with_data.items(), key=lambda x: int(x[0])):
            value_schedule += i[1]
        header_bin = format((len(command_index_with_data) - 1), '04b')[::-1] + '0000'

        header = self.bytes_to_hex(self.pack_bytes(self.COMMANDS_DATA[command_id]['format'], int(header_bin[::-1], 2)))

        return header + value_schedule

    def pack_schedule_command(self, data, command_id):
        # retrieve child command id with regular
        sch_command_pattern = r"(1.\d+.8.\d+.\d+.\d+.{}.(\d+))".format(command_id)
        # sort addresses by  child command id
        data = sorted(data, key=lambda x: int(re.search(sch_command_pattern, x).group(2)))
        # get tuple (address and child command id)
        addresses = re.findall(sch_command_pattern, str(data))
        # add value to the tuple
        addresses_info = [(i[0], i[1], self.all_addresses[i[0]]) for i in addresses]
        # check values of all commands is valid
        self.check_allowed_value_lighting(addresses_info, self.COMMANDS_DATA[command_id]['child_commands'])
        # get first address
        addr = iter(data)
        command_addr = next(addr)
        # if it is not 0, then error (because it was sorted)
        if re.search(sch_command_pattern, command_addr).group(2) != '0':
            raise InvalidAddressError('Too few arguments for schedule command: command is not specified')
        command_value = self.all_addresses[command_addr]
        param_format = self.COMMANDS_DATA[command_id]['format']
        cell_packed = ''
        # if cell is specified
        if command_value in [2, 3]:
            if len(data) != 3:
                raise InvalidAddressError('Too few arguments for schedule command: info about cell was not specified')
            cell_id_addr = next(addr)
            cell_id_count_addr = next(addr)
            cell_id = self.all_addresses[cell_id_addr]
            cell_id_count = self.all_addresses[cell_id_count_addr]
            cell_packed = self.bytes_to_hex(self.pack_bytes(param_format, cell_id, cell_id_addr)) + self.bytes_to_hex(
                self.pack_bytes(param_format, cell_id_count, cell_id_count_addr))
        # if command does not require cell info but if it was specified
        elif len(data) > 1:
            logging.warning('Too many parameters defined for schedule command')
        command_bin = format(command_value, '03b')[::-1] + '0000'
        command_packed = self.bytes_to_hex(self.pack_bytes(param_format, int(command_bin[::-1], 2), command_addr))
        return command_packed + cell_packed

    def pack_send_interval(self, data, command_id):
        param_format = self.COMMANDS_DATA[command_id]['format']
        addr = next(iter(data))
        value = self.all_addresses[addr]
        if value < 0 or value > 16777215:
            raise InvalidFormatOrValueError('Not valid value for address {}'.format(addr))
        # removing last byte, because format is uint24
        value_packed = self.pack_bytes(param_format, value, addr)[:-1]
        return self.bytes_to_hex(value_packed)

    def pack_hour_int(self, data, command_id):
        param_format = self.COMMANDS_DATA[command_id]['format']
        param_hour_addr = next(iter(data))
        value_int = self.all_addresses[param_hour_addr]
        if value_int < 0 or value_int > 23:
            raise InvalidFormatOrValueError('Invalid value for hour. Only numbers in range 0...23 are accepted')
        return self.bytes_to_hex(self.pack_bytes(param_format, value_int, param_hour_addr))

    def pack_remote_control_cmd(self, addresses, param_formats):
        # they are sorted, so we know their irder
        cmd_info, dim_info = addresses
        cmd_bin = format(cmd_info[2], param_formats[cmd_info[1]]['format'])
        dim_value = '0'  # default value
        if dim_info[0] in self.all_addresses and self.all_addresses[dim_info[0]] == 1:
            dim_value = '1'
        cmd_bin += dim_value + '000'
        return self.bytes_to_hex(
            self.pack_bytes(param_formats['format'], int(cmd_bin[::-1], 2), [cmd_info[0], dim_info[0]]))

    def pack_stop_scheduler(self, addresses, param_formats):
        minutes_info = addresses[0]
        return self.bytes_to_hex(
            self.pack_bytes(param_formats[minutes_info[1]]['format'], minutes_info[2], minutes_info[0]))

    def pack_control(self, data, command_id):
        control_value = {}
        child_commands_pattern = r'"(1.\d+.8.\d+.\d+.\d+.16.({}))"'
        child_commands_handlers = self.COMMANDS_DATA[command_id]['child_commands_handlers']
        for child_handler in child_commands_handlers:
            # for each handler get list of needed child command ids for
            child_handler_ids = list(
                [k for k in child_commands_handlers[child_handler].keys() if k.isdigit()])
            pattern = child_commands_pattern.format('|'.join(child_handler_ids))
            # using regular get addresses needed for this handler and order them

            child_addresses = sorted(re.findall(pattern, json.dumps(data)), key=lambda x: int(x[1]))
            # make tuple of (address, child command id and value)
            child_addresses_info = [(i[0], i[1], self.all_addresses[i[0]]) for i in child_addresses]
            param_formats = child_commands_handlers[child_handler]
            # check all child commands exists. if it isn't optional (like "dimming" or "cell")
            if not param_formats['optional']:
                self.check_all_child(child_addresses_info, param_formats.copy())
            # check values are valid
            self.check_allowed_value_lighting(child_addresses_info, param_formats)
            # if ok, then pass tuple to the handler and pack value
            # store data from handler with handler id, to then join them in correct order
            control_value[child_handler[1]] = getattr(self, child_handler[0])(child_addresses_info, param_formats)
        # return correctly ordered string
        return ''.join([i[1] for i in sorted(control_value.items())])

    def handle_errors(self, command, payload_index, inp_num, cfg_id, command_index, command_id):
        # if input num of address is not valid
        if payload_index in self.input_nums and self.input_nums[payload_index] != inp_num:
            raise InvalidAddressError('Not valid input number {} for payload #{}'.format(inp_num, payload_index))
        # if input num of address is not valid
        if payload_index in self.cfg_ids and self.cfg_ids[payload_index] != cfg_id:
            raise InvalidAddressError('Not valid cfg id {} for payload #{}'.format(cfg_id, payload_index))
        # they are ok, then store them
        self.input_nums[payload_index] = inp_num
        self.cfg_ids[payload_index] = cfg_id
        # if multiple command ids were assigned to the same command index
        if len(self.payload_commands[payload_index][command_index]) > 1:
            raise InvalidAddressError(
                'Not valid command id for #{} command of #{} payload'.format(command_index, payload_index))
        # if not valid command_id
        if command_id not in self.COMMANDS_DATA:
            raise InvalidAddressError('Not valid command id {}'.format(command_id))

    def commands_pack(self):
        # iterate over all addresses and group them by payload index, then command index
        for address, payload_index, inp_num, cfg_id, command_index, command_id in re.findall(self.command_pattern,
                                                                                             json.dumps(
                                                                                                 self.all_addresses)):
            # store address in dict with its indexes
            self.payload_commands.setdefault(int(payload_index), {}).setdefault(int(command_index), {}).setdefault(
                int(command_id), []).append(address)
            # check for errors
            self.handle_errors(address, int(payload_index), int(inp_num), int(cfg_id), int(command_index),
                               int(command_id))
        # iterate over each lighting payload
        for payload_index in self.payload_commands:
            lighting_value = ''
            for command_index, command_id_with_addresses in sorted(self.payload_commands[payload_index].items()):
                command_id = next(iter(command_id_with_addresses))
                # get command id and all addresses of the command and send them to the handler of command
                lighting_value += format(command_id, '02x') + getattr(self, self.COMMANDS_DATA[command_id]['handler'])(
                    command_id_with_addresses[command_id], command_id)
            # after all commands are packed, we add payload id(lighting has 08), packet length, input num and cfg id
            lighting_value = '08' + format(
                int(len(lighting_value) / 2) + 2, '02x') + format(self.input_nums[payload_index], '02x') + format(
                self.cfg_ids[payload_index],
                '02x') + lighting_value
            # store final packet of each payload with payload index
            self.payload_commands[payload_index] = lighting_value


class RequestArchiveConfig(BasePayload):
    command_pattern = r'"(1.(\d+).16.(\d+))":'
    COMMANDS_DATA = {
        ('pack_data_sourse', 0): {
            'commands': {
                '0': {
                    'format': '04b', 'allowed_values_range': [0, 2]
                },
                '1': {
                    'format': '04b', 'allowed_values_range': [0, 3]
                }
            }, 'format': 'B'
        },
        ('pack_datetime', 1): {
            'commands': ['2'], 'format': 'I'
        },
        ('pack_data_sourse', 2): {
            'commands': {
                '3': {'format': '06b', 'allowed_values_range': [0, 64]}
            }, 'rfu': (1, 2), 'format': 'B'
        }
    }

    def __init__(self, all_addresses):
        super(RequestArchiveConfig, self).__init__(all_addresses)
        self.exisiting_payload_indexes = None
        self.commands_pack()

    def pack_data_sourse(self, addresses, data, seq):
        for i in self.exisiting_payload_indexes:
            addrs = sorted([j for j in addresses if j[1] == i], key=lambda x: int(x[2]))
            val = self.pack_dev_cfg(addrs, data, 2)
            self.payload_commands.setdefault(int(i), {}).update({seq: val})

    def pack_datetime(self, addresses, data, seq):
        for i in self.exisiting_payload_indexes:
            addrs = sorted([j for j in addresses if j[1] == i], key=lambda x: int(x[2]))
            value_datetime_str = addrs[0][-1]
            value_datetime = datetime.strptime(value_datetime_str, self.DEFAULT_DATETIME_FORMAT)
            value_timestamp_full = datetime.timestamp(value_datetime.replace(tzinfo=pytz.utc))
            value_timestamp_offset = value_timestamp_full - self.DEFAULT_TIMESTAMP_OFFSET
            val = self.ncd_integer(value_timestamp_offset, param_format=data['format'])
            self.payload_commands.setdefault(int(i), {}).update({seq: val})

    def commands_pack(self):
        addresses_all = re.findall(self.command_pattern, json.dumps(self.all_addresses))
        self.exisiting_payload_indexes = set([i[1] for i in addresses_all])
        for i in sorted(self.COMMANDS_DATA, key=lambda x: int(x[1])):
            commands_for_handler = self.COMMANDS_DATA[i]['commands']
            addresses_for_handler = [k for k in addresses_all if k[2] in commands_for_handler]

            for p in self.exisiting_payload_indexes:
                if not all(elem in [n[2] for n in addresses_for_handler if n[1] == p] for elem in commands_for_handler):
                    raise InvalidAddressError('Not enough data for {} command'.format(i[0]))

            addresses_for_handler_with_values = [(g[0], g[1], g[2], self.all_addresses[g[0]]) for g in
                                                 addresses_for_handler]

            getattr(self, i[0])(addresses_for_handler_with_values, self.COMMANDS_DATA[i], i[1])

        self.payload_commands = {
            key: '10' + format(int(len(''.join([i[1] for i in sorted(self.payload_commands[key].items())])) / 2),
                               '02x') + ''.join([i[1] for i in sorted(self.payload_commands[key].items())]) for key in
            self.payload_commands}


class RequestServiceInfoConfig(BasePayload):
    command_pattern = r'"(1.(\d+).17.(0))":'
    COMMANDS_DATA = {
        '0': {
            'handler': 'pack_addr', 'allowed_values_range': [0, 1], 'format': 'B'
        }
    }

    def __init__(self, all_addresses):
        super(RequestServiceInfoConfig, self).__init__(all_addresses)
        self.commands_pack()

    def commands_pack(self):
        addresses = re.findall(self.command_pattern, json.dumps(self.all_addresses))
        if not addresses:
            raise InvalidAddressError('Not enough data for RequestServiceInfoConfig')
        for addr in addresses:
            data = self.COMMANDS_DATA[addr[2]]
            self.check_allowed_value(self.all_addresses[addr[0]], data['allowed_values_range'], addr[0])
            payload_index = addr[1]
            addr = [(addr[0], self.all_addresses[addr[0]])]
            self.payload_commands[int(payload_index)] = getattr(self, data['handler'])(addr, data)
        self.payload_commands = {
            key: '11' + format(int(len(self.payload_commands[key]) / 2), '02x') + self.payload_commands[key] for key in
            self.payload_commands}


class SensorsConfig(BasePayload):
    command_pattern = r'"(1.(\d+).9.(\d+).2.(\d+).?(\d+)?)":'
    SENSOR_NUMBERS = [0, 1, 2, 3]
    COMMANDS_DATA = {
        '': {'handler': 'pack_params', 'format': 'B'},
        0: {
            'handler': 'pack_bits8', 'format': 'B', 'allowed_value_range': [0, 7], 'child_commands': ''
        },
        2: {
            'handler': 'pack_interval',
            'child_commands': {
                '0': {
                    'format': 'I', 'allowed_value_range': [10, 16777215]
                },
                '1': {
                    'format': 'H', 'allowed_value_range': [600, 65535], 'allowed_values_special': [0, 1, 10]
                },
            }
        },
        3: {
            'handler': 'pack_thresholds', 'format': 'B',
            'child_commands': {
                '0': {'format': '03b', 'allowed_value_range': [0, 6]},
                '1': {'format': '03b', 'allowed_value_range': [0, 6]},
                '2': {
                    (0, 1, 2, 3): {'format': 'H', 'allowed_value_range': [0, 100000]},
                    (4,): {'format': 'B', 'allowed_value_range': [0, 100]},
                    (5, 6): {'format': 'i'}
                }
            }
        },
        4: {
            'handler': 'pack_actions',
            'format': 'B',
            'child_commands': {'0': {'allowed_value_range': [0, 7]}, '1': {'allowed_value_range': [0, 7]}}
        },
        5: {
            'handler': 'pack_actions',
            'format': 'B',
            'child_commands': {'0': {'allowed_value_range': [0, 7]}, '1': {'allowed_value_range': [0, 7]}}
        },
        6: {
            'handler': 'pack_actions',
            'format': 'B',
            'child_commands': {'0': {'allowed_value_range': [0, 7]}, '1': {'allowed_value_range': [0, 7]}}
        },
        7: {
            'handler': 'pack_achiving',
            'child_commands': {
                '0': {'format': '03b', 'allowed_value_range': [0, 6]},
                '1': {'format': '01b', 'allowed_value_range': [0, 1]},
                '2': {'format': 'H', 'optional': True},
                '3': {'format': 'B', 'optional': True},
            }
        },
        8: {
            'handler': 'pack_measured_values', 'format': 'B',
            'child_commands': {
                '0': {'format': '01b', 'allowed_value_range': [0, 1]},
                '1': {'format': '01b', 'allowed_value_range': [0, 1]},
            }
        }
    }

    def __init__(self, all_addresses):
        super(SensorsConfig, self).__init__(all_addresses)
        self.pack_params()
        self.commands_pack()

    def handle_errors(self, commands_with_indexes, payload_index, command_index, command_id, addresses):
        # if command has child commands
        if 'child_commands' in self.COMMANDS_DATA[command_id] and self.COMMANDS_DATA[command_id]['child_commands']:
            # get only not optional child commands
            child_commands_info = {k: self.COMMANDS_DATA[command_id]['child_commands'][k] for k in
                                   self.COMMANDS_DATA[command_id]['child_commands'] if
                                   'optional' not in self.COMMANDS_DATA[command_id]['child_commands'][k]}
            # check if all child commands exists
            if not all(elem in [i[1] for i in addresses] for elem in child_commands_info):
                raise InvalidAddressError('Too few values for parameter {}'.format([i[0] for i in addresses]))
            # check values
            # consider only not optional addresses
            for i in [j for j in addresses if j[1] in child_commands_info]:
                if 'allowed_value_range' in child_commands_info[i[1]]:
                    # if value is list then we should validate all items, so we wrap values to list
                    value = i[2] if isinstance(i[2], list) else [i[2]]
                    # if all values are in special group
                    if 'allowed_values_special' in child_commands_info[i[1]] \
                            and all(elem in child_commands_info[i[1]]['allowed_values_special'] for elem in value):
                        continue
                    # if at least one of values is out of range
                    elif any(elem < child_commands_info[i[1]]['allowed_value_range'][0] or elem > \
                             child_commands_info[i[1]]['allowed_value_range'][1] for elem in value):
                        raise InvalidFormatOrValueError('Not valid value for address {}'.format(i[0]))
        # the same validation but for params without child commands
        if 'allowed_value_range' in self.COMMANDS_DATA[command_id]:
            for i in addresses:
                value = i[2] if isinstance(i[2], list) else [i[2]]
                if any(elem < self.COMMANDS_DATA[command_id]['allowed_value_range'][0] or elem > \
                       self.COMMANDS_DATA[command_id]['allowed_value_range'][1] for elem in value):
                    raise InvalidFormatOrValueError('Not valid value for address {}'.format(i[0]))
        # check if indexes are unique
        if [i for i in commands_with_indexes if i[0] == payload_index and i[1] == command_index]:
            raise InvalidAddressError(
                'Param with the same indexes already exists {}.'.format(
                    [i[0] for i in addresses]))

    def pack_params(self):
        params_pattern = r'"(1.(\d+).9.(0|1))":'
        # get all sensor numbers and cgf ids
        all_params = (re.findall(params_pattern, json.dumps(self.all_addresses)))
        # check is sensor numbers are valid
        if any(elem[2] == '0' and self.all_addresses[elem[0]] not in self.SENSOR_NUMBERS for elem in all_params):
            raise InvalidFormatOrValueError('Not valid sensor number')
        # pack ll values
        all_params_values = sorted([
            (i[1], i[2], self.pack_bits_or_bytes('B', self.all_addresses[i[0]], i[0])) for i
            in all_params], key=lambda x: int(x[1]))
        # set joined values of sensor num and cgf id in output dict
        for i in all_params_values:
            self.payload_commands.setdefault(int(i[0]), []).append(i[2])
        self.payload_commands = {key: ''.join(self.payload_commands[key]) for key in self.payload_commands}

    def pack_bits8(self, addresses, command_id, values=None):
        value_bin = ['0'] * 8
        # if values is not specified, then we will get them from addresses. Value is specified in "measured values"
        #                                                                                               param only
        if not values:
            # to allow multiple values
            # if it is int then convert to list or if its is list then do nothing
            values = [addresses[0][2]] if type(addresses[0][2]) == int else addresses[0][2]
        for i in values:
            value_bin[i] = '1'
        param_format = self.COMMANDS_DATA[command_id]['format']
        return self.pack_bits_or_bytes(param_format, int(''.join(value_bin[::-1]), 2), addresses[0][0])

    def pack_interval(self, addresses, command_id):
        child_commands_data = self.COMMANDS_DATA[command_id]['child_commands']
        addr_first, addr_second = [self.pack_bytes(child_commands_data[i[1]]['format'], i[2], i[0]) for i
                                   in addresses]
        # uint24
        addr_first_cut = addr_first[:-1]
        return self.bytes_to_hex(addr_first_cut) + self.bytes_to_hex(addr_second)

    def pack_thresholds(self, addresses, command_id):
        child_commands_data = self.COMMANDS_DATA[command_id]['child_commands']
        addr = iter(addresses)
        # all values are sorted and all child commands exist so we know the order of addresss
        channel_num_info = next(addr)
        channel_num_value_packed = format(channel_num_info[2], child_commands_data[channel_num_info[1]]['format'])[::-1]
        threshold_type_info = next(addr)
        threshold_type_value_packed = format(threshold_type_info[2],
                                             child_commands_data[threshold_type_info[1]]['format'])[::-1]
        threshold_type_full = int((channel_num_value_packed + threshold_type_value_packed + '00')[::-1], 2)
        threshold_type_packed = self.pack_bits_or_bytes(self.COMMANDS_DATA[command_id]['format'], threshold_type_full,
                                                        [channel_num_info[0], threshold_type_info[0]])
        threshold_value = next(addr)
        threshold_packed = ''
        for i in child_commands_data[threshold_value[1]]:
            if threshold_type_info[2] in i:
                param_format = child_commands_data[threshold_value[1]][i]['format']
                threshold_packed = self.pack_bits_or_bytes(param_format, threshold_value[2], threshold_value[0])
                break
        return threshold_type_packed + threshold_packed

    def pack_actions(self, addresses, command_id):
        channel = self.pack_bits8([addresses[0]], command_id)
        threshold_info = addresses[1]
        if isinstance(threshold_info[2], list):
            raise InvalidFormatOrValueError('Value for address {} should not be array'.format(threshold_info[0]))
        threshold = self.pack_bits8([threshold_info], command_id)
        return channel + threshold

    def pack_achiving(self, addresses, command_id):
        child_commands_data = self.COMMANDS_DATA[command_id]['child_commands']
        values = {}
        # get values
        for i in addresses:
            values[i[1]] = (i[0], self.pack_bits_or_bytes(child_commands_data[i[1]]['format'], i[2], i[0]))
        archiving_info_bin = values['0'][1] + '0000' + values['1'][1]
        archiving_info_packed = self.pack_bits_or_bytes('B', int(archiving_info_bin[::-1], 2),
                                                        [values[i][0] for i in values if i in ['0', '1']])
        if '2' in values and '3' in values:
            # if archiving is not activated but values have come
            if values['1'][1] != '1':
                raise InvalidAddressError(
                    'Archiving details should not be defined, because archiving is turned off {}'.format(
                        [values[i][0] for i in values if i in ['2', '3']]))
            archiving_interval = values['2'][1] + values['3'][1]
            return archiving_info_packed + archiving_interval
        # if archiving is activated but values didn't come
        elif not all(elem in values for elem in ['2', '3']) and values['1'][1] == '1':
            raise InvalidAddressError(
                'Archiving details are not defined {}'.format([values[i][0] for i in values]))
        return archiving_info_packed

    def pack_measured_values(self, addresses, command_id):
        # get addresses with value 1
        values = [int(i[1]) for i in addresses if i[2] == 1]
        return self.pack_bits8(addresses, command_id, values)

    def commands_pack(self):
        # dynamic pattern of child commands of the command. Command id will be specified later by 'format' function
        relatives_pattern = r'"(1.{}.9.{}.2.{}.?({})?)"'
        # get all params
        params = re.findall(self.command_pattern, json.dumps(self.all_addresses))
        next_addr_i = 0
        commands_with_indexes = []
        while params:
            # always get first element
            addr = params[next_addr_i]
            payload_index, command_index, command_id = int(addr[1]), int(addr[2]), int(addr[3])
            # get relatives of the address
            relatives = sorted(re.findall(relatives_pattern.format(payload_index, command_index, command_id, '|'.join(
                [k for k in self.COMMANDS_DATA[command_id]['child_commands']])), json.dumps(params)),
                               key=lambda x: int(x[1]) if x[1].isdigit() else x)
            # append values to each relative
            relatives_with_values = ([(i[0], i[1], self.all_addresses[i[0]]) for i in relatives])
            # pass all relatives with its values to the function to handle errors
            self.handle_errors(commands_with_indexes, payload_index, command_index, command_id, relatives_with_values)
            # pass all relatives with its values and pack
            value_packed = getattr(self, self.COMMANDS_DATA[command_id]['handler'])(relatives_with_values, command_id)
            # append command id to beginning of packed value
            commands_with_indexes.append((payload_index, command_index, format(command_id, '02x') + value_packed))
            # remove all addresses those were processed
            params = [k for k in params if k[0] not in [i[0] for i in relatives]]
        # group all commands by their indexes and sort
        d = defaultdict(list)
        for k, v, p in commands_with_indexes:
            d[k].append((v, p))
        # if there is any payloads without sensor number of CFG id
        payloads_without_num = [i for i in d if i not in self.payload_commands]
        if payloads_without_num:
            raise InvalidAddressError(
                'Sensor number of CFG id was not specified for payloads #{}'.format(payloads_without_num))
        self.payload_commands = {
            key: '09' + format(int(len(''.join([j[1] for j in d[key]])) / 2) + 2, '02x') + self.payload_commands[
                key] + ''.join([i[1] for i in sorted(d[key])]) for key in
            self.payload_commands}


class SerialPortConfig(BasePayload):
    command_pattern = r'"(1.(\d+).7.(\d+).(\d+))":'
    COMMANDS_DATA = {
        '1': {
            'format': 'H', 'rfu': (4, 6), 'handler': 'pack_serial_cfg', 'child_commands': {
                '0': {
                    'format': '04b', 'allowed_values_range': [0, 13]
                },
                '1': {
                    'format': '02b', 'allowed_values_range': [0, 2]
                },
                '2': {
                    'format': '02b', 'allowed_values_range': [0, 3]
                },
                '3': {
                    'format': '02b', 'allowed_values_range': [0, 2]
                },
            }
        }
    }

    def __init__(self, all_addresses):
        super(SerialPortConfig, self).__init__(all_addresses)
        self.commands_pack()

    def pack_serial_cfg(self, addresses, data, payload_indexes):
        for i in payload_indexes:
            addrs = sorted([addr for addr in addresses if addr[1] == i and addr[2] in data['child_commands']],
                           key=lambda x: int(x[2]))
            if not all(elem in [add[-2] for add in addrs] for elem in data['child_commands']):
                raise InvalidFormatOrValueError('Not enough data for payload {}'.format(i))
            val_bin = []
            for addr in addrs:
                self.check_allowed_value(addr[-1], data['child_commands'][addr[2]]['allowed_values_range'], addr[0])
                param_format = data['child_commands'][addr[2]]['format']
                val_bin.append(format(addr[-1], param_format)[::-1])
            if 'rfu' in data:
                val_bin.insert(data['rfu'][0], '0' * data['rfu'][1])
            val_bin = ''.join(val_bin)[::-1]
            self.payload_commands[int(i)] += self.ncd_integer(int(val_bin, 2), param_format=data['format'])

    def commands_pack(self):
        addresses_all = re.findall(self.command_pattern, json.dumps(self.all_addresses))
        payload_indexes = set([i[1] for i in addresses_all])
        self.payload_commands = {
            int(key): '0000' for key in payload_indexes}
        for j in self.COMMANDS_DATA:
            addrs = [addr for addr in addresses_all if addr[2] == j]
            addrs_with_values = ([(k[0], k[1], k[3], self.all_addresses[k[0]]) for k in addrs])
            getattr(self, self.COMMANDS_DATA[j]['handler'])(addrs_with_values, self.COMMANDS_DATA[j],
                                                            payload_indexes)
        self.payload_commands = {
            key: '07' + format(int(len(self.payload_commands[key]) / 2), '02x') + self.payload_commands[key] for key in
            self.payload_commands}


class SerialDriverConfig(BasePayload):
    SERIAL_DRIVERS_CONFIG = {
        '3': 'EnergyMeter',
        '2': 'Mercury20',
        '1': 'Mercury23',
        '14': 'Modbus',
        '13': 'SaimanIEC',
        '12': 'SaimanDala',
        '11': 'SaimanPLC',
        '4': 'MarsPulsar',
        '15': 'Mirtek',
        '16': 'Sempal'
    }
    SERIAL_DRIVERS_COMMAND = {
        '2': 'Mercury20Command',
        '13': 'SaimanIECCommand',
        '12': 'SaimanDalaCommand',
        '11': 'SaimanPLCCommand',
        '15': 'MirtekCommand',
        '16': 'SempalCommand'
    }
    command_pattern = r'"(1.({}).6.({}).(\d+).?(\d+)?.?(\d+)?.?(\d+)?)":'
    driver_params_pattern = r'"(1.(\d+).6.0.(0|1|2|3))":'
    driver_id_pattern = r'"(1.{}.6.0.0)":'
    type_pattern = r'"(1.{}.6.0.4)":'
    COMMANDS_DATA = {
        '0': {'format': '012b', 'allowed_values_range': [0, 4095]},
        '1': {'format': '02b', 'allowed_values_range': [0, 3]},
        '2': {'format': '01b', 'allowed_values_range': [0, 3]},
        '3': {'format': '01b', 'allowed_values_range': [0, 1]},
        '4': {'format': '02b', 'allowed_values_range': [0, 3]}
    }
    DRIVER_PAYLOAD_TYPE = {
        1: 'SERIAL_DRIVERS_CONFIG',
        3: 'SERIAL_DRIVERS_COMMAND'
    }

    def __init__(self, all_addresses):
        super(SerialDriverConfig, self).__init__(all_addresses)
        # stores whether payload is enabled or not
        self.enabled_payloads = {}
        self.commands_pack()

    def pack_driver_info(self, info):
        # we have info of driver params separated by payloads
        # for each payload
        for payload_index in info:
            value_bin = ''
            if not all(elem in [i[0] for i in info[payload_index]] for elem in [j for j in self.COMMANDS_DATA][:-1]):
                raise InvalidAddressError('Not enough driver info provided in payload {}'.format(payload_index))
            # addresses is already sorted so we can pack them in loop
            for i in info[payload_index]:
                self.check_allowed_value(i[1], self.COMMANDS_DATA[i[0]]['allowed_values_range'], i[0])
                value_bin += format(i[1], self.COMMANDS_DATA[i[0]]['format'])[::-1]
            # for each payload store the enabled value
            self.enabled_payloads[payload_index] = info[payload_index][-1][1]
            self.payload_commands[payload_index] = self.bytes_to_hex(
                self.pack_bytes('H', int(value_bin[::-1], 2)))

    def pack_len_type(self, type_val, output):
        # packing len\type type_val is 1
        length = int(len(output) / 2) - 1
        self.check_allowed_value(type_val, [0, 3], 4)
        value_bin = format(length, '06b')[::-1] + format(type_val, '02b')[::-1]

        return self.ncd_integer(int(''.join(value_bin)[::-1], 2), None, 'B')

    def commands_pack(self):
        # get all addresses for driver params (all payloads)
        drivers_info = sorted(re.findall(self.driver_params_pattern, json.dumps(self.all_addresses)),
                              key=lambda x: int(x[2]))
        drivers_info_with_payloads = {}
        # separate them according to payload index
        for i in drivers_info:
            drivers_info_with_payloads.setdefault(int(i[1]), []).append((i[2], self.all_addresses[i[0]]))
        self.pack_driver_info(drivers_info_with_payloads)

        # then iterate over each payload index
        for i in self.payload_commands:

            # if enabled flag is 1 then this payload have parameters
            if self.enabled_payloads[i] == 1:
                drv_id = self.all_addresses[
                    re.search(self.driver_id_pattern.format(i), json.dumps(self.all_addresses)).group(1)]
                # get all possible parameters
                all_params = re.findall(self.command_pattern.format(i, drv_id),
                                        json.dumps(self.all_addresses))
                if not all_params:
                    raise InvalidAddressError('Not enough data for driver id {}'.format(drv_id))
                # get values of all of them
                all_params_values = {g[0]: self.all_addresses[g[0]] for g in all_params}
                # get driver id from each address and ensure that all params have same driver id
                # because 1 serial driver config can have only one driver id
                drv_types = set([y[2] for y in all_params])
                if len(drv_types) > 1:
                    raise InvalidAddressError('Only one driver type can be specified')
                # get type of this payload
                type_addr = re.search(self.type_pattern.format(i), json.dumps(self.all_addresses)).group(1)
                # then go to the class of driver and get output
                type_value = self.all_addresses[type_addr]
                if type_value in self.DRIVER_PAYLOAD_TYPE:
                    # choose class for driver according to type. (Config/Command)
                    driver_class = globals()[
                        getattr(self, self.DRIVER_PAYLOAD_TYPE[type_value])[next(iter(drv_types))]](all_params_values)
                    driver_packed = driver_class.output
                    # get type of this payload and pack it together with raw data length (output of the driver class)

                    len_type = self.pack_len_type(type_value, driver_packed)
                    # add it to output string
                    self.payload_commands[i] += len_type + driver_packed
            # if payload is not enabled then len\type byte is 00
            else:
                self.payload_commands[i] += '00'
        # build output string
        self.payload_commands = {
            key: '06' + format(int(len(self.payload_commands[key]) / 2), '02x') + self.payload_commands[key] for key in
            self.payload_commands}


class BaseDriverConfig(BasePayload):
    COMMANDS_DATA = {}
    params_pattern = None
    rfu_bit = None

    def __init__(self, all_addresses):
        super(BaseDriverConfig, self).__init__(all_addresses)
        self.output = ''
        self.commands_pack()

    def check_all_commands_and_child(self, addresses, data):
        all_commands = [i[1] for i in addresses]
        needed_commands = data['commands']
        # check if all commands exist
        if not all(elem in all_commands for elem in needed_commands):
            raise InvalidAddressError('Not enough commands {}'.format([i[0] for i in addresses]))
        # now for params
        if 'child_commands' in data:
            # need to split params by indexes
            child_commands_with_param_index = defaultdict(list)
            for i in addresses:
                child_commands_with_param_index[i[2]].append(i[3])
            # iterate over params separately and check its child commands
            for param in child_commands_with_param_index:
                if not all(elem in child_commands_with_param_index[param] for elem in
                           [k for k in data['child_commands'] if 'optional' not in data['child_commands'][k]]):
                    raise InvalidAddressError(
                        'Not all child commands exist {}'.format([i[0] for i in addresses if i[2] == param]))

    def pack_ascii(self, addresses, data):
        res = ''
        # get addresses in correct order and fetch their values.
        for g in addresses:
            if not isinstance(g[-1], int):
                self.all_addresses[g[0]] = int(self.all_addresses[g[0]])

        ascii_addresses = [(i[0], format(self.all_addresses[i[0]], data['commands'][i[1]])) for i in
                           sorted(re.findall(self.command_pattern.format('|'.join([c for c in data['commands']])),
                                             json.dumps(self.all_addresses)), key=lambda x: int(x[1]))]

        for i in ascii_addresses:
            ascii_numbers_hex = [format(ord(j), '02x') for j in i[1]]
            res += ''.join(ascii_numbers_hex)
        return res

    def pack_value_from_list(self, addresses, data):
        # just validate the value and pack
        command_id = addresses[0][1]
        value = addresses[0][-1]
        if value in data['commands'][command_id]:
            return self.ncd_integer(value, param_format=data['format'])
        raise InvalidFormatOrValueError('Not valid value for address {}'.format(addresses[0][0]))

    def pack_base_params(self, addresses, data):
        pattern = self.params_pattern
        params_all = ''
        for i in range(4):
            # if param is active or not
            pattern_param = pattern.format(i, '|'.join([j for j in data['child_commands']]))

            addr_values = [(k[0], k[1], k[2], self.all_addresses[k[0]]) for k in
                           sorted(re.findall(pattern_param, json.dumps(addresses)), key=lambda x: int(x[2]))]

            has_params = False
            if addr_values:
                value_bin = []
                for k in addr_values:
                    if 'optional' not in data['child_commands'][k[2]]:
                        param_format = data['child_commands'][k[2]]['format']
                        allowed_values_range = data['child_commands'][k[2]][
                            'allowed_values_range'] if 'allowed_values_range' in data['child_commands'][k[2]] else None
                        allowed_values_special = data['child_commands'][k[2]][
                            'allowed_values_special'] if 'allowed_values_special' in data['child_commands'][
                            k[2]] else None

                        self.check_allowed_value(k[-1], allowed_values_range, k[0], allowed_values_special)
                        value_bin.append(format(k[-1], param_format)[::-1])
                    else:
                        # if param list is not defined but param is active
                        if k[-1] == 0:
                            raise InvalidFormatOrValueError('Param list should not be specified for param {}'.format(i))
                        has_params = True
                if not has_params and addr_values[-1][-1] == 1 and 'modbus' not in data:
                    raise InvalidFormatOrValueError('Param for param {} were not specified'.format(i))

                for g in data['rfu']:
                    value_bin.insert(g[0], '0' * g[1])
                value_packed = self.bytes_to_hex(
                    self.pack_bytes(data['format'], int(''.join(value_bin)[::-1], 2))[:data['bytes_num'][0]])
                params_all += value_packed
                # if param is active
                if has_params:
                    param_list = addr_values[-1][-1]
                    par_bin = ['0'] * data['bytes_num'][1] * 8

                    for par in param_list:
                        self.check_allowed_value(par,
                                                 data['child_commands'][addr_values[-1][2]]['allowed_values_range'],
                                                 addr_values[-1][0])
                        par_bin[par] = '1'
                    param_list_packed = self.bytes_to_hex(
                        self.pack_bytes(data['format'], int(''.join(par_bin)[::-1], 2))[:data['bytes_num'][1]])
                    params_all += param_list_packed
        return params_all

    def commands_pack(self):
        # iterate over handlers and pack their commands
        res = {}
        for i in self.COMMANDS_DATA:
            commands = self.COMMANDS_DATA[i]['commands']
            # get addresses of all commands of the handler
            addresses = sorted(re.findall(self.command_pattern.format('|'.join(commands)),
                                          json.dumps(self.all_addresses)), key=lambda x: int(x[1]))
            # check if all needed commands exist
            self.check_all_commands_and_child(addresses, self.COMMANDS_DATA[i])
            # add values
            addresses_values = [(j[0], j[1], j[2], j[3], self.all_addresses[j[0]]) for j in addresses]
            a = getattr(self, i[0])(addresses_values, self.COMMANDS_DATA[i])
            res[i[1]] = a

        self.output += ''.join([i[1] for i in sorted(res.items())])
        if self.rfu_bit:
            self.output = self.output[:self.rfu_bit * 2] + '00' + self.output[self.rfu_bit * 2:]


class Modbus(BaseDriverConfig):
    command_pattern = r'"(1.\d+.6.14.({}).?(\d+)?.?(\d+)?.?(0|1)?)":'
    DRV_ID = '14'
    rfu_bit = 2
    COMMANDS_DATA = {
        ('pack_addr', 0): {
            'commands': ['0'], 'format': 'B'
        },
        ('pack_dev_cfg', 1): {
            'commands': {
                '1': {'format': '04b', 'allowed_values_range': [0, 15]},
                '2': {'format': '02b', 'allowed_values_range': [0, 3]},
                '3': {'format': '02b', 'allowed_values_range': [0, 3]},
            }, 'format': 'B'
        },
        ('pack_params_new', 2): {'commands': ['4'], 'bytes_num': 3, 'format': 'I', 'rfu_param': (2, 5),
                                 'rfu_block': (1, 6),
                                 'pairs': {0: ('rfu_param', False, 'uint24', 'pack_dev_cfg'),
                                           1: ('rfu_block', True, 'pack_blocks')},
                                 'child_commands': {
                                     '0': {'format': '013b', 'allowed_values_range': [0, 8191], 'optional': False},
                                     '1': {'format': '05b', 'allowed_values_range': [0, 23],
                                           'allowed_values_special': [31], 'optional': False},
                                     '2': {'format': '01b', 'allowed_values_range': [0, 1], 'optional': False},
                                     '3': {'format': '02b', 'allowed_values_range': [0, 3], 'optional': True},
                                     '4': {'format': '08b', 'allowed_values_range': [0, 255], 'optional': True},
                                     '5': {'format': '016b', 'allowed_values_range': [0, 65535], 'optional': True}
                                 }}

    }

    def pack_params_new(self, addresses, data):
        modbus_param_pattern = r'"(1.\d+\.6.14\.(\d+)\.({})\.(\d+)?.?(0|1)?)"'
        active_pattern = r'"(1.\d+\.6.14\.4\.\d+\.2)"'
        existing_params = sorted(set([i[2] for i in addresses]))
        values = ''
        for num in existing_params:
            info_all = sorted(re.findall(modbus_param_pattern.format(num), json.dumps(addresses)),
                              key=lambda x: int(x[3]))

            for pair in sorted(data['pairs']):
                if pair == 1:
                    active = re.search(active_pattern, json.dumps(info_all))
                    if not active:
                        raise InvalidFormatOrValueError('Not valid address for param {}'.format(num))
                    active = active.group(1)
                    if self.all_addresses[active] == 0:
                        break
                params_info = [(k[0], k[3], self.all_addresses[k[0]]) for k in
                               [i for i in info_all if
                                'optional' in data['child_commands'][i[3]] and data['child_commands'][i[3]][
                                    'optional'] == data['pairs'][pair][1]]]
                data_new = {
                    'commands': data['child_commands'], 'rfu': data[data['pairs'][pair][0]], 'format': data['format']
                }

                value = getattr(self, data['pairs'][pair][-1])(params_info, data_new)
                if 'uint24' in data['pairs'][pair]:
                    value = value[:-2]
                values += value

        return values

    def pack_blocks(self, addresses, data):
        block_pattern = r'"(1.\d+\.6.14\.\d+\.\d+\.(\d+)\.({}))"'
        value = ''
        for i in range(2):
            block_info = sorted(re.findall(block_pattern.format(str(i)), json.dumps(addresses)),
                                key=lambda x: int(x[1]))
            addrs_values = [(k[0], k[1], k[2], self.all_addresses[k[0]]) for k in block_info]
            val = self.pack_dev_cfg(addrs_values, data)
            value += val
        return value


class EnergyMeter(BaseDriverConfig):
    command_pattern = '"(1.\d+.6.3.({}).?(\d+)?.?(\d+)?)":'
    params_pattern = '"(1.\d+.6.3.8.?({})?.?({})?)",'
    DRV_ID = '3'
    rfu_bit = 17

    COMMANDS_DATA = {
        ('pack_ascii', 0): {
            'commands': {'0': '09d', '1': '06d'}
        },
        ('pack_dev_cfg', 1): {
            'commands': {
                '2': {'format': '02b', 'allowed_values_range': [0, 3]},
                '3': {'format': '01b', 'allowed_values_range': [0, 1]},
                '4': {'format': '01b', 'allowed_values_range': [0, 1]},
                '5': {'format': '02b', 'allowed_values_range': [0, 3]},
                '6': {'format': '02b', 'allowed_values_range': [0, 1], 'allowed_values_special': [3]}
            }, 'format': 'B'
        },
        ('pack_value_from_list', 2): {'commands': {'7': [0, 17, 51]}, 'format': 'B'},
        ('pack_base_params', 3): {'commands': ['8'], 'bytes_num': (3, 3), 'format': 'I', 'rfu': [(3, 3)],
                                  'child_commands': {
                                      '0': {'format': '013b', 'allowed_values_range': [0, 8191]},
                                      '1': {'format': '05b', 'allowed_values_range': [0, 23],
                                            'allowed_values_special': [31]},
                                      '2': {'format': '02b', 'allowed_values_range': [0, 3]},
                                      '3': {'format': '01b', 'allowed_values_range': [0, 1]},
                                      '4': {'allowed_values_range': [0, 23], 'optional': True}
                                  }}
    }

    def __init__(self, all_addresses):
        super(EnergyMeter, self).__init__(all_addresses)


class Mercury20(BaseDriverConfig):
    DRV_ID = '2'
    command_pattern = '"(1.\d+.6.2.({}).?(\d+)?.?(\d+)?)":'
    params_pattern = '"(1.\d+.6.2.8.?({})?.?({})?)",'
    COMMANDS_DATA = {
        ('pack_addr', 0): {'commands': ['0'], 'format': 'I'},
        ('pack_dev_cfg', 1): {
            'commands': {
                '1': {'format': '02b', 'allowed_values_range': [0, 3]},
                '2': {'format': '01b', 'allowed_values_range': [0, 1]},
                '3': {'format': '01b', 'allowed_values_range': [0, 1]},
                '4': {'format': '02b', 'allowed_values_range': [0, 3]},
                '5': {'format': '02b', 'allowed_values_range': [0, 1], 'allowed_values_special': [3]}
            }, 'format': 'B'
        },
        ('pack_dev_type', 2): {
            'commands': {'6': {'values': [0, 1], 'format': '04b'}, '7': {'values': [0, 15], 'format': '04b'},
                         '0': {'0': 6, '1': 9}},
            'format': 'B'},
        ('pack_base_params', 3): {'commands': ['8'], 'bytes_num': (3, 3), 'format': 'I', 'rfu': [(3, 3)],
                                  'child_commands': {
                                      '0': {'format': '013b', 'allowed_values_range': [0, 8191]},
                                      '1': {'format': '05b', 'allowed_values_range': [0, 23],
                                            'allowed_values_special': [31]},
                                      '2': {'format': '02b', 'allowed_values_range': [0, 3]},
                                      '3': {'format': '01b', 'allowed_values_range': [0, 1]},
                                      '4': {'allowed_values_range': [0, 23], 'optional': True}
                                  }}
    }
    rfu_bit = 6

    def __init__(self, all_addresses):
        super(Mercury20, self).__init__(all_addresses)

    def pack_dev_type(self, addresses, data):
        # just validate the value and pack
        # param 6 - type of mercury 200 or 206
        type = addresses[1][-1]
        # param 0 is address of driver, we get its length
        addr_len = len(str(addresses[0][-1]))
        # mercury 206 requires last 9 symbols of address while mercury 200 requires 6
        required_len = data['commands'][addresses[0][1]][str(type)]
        # if address length is not equal to required
        if addr_len != required_len:
            raise InvalidFormatOrValueError('For this driver last {} digits of address should be specified'.format(
                required_len))
        value_bin = ''
        # we don't need param 0 to pack, so iterate from 2nd element
        for i in addresses[1:]:
            command_id = i[1]
            value = i[-1]
            if value in data['commands'][command_id]['values']:
                value_bin += format(value, data['commands'][command_id]['format'])[::-1]
            else:
                raise InvalidFormatOrValueError('Not valid value for address {}'.format(i[0]))
        return self.ncd_integer(int(value_bin[::-1], 2), param_format=data['format'])


class Mercury23(BaseDriverConfig):
    DRV_ID = '1'
    command_pattern = '"(1.\d+.6.1.({}).?(\d+)?.?(\d+)?)":'
    params_pattern = '"(1.\d+.6.1.8.?({})?.?({})?)",'
    COMMANDS_DATA = {
        ('pack_addr', 0): {'commands': ['0'], 'format': 'B'},
        ('pack_dev_cfg', 1): {
            'commands': {
                '1': {'format': '02b', 'allowed_values_range': [0, 2]},
            }, 'format': 'B', 'rfu': (1, 6)
        },
        ('pack_ascii', 2): {'commands': {'2': '06d'}},
        ('pack_dev_cfg', 3): {
            'commands': {
                '3': {'format': '02b', 'allowed_values_range': [0, 3]},
                '4': {'format': '01b', 'allowed_values_range': [0, 1]},
                '5': {'format': '01b', 'allowed_values_range': [0, 1]},
                '6': {'format': '02b', 'allowed_values_range': [0, 3]},
                '7': {'format': '02b', 'allowed_values_range': [0, 1], 'allowed_values_special': [3]}
            }, 'format': 'B'
        },
        ('pack_base_params', 4): {'commands': ['8'], 'format': 'I', 'bytes_num': (4, 4), 'rfu': [(3, 10)],
                                  'child_commands': {
                                      '0': {'format': '013b', 'allowed_values_range': [0, 8191]},
                                      '1': {'format': '05b', 'allowed_values_range': [0, 23]},
                                      '2': {'format': '03b', 'allowed_values_range': [0, 1],
                                            'allowed_values_special': [7]},
                                      '3': {'format': '01b', 'allowed_values_range': [0, 1]},
                                      '4': {'allowed_values_range': [0, 28], 'optional': True}
                                  }}
    }

    def __init__(self, all_addresses):
        super(Mercury23, self).__init__(all_addresses)


class SaimanIEC(BaseDriverConfig):
    command_pattern = '"(1.\d+.6.13.({}).?(\d+)?.?(\d+)?)":'
    params_pattern = '"(1.\d+.6.13.7.?({})?.?({})?)",'
    DRV_ID = '13'
    rfu_bit = 15

    COMMANDS_DATA = {
        ('pack_ascii', 0): {
            'commands': {'0': '06d', '1': '08d'}
        },
        ('pack_dev_cfg', 1): {
            'commands': {
                '2': {'format': '02b', 'allowed_values_range': [0, 3]},
                '3': {'format': '01b', 'allowed_values_range': [0, 1]},
                '4': {'format': '01b', 'allowed_values_range': [0, 1]},
                '5': {'format': '02b', 'allowed_values_range': [0, 3]},
                '6': {'format': '02b', 'allowed_values_range': [0, 1], 'allowed_values_special': [3]},
            }, 'format': 'B'
        },
        ('pack_base_params', 2): {'commands': ['7'], 'format': 'I', 'bytes_num': (4, 4), 'rfu': [(4, 5)],
                                  'child_commands': {
                                      '0': {'format': '013b', 'allowed_values_range': [0, 8191]},
                                      '1': {'format': '05b', 'allowed_values_range': [0, 23],
                                            'allowed_values_special': [31]},
                                      '2': {'format': '03b', 'allowed_values_range': [0, 4],
                                            'allowed_values_special': [7]},
                                      '3': {'format': '05b', 'allowed_values_range': [0, 31]},
                                      '4': {'format': '01b', 'allowed_values_range': [0, 1]},
                                      '5': {'allowed_values_range': [0, 31], 'optional': True}
                                  }
                                  }
    }

    def __init__(self, all_addresses):
        super(SaimanIEC, self).__init__(all_addresses)


class SaimanDala(BaseDriverConfig):
    command_pattern = '"(1.\d+.6.12.({}).?(\d+)?.?(\d+)?)":'
    params_pattern = '"(1.\d+.6.12.8.?({})?.?({})?)",'
    DRV_ID = '12'
    rfu_bit = 9
    COMMANDS_DATA = {
        ('pack_addr', 0): {'commands': ['0'], 'format': 'B'},
        ('pack_dev_cfg', 1): {
            'commands': {
                '1': {'format': '02b', 'allowed_values_range': [0, 2]},
            }, 'format': 'B', 'rfu': (1, 6)
        },
        ('pack_ascii', 2): {
            'commands': {'2': '06d'}
        },
        ('pack_dev_cfg', 3): {
            'commands': {
                '3': {'format': '02b', 'allowed_values_range': [0, 3]},
                '4': {'format': '01b', 'allowed_values_range': [0, 1]},
                '5': {'format': '01b', 'allowed_values_range': [0, 1]},
                '6': {'format': '02b', 'allowed_values_range': [0, 3]},
                '7': {'format': '02b', 'allowed_values_range': [0, 1], 'allowed_values_special': [3]},
            }, 'format': 'B'
        },
        ('pack_base_params', 4): {'commands': ['8'], 'format': 'I', 'bytes_num': (3, 2), 'rfu': [(2, 5)],
                                  'child_commands': {
                                      '0': {'format': '013b', 'allowed_values_range': [0, 8191]},
                                      '1': {'format': '05b', 'allowed_values_range': [0, 23],
                                            'allowed_values_special': [31]},
                                      '2': {'format': '01b', 'allowed_values_range': [0, 1]},
                                      '3': {'allowed_values_range': [0, 15], 'optional': True}
                                  }
                                  }
    }

    def __init__(self, all_addresses):
        super(SaimanDala, self).__init__(all_addresses)


class SaimanPLC(BaseDriverConfig):
    command_pattern = '"(1.\d+.6.11.({}).?(\d+)?.?(\d+)?)":'
    params_pattern = '"(1.\d+.6.11.8.?({})?.?({})?)",'
    DRV_ID = '11'
    rfu_bit = 11
    COMMANDS_DATA = {
        ('pack_addr_bcd', 0): {'commands': {'0': '012d', '1': '06d'}},
        ('pack_value_from_list', 1): {'commands': {'2': [0, 17, 51]}, 'format': 'B'},
        ('pack_dev_cfg', 2): {
            'commands': {
                '3': {'format': '02b', 'allowed_values_range': [0, 3]},
                '4': {'format': '01b', 'allowed_values_range': [0, 1]},
                '5': {'format': '01b', 'allowed_values_range': [0, 1]},
                '6': {'format': '02b', 'allowed_values_range': [0, 3]},
                '7': {'format': '02b', 'allowed_values_range': [0, 1], 'allowed_values_special': [3]},
            }, 'format': 'B'
        },
        ('pack_base_params', 3): {'commands': ['8'], 'format': 'I', 'bytes_num': (3, 3), 'rfu': [(3, 3)],
                                  'child_commands': {
                                      '0': {'format': '013b', 'allowed_values_range': [0, 8191]},
                                      '1': {'format': '05b', 'allowed_values_range': [0, 23],
                                            'allowed_values_special': [31]},
                                      '2': {'format': '02b', 'allowed_values_range': [0, 1],
                                            'allowed_values_special': [3]},
                                      '3': {'format': '01b', 'allowed_values_range': [0, 1]},
                                      '4': {'allowed_values_range': [0, 23], 'optional': True}
                                  }
                                  }
    }

    def __init__(self, all_addresses):
        super(SaimanPLC, self).__init__(all_addresses)

    def pack_addr_bcd(self, addresses, data):
        res = ''
        for i in addresses:
            val = int(i[-1]) if not isinstance(i[-1], int) else i[-1]
            res += format(val, data['commands'][i[1]])
        return res


class MarsPulsar(BaseDriverConfig):
    command_pattern = '"(1.\d+.6.4.({}).?(\d+)?.?(\d+)?)":'
    params_pattern = '"(1.\d+.6.4.6.?({})?.?({})?)",'
    DRV_ID = '4'
    rfu_bit = 6

    COMMANDS_DATA = {
        ('pack_little_endian', 0): {'commands': ['0'], 'format': 'I'},
        ('pack_dev_cfg', 2): {
            'commands': {
                '1': {'format': '08b', 'allowed_values_range': [0, 255]},
                '2': {'format': '01b', 'allowed_values_range': [0, 1]},
                '3': {'format': '01b', 'allowed_values_range': [0, 1]},
                '4': {'format': '02b', 'allowed_values_range': [0, 3]},
                '5': {'format': '02b', 'allowed_values_range': [0, 1], 'allowed_values_special': [3]},
            }, 'format': 'H', 'rfu': (1, 2)
        },
        ('pack_base_params', 3): {'commands': ['6'], 'format': 'I', 'bytes_num': (3, 2), 'rfu': [(3, 3)],
                                  'child_commands': {
                                      '0': {'format': '013b', 'allowed_values_range': [0, 8191]},
                                      '1': {'format': '05b', 'allowed_values_range': [0, 23]},
                                      '2': {'format': '02b', 'allowed_values_range': [0, 1],
                                            'allowed_values_special': [3]},
                                      '3': {'format': '01b', 'allowed_values_range': [0, 1]},
                                      '4': {'allowed_values_range': [0, 15], 'optional': True}
                                  }
                                  }
    }

    def __init__(self, all_addresses):
        super(MarsPulsar, self).__init__(all_addresses)

    def pack_little_endian(self, addresses, data):
        ba = bytearray.fromhex(addresses[0][-1])
        ba.reverse()
        s = ''.join(format(x, '02x') for x in ba)
        return s


class Mirtek(BaseDriverConfig):
    command_pattern = '"(1.\d+.6.15.({}).?(\d+)?.?(\d+)?)":'
    params_pattern = '"(1.\d+.6.15.10.?({})?.?({})?)",'
    DRV_ID = '15'
    rfu_bit = 8
    COMMANDS_DATA = {
        ('pack_addr', 0): {'commands': ['0'], 'format': 'H'},
        ('pack_addr', 1): {'commands': ['1'], 'format': 'I'},
        ('pack_dev_cfg', 2): {
            'commands': {
                '2': {'format': '02b', 'allowed_values_range': [0, 1], 'allowed_values_special': [3]},
                '3': {'format': '01b', 'allowed_values_range': [0, 1]},
                '4': {'format': '01b', 'allowed_values_range': [0, 1]},
            }, 'format': 'B', 'rfu': (3, 4)
        },
        ('pack_dev_cfg', 3): {
            'commands': {
                '5': {'format': '02b', 'allowed_values_range': [0, 3]},
                '6': {'format': '01b', 'allowed_values_range': [0, 1]},
                '7': {'format': '01b', 'allowed_values_range': [0, 1]},
                '8': {'format': '02b', 'allowed_values_range': [0, 3]},
                '9': {'format': '02b', 'allowed_values_range': [0, 1], 'allowed_values_special': [3]},
            }, 'format': 'B'
        },
        ('pack_base_params', 4): {'commands': ['10'], 'format': 'I', 'bytes_num': (3, 4), 'rfu': [(3, 3)],
                                  'child_commands': {
                                      '0': {'format': '013b', 'allowed_values_range': [0, 8191]},
                                      '1': {'format': '05b', 'allowed_values_range': [0, 23],
                                            'allowed_values_special': [31]},
                                      '2': {'format': '02b', 'allowed_values_range': [0, 1],
                                            'allowed_values_special': [3]},
                                      '3': {'format': '01b', 'allowed_values_range': [0, 1]},
                                      '4': {'allowed_values_range': [0, 31], 'optional': True}
                                  }
                                  }
    }

    def __init__(self, all_addresses):
        super(Mirtek, self).__init__(all_addresses)


class Sempal(BaseDriverConfig):
    command_pattern = '"(1.\d+.6.16.({}).?(\d+)?.?(\d+)?)":'
    params_pattern = '"(1.\d+.6.16.7.?({})?.?({})?)",'
    DRV_ID = '16'
    rfu_bit = 2
    COMMANDS_DATA = {
        ('pack_dev_cfg', 0): {
            'commands': {
                '0': {'format': '03b', 'allowed_values_range': [0, 7]},
                '1': {'format': '03b', 'allowed_values_range': [0, 7]},
                '2': {'format': '01b', 'allowed_values_range': [0, 1]},
                '3': {'format': '01b', 'allowed_values_range': [0, 1]},
                '4': {'format': '01b', 'allowed_values_range': [0, 1]},
                '5': {'format': '02b', 'allowed_values_range': [0, 3]},
                '6': {'format': '02b', 'allowed_values_range': [0, 1], 'allowed_values_special': [3]},
            }, 'format': 'H', 'rfu': (3, 3)
        },
        ('pack_base_params', 4): {'commands': ['7'], 'format': 'I', 'bytes_num': (3, 4), 'rfu': [(3, 3)],
                                  'child_commands': {
                                      '0': {'format': '013b', 'allowed_values_range': [0, 8191]},
                                      '1': {'format': '05b', 'allowed_values_range': [0, 23],
                                            'allowed_values_special': [31]},
                                      '2': {'format': '02b', 'allowed_values_range': [3, 3]},
                                      '3': {'format': '01b', 'allowed_values_range': [0, 1]},
                                      '4': {'allowed_values_range': [0, 31], 'optional': True}
                                  }
                                  }
    }

    def __init__(self, all_addresses):
        super(Sempal, self).__init__(all_addresses)


class BaseDriverCommand(BasePayload):
    DRV_ID = None
    command_num_pattern = '"(1.\d+.6.{}.0)":'
    command_val_pattern = '"(1.\d+.6.{}.1)":'
    command_child_val_pattern = '"(1.\d+.6.{}.{})":'

    def __init__(self, all_addresses):
        super(BaseDriverCommand, self).__init__(all_addresses)
        command_num_addr = re.search(self.command_num_pattern.format(self.DRV_ID), json.dumps(self.all_addresses))
        command_val_addr = re.search(self.command_val_pattern.format(self.DRV_ID), json.dumps(self.all_addresses))
        if command_num_addr is None or command_val_addr is None:
            raise InvalidAddressError('Command info is not specified in driver {}'.format(self.DRV_ID))
        self.command_num_addr = command_num_addr.group(1)
        self.command_val_addr = command_val_addr.group(1)
        self.command_num = self.all_addresses[self.command_num_addr]
        self.command_val = self.all_addresses[self.command_val_addr]
        self.output = ''
        self.commands_pack()

    def pack_multiple_integers(self, data, value):
        values = []
        for i in sorted(data['addresses'], key=lambda x: int(x)):
            par_addr = re.search(self.command_child_val_pattern.format(self.DRV_ID, i), json.dumps(self.all_addresses))
            if not par_addr:
                raise InvalidAddressError('Not enough info in driver {}'.format(self.DRV_ID))
            par_addr = par_addr.group(1)
            par_val = self.all_addresses[par_addr]
            if 'allowed_values_range' in data['addresses'][i]:
                self.check_allowed_value(par_val, data['addresses'][i]['allowed_values_range'], par_addr)
            par_val_packed = self.ncd_integer(par_val, param_format=data['addresses'][i]['format'])
            if 'uint24' in data['addresses'][i]:
                par_val_packed = par_val_packed[:-2]
            values.append(par_val_packed)
        return ''.join(values)

    def pack_integer(self, data, value):
        param_format = data['format']
        return self.ncd_integer(value, param_format=param_format)

    def commands_pack(self):
        # check for allowed values then pack command number
        if self.command_num not in self.COMMANDS_DATA:
            raise InvalidFormatOrValueError('Not valid command id in driver {}'.format(self.DRV_ID))
        command_num_packed = self.ncd_integer(self.command_num, param_format=self.COMMAND_NUM_INFO['format'])
        # then pack command payload
        data = self.COMMANDS_DATA[self.command_num]
        if 'value' not in data:
            if 'allowed_values_range' in data:
                allowed_values_special = data['allowed_values_special'] if 'allowed_values_special' in data else None
                self.check_allowed_value(self.command_val, data['allowed_values_range'], self.command_val_addr,
                                         allowed_values_special)
            command_val_packed = self.ncd_integer(self.command_val, param_format=data['format'])
        else:
            command_val_addresses = re.findall(self.command_val_pattern.format(self.DRV_ID),
                                               json.dumps(self.all_addresses))
            selected_params = [int(par[1]) for par in command_val_addresses if
                               par[1] and self.all_addresses[par[0]] == 1]
            param_list_bin = ['0'] * self.BITS_IN_FORMATS[data['format']]
            self.command_val = selected_params
            for val in self.command_val:
                param_list_bin[val] = '1'
            value_int = int(''.join(param_list_bin)[::-1], 2)
            command_val_packed = self.bytes_to_hex(self.pack_bytes(data['format'], value_int, self.command_val_addr))

        if 'uint24' in data:
            command_val_packed = command_val_packed[:-2]
        child_command_value_packed = ''
        if 'values' in data:
            info = None
            for l in data['values']:
                if l[0] <= self.command_val <= l[1]:
                    info = data['values'][l]
            if info:
                child_command_value = None
                if 'address' in info:
                    child_command_addr = re.search(self.command_child_val_pattern.format(self.DRV_ID, info['address']),
                                                   json.dumps(self.all_addresses))
                    if child_command_addr is None:
                        raise InvalidAddressError(

                            'Not enough data for command {} in driver {}'.format(self.command_val, self.DRV_ID))
                    child_command_addr = child_command_addr.group(1)
                    child_command_value = self.all_addresses[child_command_addr]
                    if 'allowed_values_range' in info:
                        allowed_values_special = info[
                            'allowed_values_special'] if 'allowed_values_special' in info else None
                        self.check_allowed_value(child_command_value, info['allowed_values_range'], child_command_addr,
                                                 allowed_values_special)
                child_command_value_packed = getattr(self, info['handler'])(info, child_command_value)
        self.output = command_num_packed + command_val_packed + child_command_value_packed


class SaimanIECCommand(BaseDriverCommand):
    DRV_ID = '13'
    COMMAND_NUM_INFO = {
        'format': 'B',
    }
    command_val_pattern = '"(1.\d+.6.{}.1.(\d*))":'
    COMMANDS_DATA = {
        0: {'format': 'B', 'allowed_values_range': [0, 0], 'values': {
            (0, 0): {'address': '2', 'format': 'B', 'allowed_values_range': [0, 0], 'allowed_values_special': [255],
                     'handler': 'pack_integer'}
        }
            },
        1: {'format': 'I', 'allowed_values_range': [0, 31], 'value': 'list'}
    }

    def __init__(self, all_addresses):
        super(SaimanIECCommand, self).__init__(all_addresses)


class SaimanDalaCommand(BaseDriverCommand):
    DRV_ID = '12'
    COMMAND_NUM_INFO = {
        'format': 'B',
    }
    COMMANDS_DATA = {
        0: {'format': 'I', 'allowed_values_range': [0, 16777215], 'uint24': True
            }
    }

    def __init__(self, all_addresses):
        super(SaimanDalaCommand, self).__init__(all_addresses)


class SaimanPLCCommand(BaseDriverCommand):
    DRV_ID = '11'
    COMMAND_NUM_INFO = {
        'format': 'B',
    }
    COMMANDS_DATA = {
        0: {'format': 'B', 'allowed_values_range': [0, 0], 'allowed_values_special': [255]
            }
        ,
        1: {'format': 'I', 'allowed_values_range': [0, 16777215], 'uint24': True},
        2: {'format': 'B', 'allowed_values_range': [0, 44], 'values': {
            (0, 44): {'address': '2', 'format': 'H', 'handler': 'pack_integer'}
        }
            },
        3: {'format': 'B', 'values': {
            (0, 2): {'address': '2', 'format': 'H', 'handler': 'pack_integer'}
        }
            }
    }

    def __init__(self, all_addresses):
        super(SaimanPLCCommand, self).__init__(all_addresses)


class Mercury20Command(BaseDriverCommand):
    DRV_ID = '2'
    COMMAND_NUM_INFO = {
        'format': 'B',
    }
    COMMANDS_DATA = {
        0: {'format': 'B', 'allowed_values_range': [0, 0], 'values': {
            (0, 0): {'address': '2', 'format': 'B', 'allowed_values_range': [0, 1], 'allowed_values_special': [255],
                     'handler': 'pack_integer'}
        }
            }
    }

    def __init__(self, all_addresses):
        super(Mercury20Command, self).__init__(all_addresses)


class MirtekCommand(BaseDriverCommand):
    DRV_ID = '15'
    COMMAND_NUM_INFO = {
        'format': 'B',
    }
    COMMANDS_DATA = {
        0: {'format': 'B', 'allowed_values_range': [0, 0], 'allowed_values_special': [255]
            }
        ,
        1: {'format': 'I', 'allowed_values_range': [0, 16777215], 'uint24': True},
        2: {'format': 'B', 'allowed_values_range': [0, 31], 'values': {
            (0, 31): {'handler': 'pack_multiple_integers',
                      'addresses': {
                          '2': {'format': 'B', 'allowed_values_range': [0, 12]},
                          '3': {'format': 'B', 'allowed_values_range': [0, 255]},
                          '4': {'format': 'I', 'uint24': True, 'allowed_values_range': [0, 16777215]},
                      }
                      }
        }},
        3: {'format': 'B', 'allowed_values_range': [0, 12], 'values': {
            (0, 12): {'handler': 'pack_multiple_integers',
                      'addresses': {
                          '2': {'format': 'B'},
                          '3': {'format': 'I', 'uint24': True, 'allowed_values_range': [0, 16777215]},
                      }
                      }
        }}
    }

    def __init__(self, all_addresses):
        super(MirtekCommand, self).__init__(all_addresses)


class SempalCommand(BaseDriverCommand):
    DRV_ID = '16'
    COMMAND_NUM_INFO = {
        'format': 'B',
    }
    COMMANDS_DATA = {
        0: {'format': 'B', 'allowed_values_range': [0, 0], 'allowed_values_special': [255]
            }
        ,
        1: {'format': 'I', 'allowed_values_range': [0, 16777215], 'uint24': True},
        2: {'format': 'B', 'allowed_values_range': [0, 31], 'values': {
            (0, 31): {'handler': 'pack_multiple_integers',
                      'addresses': {
                          '2': {'format': 'B', 'allowed_values_range': [0, 12]},
                          '3': {'format': 'B', 'allowed_values_range': [0, 255]},
                          '4': {'format': 'I', 'uint24': True, 'allowed_values_range': [0, 16777215]},
                      }
                      }
        }},
        3: {'format': 'B', 'allowed_values_range': [0, 12], 'values': {
            (0, 12): {'handler': 'pack_multiple_integers',
                      'addresses': {
                          '2': {'format': 'B'},
                          '3': {'format': 'I', 'uint24': True, 'allowed_values_range': [0, 16777215]},
                      }
                      }
        }}
    }

    def __init__(self, all_addresses):
        super(SempalCommand, self).__init__(all_addresses)