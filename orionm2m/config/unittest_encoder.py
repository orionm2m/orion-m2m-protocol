import unittest
from encoder_r import Encoder, InvalidFormatOrValueError, InvalidAddressError, InvalidHexInputError, \
    RepeatedIndexesError
import struct


class TestEncoder(unittest.TestCase):
    BASE_INPUT = {
        "0.0": 1,
    }

    def test_system_not_valid_flag_id(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp["0.1.9"] = 1
        self.assertRaises(InvalidAddressError, lambda: Encoder(input_dict=test_inp))

    def test_system_not_valid_flag_value(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp["0.1.1"] = 15
        self.assertRaises(InvalidFormatOrValueError, lambda: Encoder(input_dict=test_inp))

    def test_system_hex_too_long(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp["1.0.0.0.2"] = "bbabcdef12"
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidHexInputError, lambda: e.encode())

    def test_system_hex_not_valid(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp["1.0.0.0.2"] = "GGPEPE12"
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidHexInputError, lambda: e.encode())

    def test_system_not_valid_address_of_child_commands(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "1.3.0.0.4.56": 19,
            "1.3.0.0.4.0": 17
        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidAddressError, lambda: e.encode())

    def test_system_not_valid_value_of_child_commands(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "1.3.0.0.4.1": 19111,
            "1.3.0.0.4.0": 17
        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidFormatOrValueError, lambda: e.encode())

    def test_system_format_error(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "1.3.0.0.6": 19999,
        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidFormatOrValueError, lambda: e.encode())

    def test_system_repeated_index(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "1.3.0.0.6": 19,
            "1.3.0.0.1": 1,
        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(RepeatedIndexesError, lambda: e.encode())

    def test_system_repeated_index_with_2_child(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "1.0.0.2.4.1": -120,
            "1.0.0.2.4.0": 127,
            "1.0.0.2.9.0": 1,
            "1.0.0.2.9.1": 1,
        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(RepeatedIndexesError, lambda: e.encode())

    def test_system_incorrect_payload_id(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "1.3.25.0.6": 19
        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidAddressError, lambda: e.encode())

    def test_system_incorrect_command_id(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "1.3.0.0.16": 19
        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidAddressError, lambda: e.encode())

    def test_system_not_allowed_value(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "1.3.0.0.1": 19
        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidFormatOrValueError, lambda: e.encode())

    def test_system_incorrect_address_len(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "1.3.0.1.11.1.1.1.1.1": 19
        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidAddressError, lambda: e.encode())

    def test_system0(self):
        inp = {
            "0.0": 1,
            "0.1.0": 1,
            "1.1.0.4.5": 255,
            "1.1.0.5.6": -127,
            "1.1.0.6.8": 34444,
            "1.1.0.7.9.0": 4294967295,
            "1.1.0.7.9.1": 4294967295,
            "1.1.0.8.10": 1,
            "1.1.0.11.11": "AB1234CD",
            "1.1.0.9.12": 1,
            "1.1.0.12.13": 1,
            "1.0.0.0.0": 4294967295,
            "1.0.0.1.2": "ABABABBA",
            "1.0.0.3.1": 1,
            "1.0.0.4.3": 65535,
            "1.0.0.5.5": 255,
            "1.0.0.9.6": -127,
            "1.0.0.2.4.1": -120,
            "1.0.0.2.4.0": 127,
            "1.1.0.0.1": 1,
            "1.1.0.10.2": "12345678",
            "1.1.0.1.0": 323232323,
            "1.1.0.2.3": 55,
            "1.1.0.3.4.1": 0,
            "1.1.0.3.4.0": 127
        }
        e = Encoder(inp)
        self.assertEqual(
            '780101001500FFFFFFFF02BAABABAB047F880103FFFF05FF0681002A010043224413033700047F0005FF0681088C8609FFFFFFFFFFFFFFFF0A0C0102785634120BCD3412AB0D',
            e.encode())

    def test_system1(self):
        inp = {
            "0.0": 1,
            "0.1.1": 1,
            "1.0.0.0.0": 566684800,
            "1.0.0.1.2": "BBABCDEF",
            "1.0.0.3.1": 1,
            "1.0.0.4.3": 19,
            "1.0.0.5.5": 19,
            "1.0.0.9.6": 35,
            "1.0.0.2.4.1": 14,
            "1.0.0.2.4.0": 11,
            "1.0.0.8.8": 11,
            "1.0.0.10.11": "abacabac",
            "1.0.0.6.12": 1,
            "1.0.0.7.13": 1,
            "1.3.0.9.0": 566684800,
            "1.3.0.1.0": 5666848,
            "1.3.0.2.3": 123,
            "1.7.0.2.3": 1233,
            "1.3.0.0.4.1": 19,
            "1.3.0.0.4.0": 17
        }
        e = Encoder(inp)
        self.assertEqual(
            '78010200200080ECC62102EFCDABBB040B0E0103130005130C010D080B0006230BACABACAB00100411130020785600037B000080ECC621000303D104',
            e.encode())

    def test_di_0(self):
        inp = {
            "0.0": 1,
            "0.1.0": 1,
            "1.1.2.1.2.3.3": 12,
            "1.1.2.1.2.4.4.0": 1,
            "1.1.2.1.2.4.4.1": 1,
            "1.1.2.1.2.4.4.2": 1,
            "1.1.2.1.2.0.0": 111122,
            "1.1.2.1.2.1.1": 11,
            "1.1.2.1.2.4.4.3": 1,
            "1.1.2.1.2.5.6.0": 445,
            "1.1.2.1.2.5.6.1": 0,
            "1.1.2.1.2.6.10": 1,
            "1.0.2.0.1.3.3": 34,
            "1.0.2.0.1.4.4.0": 2,
            "1.0.2.0.1.4.4.1": 1,
            "1.0.2.0.1.4.4.2": 2,
            "1.0.2.0.1.0.0": 566684800,
            "1.0.2.0.1.1.1": 5555,
            "1.0.2.0.1.4.4.3": 0,
            "1.0.2.0.1.5.6.0": 1333,
            "1.0.2.0.1.5.6.1": 1,
            "1.0.2.0.1.6.10": 2
        }
        e = Encoder(inp)
        self.assertEqual(e.encode(),
                         '780101021600010080ECC62101B315000003220004260635850A04021601020012B20100010B000000030C00045506BD010A02')

    def test_di_not_allowed_value_int24(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "1.0.2.0.1.2.2": 16777216,
        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidFormatOrValueError, lambda: e.encode())

    def test_di_not_allowed_value_coeff(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "1.0.2.0.1.5.6.0": 12121121212,
            "1.0.2.0.1.5.6.1": 1,
        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidFormatOrValueError, lambda: e.encode())

    def test_di_not_enough_params(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "1.0.2.0.1.5.6.0": 12,

        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidAddressError, lambda: e.encode())

    def test_di_different_input_nums(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "1.0.2.0.1.0.0": 566684800,
            "1.0.2.1.1.1.1": 5555,

        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidAddressError, lambda: e.encode())

    def test_di_not_valid_input_num(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "1.0.2.5.1.0.0": 566684800
        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidAddressError, lambda: e.encode())

    def test_di_not_valid_cfg_id(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "1.0.2.1.256.0.0": 566684800

        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidAddressError, lambda: e.encode())

    def test_di_different_cfg_ids(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "1.0.2.1.2.0.0": 566684800,
            "1.0.2.1.1.1.1": 5555,

        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidAddressError, lambda: e.encode())

    def test_di_non_existing_address(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "1.0.2.1.2.0.5": 566684800
        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidAddressError, lambda: e.encode())

    def test_di_1(self):
        inp = {"0.0": 1,
               "0.1.0": 1,
               "1.0.2.0.1.3.3": 34,
               "1.0.2.0.1.4.4.1": 1,
               "1.0.2.0.1.2.2": 16777215,
               "1.0.2.0.1.4.4.0": 2,
               "1.0.2.0.1.4.4.2": 2,
               "1.0.2.0.1.0.0": 566684800,
               "1.0.2.0.1.1.1": 5555,
               "1.0.2.0.1.4.4.3": 0,
               "1.0.2.0.1.5.6.0": 1222,
               "1.0.2.0.1.5.6.1": 1,
               "1.0.2.0.1.6.10": 2,
               "1.1.2.2.255.6.10": 2}
        e = Encoder(inp)
        self.assertEqual('780101021A00010080ECC62101B315000002FFFFFF032200042606C6840A04020402FF0A04', e.encode())

    def test_di_2(self):
        inp = {
            "0.0": 1,
            "0.1.0": 1,
            "1.0.2.1.1.0.4.0": 0,
            "1.0.2.1.1.0.4.1": 1,
            "1.0.2.1.1.0.4.2": 2,
            "1.0.2.1.1.0.4.3": 1,
            "1.0.2.1.1.1.1": -1161111345,
            "1.0.2.1.1.2.0": -1,
            "1.0.2.1.1.3.2": 65555,
            "1.0.2.1.1.4.6.0": 32767,
            "1.0.2.1.1.4.6.1": 1,
            "1.0.2.1.1.5.10": 0,
            "1.0.2.1.1.6.3": 11111
        }
        e = Encoder(inp)
        self.assertEqual('780101021A0101046401CFD8CABA00FFFFFFFF0213000106FFFF0A0103672B', e.encode())

    def test_di_3(self):
        inp = {
            "0.0": 1,
            "0.1.0": 1,
            "1.1.2.1.1.0.4.0": 0,
            "1.1.2.1.1.0.4.1": 1,
            "1.1.2.1.1.0.4.2": 2,
            "1.1.2.1.1.0.4.3": 1,
            "1.1.2.1.1.1.1": -1161111345,
            "1.1.2.1.1.2.0": -1,
            "1.1.2.1.1.3.2": 65555,
            "1.1.2.1.1.4.6.0": 32767,
            "1.1.2.1.1.4.6.1": 1,
            "1.1.2.1.1.5.10": 0,
            "1.1.2.1.1.6.3": 11111,
            "1.0.2.1.1.2.0": -1777,
            "1.0.2.1.1.3.2": 121312,
            "1.0.2.1.1.4.6.0": 6767,
            "1.0.2.1.1.4.6.1": 0,
            "1.0.2.1.1.5.10": 2,
            "1.0.2.1.1.0.4.0": 1,
            "1.0.2.1.1.0.4.1": 2,
            "1.0.2.1.1.0.4.2": 2,
            "1.0.2.1.1.0.4.3": 2,
            "1.0.2.1.1.1.1": -45,
            "1.0.2.1.1.6.3": 333
        }
        e = Encoder(inp)
        self.assertEqual(
            '780101021A010104A901D3FFFFFF000FF9FFFF02E0D901066F1A0A04034D01021A0101046401CFD8CABA00FFFFFFFF0213000106FFFF0A0103672B',
            e.encode())

    def test_di_4(self):
        inp = {
            "0.0": 1,
            "0.1.0": 1,
            "1.1.2.1.2.3.3": 12,
            "1.1.2.1.2.4.4.0": 1,
            "1.1.2.1.2.4.4.1": 1,
            "1.1.2.1.2.4.4.2": 1,
            "1.1.2.1.2.0.0": 111122,
            "1.1.2.1.2.1.1": 11,
            "1.1.2.1.2.4.4.3": 1,
            "1.1.2.1.2.5.6.0": 445,
            "1.1.2.1.2.5.6.1": 0,
            "1.1.2.1.2.6.10": 1,
            "1.0.2.0.1.3.3": 34,
            "1.0.2.0.1.4.4.0": 2,
            "1.0.2.0.1.4.4.1": 1,
            "1.0.2.0.1.4.4.2": 2,
            "1.0.2.0.1.0.0": 566684800,
            "1.0.2.0.1.1.1": 5555,
            "1.0.2.0.1.4.4.3": 0,
            "1.0.2.0.1.5.6.0": 1333,
            "1.0.2.0.1.5.6.1": 1,
            "1.0.2.0.1.6.10": 2
        }
        e = Encoder(inp)
        self.assertEqual(
            '780101021600010080ECC62101B315000003220004260635850A04021601020012B20100010B000000030C00045506BD010A02',
            e.encode())

    def test_di_repeated_address(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "1.0.2.0.1.0.0": 566684800,
            "1.0.2.0.1.0.1": 5,
        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(RepeatedIndexesError, lambda: e.encode())

    # LIGHTING
    def test_lighting_schedule_0(self):
        inp = {
            "0.0": 1,
            "0.1.0": 1,
            "1.0.8.0.1.0.0.0.0": 2,
            "1.0.8.0.1.0.0.0.1": 1,
            "1.0.8.0.1.0.0.0.2": 1,
            "1.0.8.0.1.0.0.0.3": 1,
            "1.0.8.0.1.0.0.0.4": 1,
            "1.0.8.0.1.0.0.0.5": 1,
            "1.0.8.0.1.0.0.0.6": 3,
            "1.0.8.0.1.0.0.0.7": 32200,
            "1.0.8.0.1.0.0.0.8": 22,
            "1.0.8.0.1.0.0.0.9": 22,
            "1.0.8.0.1.0.0.0.10": 120,
            "1.0.8.0.1.0.0.0.11": 200
        }
        e = Encoder(inp)
        self.assertEqual(e.encode(), '780101080C000100002E637DC8165878C8')

    def test_lighting_schedule_1(self):
        inp = {
            "0.0": 1,
            "0.1.0": 1,
            "1.0.8.0.1.0.0.0.0": 2,
            "1.0.8.0.1.0.0.0.1": 1,
            "1.0.8.0.1.0.0.0.2": 1,
            "1.0.8.0.1.0.0.0.3": 1,
            "1.0.8.0.1.0.0.0.4": 1,
            "1.0.8.0.1.0.0.0.5": 1,
            "1.0.8.0.1.0.0.0.6": 3,
            "1.0.8.0.1.0.0.0.7": 13432,
            "1.0.8.0.1.0.0.0.8": 22,
            "1.0.8.0.1.0.0.0.9": 22,
            "1.0.8.0.1.0.0.0.10": 120,
            "1.0.8.0.1.0.0.0.11": 200,
            "1.0.8.0.1.0.0.1.0": 2,
            "1.0.8.0.1.0.0.1.1": 1,
            "1.0.8.0.1.0.0.1.2": 1,
            "1.0.8.0.1.0.0.1.3": 2,
            "1.0.8.0.1.0.0.1.4": 1,
            "1.0.8.0.1.0.0.1.5": 1,
            "1.0.8.0.1.0.0.1.6": 3,
            "1.0.8.0.1.0.0.1.8": 22,
            "1.0.8.0.1.0.0.1.9": 22,
            "1.0.8.0.1.0.0.1.10": 2,
            "1.0.8.0.1.0.0.1.11": 120,
            "1.0.8.0.1.0.0.1.7": 32500,
            "1.0.8.0.1.0.0.1.12": 200,
            "1.0.8.0.1.1.0.0.0": 2,
            "1.0.8.0.1.1.0.0.1": 1,
            "1.0.8.0.1.1.0.0.2": 1,
            "1.0.8.0.1.1.0.0.3": 1,
            "1.0.8.0.1.1.0.0.4": 1,
            "1.0.8.0.1.1.0.0.5": 1,
            "1.0.8.0.1.1.0.0.6": 3,
            "1.0.8.0.1.1.0.0.7": 13432,
            "1.0.8.0.1.1.0.0.8": 22,
            "1.0.8.0.1.1.0.0.9": 22,
            "1.0.8.0.1.1.0.0.10": 2,
            "1.0.8.0.1.1.0.0.11": 120,
            "1.0.8.0.1.1.0.0.12": 200,
            "1.0.8.0.1.2.2.0": 0,
            "1.0.8.0.1.3.3": 134321,
            "1.0.8.0.1.4.5.1": 1,
            "1.0.8.0.1.4.5.23": 1,
            "1.0.8.0.1.5.4.0": 1,
            "1.0.8.0.1.5.4.2": 1,
            "1.0.8.0.1.5.4.3": 1,
            "1.0.8.0.1.5.4.4": 1,
            "1.0.8.0.1.5.4.9": 1,
            "1.0.8.0.1.5.4.5": 0,
            "1.0.8.0.1.5.4.6": 0,
            "1.0.8.0.1.6.7": 16,
            "1.0.8.0.1.7.16.1": 1,
            "1.0.8.0.1.7.16.2": 2022,
            "1.0.8.0.1.7.16.3": 125,
            "1.0.8.0.1.7.16.0": 2
        }
        e = Encoder(inp)
        self.assertEqual(e.encode(),
                         '7801010835000100012E633478165878C84E637EF41658027800002E63347816580278020003B10C020502008000041D0200000710100AE6077D')

    def test_lighting_not_valid_cfg_id(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "1.0.8.0.1.0.0.0.11": 12,
            "1.0.8.0.2.0.0.0.10": 12,

        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidAddressError, lambda: e.encode())

    def test_lighting_not_valid_command_id(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "1.0.8.0.2.0.6.0.10": 12,

        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidAddressError, lambda: e.encode())

    def test_lighting_not_valid_command_index(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "1.0.8.0.2.0.6.0.10": 12,
            "1.0.8.0.2.0.0": 12,
        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidAddressError, lambda: e.encode())

    def test_lighting_missed_command(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "1.0.8.0.1.7.16.2": 2022,
            "1.0.8.0.1.7.16.3": 125,
            "1.0.8.0.1.7.16.0": 2
        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidAddressError, lambda: e.encode())

    def test_lighting_too_few_commands(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "1.0.8.0.1.2.2.0": 2,
            "1.0.8.0.1.2.2.1": 2
        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidAddressError, lambda: e.encode())

    def test_lighting_not_valid_value_control(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "1.0.8.0.1.7.16.1": 0,
            "1.0.8.0.1.7.16.2": 2050,
            "1.0.8.0.1.7.16.0": 2
        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidFormatOrValueError, lambda: e.encode())

    def test_lighting_not_valid_value_schedule_record(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "1.0.8.0.1.2.0.0.0": 3,
            "1.0.8.0.1.2.0.0.1": 0,
            "1.0.8.0.1.2.0.0.2": 1,
            "1.0.8.0.1.2.0.0.3": 1,
            "1.0.8.0.1.2.0.0.4": 1,
            "1.0.8.0.1.2.0.0.5": 0,
            "1.0.8.0.1.2.0.0.6": 3,
            "1.0.8.0.1.2.0.0.7": 12344444,
            "1.0.8.0.1.2.0.0.8": 22,
            "1.0.8.0.1.2.0.0.9": 22,
            "1.0.8.0.1.2.0.0.11": 200,
        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidFormatOrValueError, lambda: e.encode())

    def test_lighting_not_valid_value_schedule_command(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "1.0.8.0.1.2.2.2": 33,
            "1.0.8.0.1.2.2.0": 8,
            "1.0.8.0.1.2.2.1": 12,
        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidFormatOrValueError, lambda: e.encode())

    def test_lighting_2(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.0.8.0.1.2.0.0.0": 3,
            "1.0.8.0.1.2.0.0.1": 0,
            "1.0.8.0.1.2.0.0.2": 1,
            "1.0.8.0.1.2.0.0.3": 1,
            "1.0.8.0.1.2.0.0.4": 1,
            "1.0.8.0.1.2.0.0.5": 0,
            "1.0.8.0.1.2.0.0.6": 3,
            "1.0.8.0.1.2.0.0.7": 13432,
            "1.0.8.0.1.2.0.0.8": 22,
            "1.0.8.0.1.2.0.0.9": 22,
            "1.0.8.0.1.2.0.0.11": 200,
            "1.0.8.0.1.1.4.0": 1,
            "1.0.8.0.1.1.4.2": 1,
            "1.0.8.0.1.1.4.3": 1,
            "1.0.8.0.1.1.4.4": 1,
            "1.0.8.0.1.1.4.9": 1,
            "1.0.8.0.1.3.7": 11,
            "1.0.8.0.1.0.16.1": 1,
            "1.0.8.0.1.0.16.2": 2022,
            "1.0.8.0.1.0.16.0": 2,
            "1.0.8.0.1.0.16.3": 23
        })
        e = Encoder(input_dict=test_inp)
        self.assertEqual(e.encode(), '78010108170001100AE60717041D02000000002B6134781658C8070B')

    def test_lighting_3(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.1.8.0.1.2.0.0.0": 1,
            "1.1.8.0.1.2.0.0.1": 1,
            "1.1.8.0.1.2.0.0.2": 3,
            "1.1.8.0.1.2.0.0.3": 0,
            "1.1.8.0.1.2.0.0.4": 0,
            "1.1.8.0.1.2.0.0.5": 0,
            "1.1.8.0.1.2.0.0.6": 3,
            "1.1.8.0.1.2.0.0.7": 43222,
            "1.1.8.0.1.2.0.0.8": 30,
            "1.1.8.0.1.2.0.0.9": 54,
            "1.1.8.0.1.1.4": [],
            "1.1.8.0.1.0.3": 12121,
            "1.1.8.0.1.3.2.0": 2,
            "1.1.8.0.1.3.2.1": 142,
            "1.1.8.0.1.3.2.2": 44,
            "1.1.8.0.1.4.7": 23,
            "1.0.8.0.1.2.0.0.0": 1,
            "1.0.8.0.1.2.0.0.1": 1,
            "1.0.8.0.1.2.0.0.2": 2,
            "1.0.8.0.1.2.0.0.3": 2,
            "1.0.8.0.1.2.0.0.4": 1,
            "1.0.8.0.1.2.0.0.5": 1,
            "1.0.8.0.1.2.0.0.6": 3,
            "1.0.8.0.1.2.0.0.7": 43222,
            "1.0.8.0.1.2.0.0.8": 30,
            "1.0.8.0.1.2.0.0.9": 54,
            "1.0.8.0.1.2.0.0.10": 111,
            "1.0.8.0.1.2.0.0.11": 200,
            "1.0.8.0.1.1.4.0": 1,
            "1.0.8.0.1.1.4.2": 1,
            "1.0.8.0.1.1.4.3": 1,
            "1.0.8.0.1.1.4.4": 1,
            "1.0.8.0.1.1.4.9": 1,
            "1.0.8.0.1.1.4.1": 1,
            "1.0.8.0.1.1.4.5": 1,
            "1.0.8.0.1.0.3": 98989,
            "1.0.8.0.1.3.2.0": 2,
            "1.0.8.0.1.3.2.1": 251,
            "1.0.8.0.1.3.2.2": 60,
            "1.0.8.0.1.4.7": 19
        })
        e = Encoder(input_dict=test_inp)
        self.assertEqual(e.encode(),
                         '780101081B000103AD8201043F02000000005563A8D61ED86FC80202FB3C07130819000103592F00040000000000001D60A8D61ED802028E2C0717')

    def test_lighting_4_with_di(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.1.2.1.1.0.4.0": 0,
            "1.1.2.1.1.0.4.1": 1,
            "1.1.2.1.1.0.4.2": 2,
            "1.1.2.1.1.0.4.3": 1,
            "1.1.2.1.1.1.1": -1161111345,
            "1.1.2.1.1.2.0": -1,
            "1.1.2.1.1.3.2": 65555,
            "1.1.2.1.1.4.6.0": 32767,
            "1.1.2.1.1.4.6.1": 1,
            "1.1.2.1.1.5.10": 0,
            "1.1.2.1.1.6.3": 11111,
            "1.0.8.0.1.2.0.0.0": 1,
            "1.0.8.0.1.2.0.0.1": 1,
            "1.0.8.0.1.2.0.0.2": 2,
            "1.0.8.0.1.2.0.0.3": 2,
            "1.0.8.0.1.2.0.0.4": 0,
            "1.0.8.0.1.2.0.0.5": 0,
            "1.0.8.0.1.2.0.0.6": 1,
            "1.0.8.0.1.2.0.0.7": 43222,
            "1.0.8.0.1.2.0.0.8": 1,
            "1.0.8.0.1.2.0.0.9": 50,
            "1.0.8.0.1.1.4.0": 1,
            "1.0.8.0.1.1.4.2": 1,
            "1.0.8.0.1.1.4.3": 1,
            "1.0.8.0.1.1.4.4": 1,
            "1.0.8.0.1.1.4.9": 1,
            "1.0.8.0.1.1.4.1": 1,
            "1.0.8.0.1.1.4.5": 1,
            "1.0.8.0.1.0.3": 878712,
            "1.0.8.0.1.3.2.0": 0,
            "1.0.8.0.1.4.7": 21
        })
        e = Encoder(input_dict=test_inp)
        self.assertEqual(e.encode(),
                         '780101081700010378680D043F02000000005520A8D602C802000715021A0101046401CFD8CABA00FFFFFFFF0213000106FFFF0A0103672B')

    def test_sensors_not_valid_value_0(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "1.0.9.0.2.0": 8,
        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidFormatOrValueError, lambda: e.encode())

    def test_sensors_not_valid_value_2(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "1.0.9.2.2.2.0": 1,
            "1.0.9.2.2.2.1": 11,
        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidFormatOrValueError, lambda: e.encode())

    def test_sensors_not_enough_params_2(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "1.0.9.2.2.2.0": 1
        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidAddressError, lambda: e.encode())

    def test_sensors_not_valid_value_3(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "1.0.9.3.2.3.0": 7,
            "1.0.9.3.2.3.1": 5,
            "1.0.9.3.2.3.2": 121,
        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidFormatOrValueError, lambda: e.encode())

    def test_sensors_not_enough_params_3(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "1.0.9.3.2.3.0": 7,
        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidAddressError, lambda: e.encode())

    def test_sensors_not_enough_params_4(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "1.0.9.3.2.4.1": 1
        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidAddressError, lambda: e.encode())

    def test_sensors_not_valid_value_4(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "1.0.9.3.2.4.1": 8,
            "1.0.9.3.2.4.0": [
                6,
                1
            ],
        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidFormatOrValueError, lambda: e.encode())

    def test_sensors_not_valid_value_7(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "1.1.9.2.2.7.0": 7,
            "1.1.9.2.2.7.1": 1,
            "1.1.9.2.2.7.2": 50,
            "1.1.9.2.2.7.3": 70,
        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidFormatOrValueError, lambda: e.encode())

    def test_sensors_not_enough_params_7(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "1.1.9.2.2.7.0": 6,
            "1.1.9.2.2.7.1": 1,
            "1.1.9.2.2.7.2": 50
        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidAddressError, lambda: e.encode())

    def test_sensors_not_valid_value_8(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "1.1.9.1.2.8.0": 2,
            "1.1.9.1.2.8.1": 1,
        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidFormatOrValueError, lambda: e.encode())

    def test_sensors_not_enough_params_8(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "1.1.9.1.2.8.0": 0
        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidAddressError, lambda: e.encode())

    def test_sensors_0(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.0.9.0.2.0": 1,
            "1.0.9.2.2.2.0": 120,
            "1.0.9.2.2.2.1": 10,
            "1.0.9.3.2.3.0": 6,
            "1.0.9.3.2.3.1": 5,
            "1.0.9.3.2.3.2": 121,
            "1.0.9.4.2.4.1": 1,
            "1.0.9.4.2.4.0": [
                6,
                1
            ],
            "1.1.9.2.2.7.2": 50,
            "1.1.9.2.2.7.3": 70,
            "1.1.9.3.2.4.0": 4,
            "1.1.9.3.2.4.1": 3,
            "1.1.9.2.2.7.0": 4,
            "1.1.9.2.2.7.1": 1,
            "1.1.9.1.2.8.0": 0,
            "1.1.9.1.2.8.1": 1,
            "1.0.9.0": 3,
            "1.0.9.1": 255,
            "1.1.9.0": 0,
            "1.1.9.1": 25
        })
        e = Encoder(input_dict=test_inp)
        self.assertEqual(e.encode(), '780101091303FF0002027800000A00032E79000000044202090C001908020784320046041008')

    def test_sensors_1(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.0.9.0.2.0": [
                4,
                2,
                1
            ],
            "1.0.9.2.2.2.0": 160000,
            "1.0.9.2.2.2.1": 64534,
            "1.0.9.3.2.3.0": 1,
            "1.0.9.3.2.3.1": 0,
            "1.0.9.3.2.3.2": 12344,
            "1.0.9.4.2.4.1": 7,
            "1.0.9.4.2.4.0": [
                0,
                4
            ],
            "1.1.9.2.2.4.0": 1,
            "1.1.9.2.2.4.1": 6,
            "1.1.9.0.2.7.0": 2,
            "1.1.9.0.2.7.1": 0,
            "1.1.9.1.2.8.0": 1,
            "1.1.9.1.2.8.1": 0,
            "1.0.9.0": 3,
            "1.0.9.1": 255,
            "1.1.9.0": 0,
            "1.1.9.1": 25,
            "1.2.9.2.2.4.0": 1,
            "1.2.9.2.2.4.1": 6,
            "1.2.9.0.2.7.0": 2,
            "1.2.9.0.2.7.1": 0,
            "1.2.9.1.2.8.0": 1,
            "1.2.9.1.2.8.1": 0,
            "1.2.9.3.2.2.0": 121212,
            "1.2.9.3.2.2.1": 56555,
            "1.2.9.4.2.3.0": 6,
            "1.2.9.4.2.3.1": 0,
            "1.2.9.4.2.3.2": 12344,
            "1.2.9.0": 2,
            "1.2.9.1": 251
        })
        e = Encoder(input_dict=test_inp)
        self.assertEqual(e.encode(),
                         '780101091103FF00160200710216FC030138300411800909001907020801040240091302FB07020801040240027CD901EBDC03063830')


    def test_driver_modbus_0(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.0.6.0.0": 14,
            "1.0.6.0.1": 2,
            "1.0.6.0.2": 1,
            "1.0.6.0.3": 1,
            "1.0.6.0.4": 1,
            "1.0.6.14.0": 1,
            "1.0.6.14.1": 3,
            "1.0.6.14.2": 1,
            "1.0.6.14.3": 1,
            "1.0.6.14.4.0.3.0": 2,
            "1.0.6.14.4.0.3.1": 2,
            "1.0.6.14.4.0.4.0": 6,
            "1.0.6.14.4.0.4.1": 6,
            "1.0.6.14.4.0.5.0": 56,
            "1.0.6.14.4.0.5.1": 189,
            "1.0.6.14.4.0.0": 13,
            "1.0.6.14.4.0.1": 23,
            "1.0.6.14.4.0.2": 1
        })
        e = Encoder(input_dict=test_inp)
        print(e.encode())
        self.assertEqual(e.encode(), '78010106110EE04D0153000DE082020638000206BD00')

    def test_driver_modbus_1(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.0.6.0.0": 14,
            "1.0.6.0.1": 2,
            "1.0.6.0.2": 1,
            "1.0.6.0.3": 1,
            "1.0.6.0.4": 1,
            "1.0.6.14.0": 1,
            "1.0.6.14.1": 3,
            "1.0.6.14.2": 1,
            "1.0.6.14.3": 1,
            "1.0.6.14.4.2.3.0": 1,
            "1.0.6.14.4.2.3.1": 2,
            "1.0.6.14.4.2.4.0": 32,
            "1.0.6.14.4.2.4.1": 11,
            "1.0.6.14.4.2.5.0": 191,
            "1.0.6.14.4.2.5.1": 190,
            "1.0.6.14.4.2.0": 1,
            "1.0.6.14.4.2.1": 2,
            "1.0.6.14.4.2.2": 0,
            "1.0.6.14.4.1.3.0": 2,
            "1.0.6.14.4.1.3.1": 1,
            "1.0.6.14.4.1.4.0": 1,
            "1.0.6.14.4.1.4.1": 2,
            "1.0.6.14.4.1.5.0": 126,
            "1.0.6.14.4.1.5.1": 183,
            "1.0.6.14.4.1.0": 2,
            "1.0.6.14.4.1.1": 3,
            "1.0.6.14.4.1.2": 1,
            "1.0.6.14.4.0.3.0": 2,
            "1.0.6.14.4.0.3.1": 2,
            "1.0.6.14.4.0.4.0": 6,
            "1.0.6.14.4.0.4.1": 6,
            "1.0.6.14.4.0.5.0": 123,
            "1.0.6.14.4.0.5.1": 132,
            "1.0.6.14.4.0.0": 13,
            "1.0.6.14.4.0.1": 23,
            "1.0.6.14.4.0.2": 1
        })
        e = Encoder(input_dict=test_inp)
        self.assertEqual(e.encode(), '780101061F0EE05B0153000DE08202067B000206840002608002017E000102B700014000')

    def test_driver_modbus_2(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.0.6.0.0": 14,
            "1.0.6.0.1": 2,
            "1.0.6.0.2": 1,
            "1.0.6.0.3": 1,
            "1.0.6.0.4": 1,
            "1.0.6.14.0": 1,
            "1.0.6.14.1": 3,
            "1.0.6.14.2": 1,
            "1.0.6.14.3": 1,
            "1.0.6.14.4.2.3.0": 1,
            "1.0.6.14.4.2.3.1": 2,
            "1.0.6.14.4.2.4.0": 32,
            "1.0.6.14.4.2.4.1": 11,
            "1.0.6.14.4.2.5.0": 178,
            "1.0.6.14.4.2.5.1": 199,
            "1.0.6.14.4.2.0": 1,
            "1.0.6.14.4.2.1": 2,
            "1.0.6.14.4.2.2": 1,
            "1.0.6.14.4.1.3.0": 2,
            "1.0.6.14.4.1.3.1": 1,
            "1.0.6.14.4.1.4.0": 1,
            "1.0.6.14.4.1.4.1": 2,
            "1.0.6.14.4.1.5.0": 167,
            "1.0.6.14.4.1.5.1": 166,
            "1.0.6.14.4.1.0": 2,
            "1.0.6.14.4.1.1": 3,
            "1.0.6.14.4.1.2": 1,
            "1.0.6.14.4.0.3.0": 2,
            "1.0.6.14.4.0.3.1": 2,
            "1.0.6.14.4.0.4.0": 6,
            "1.0.6.14.4.0.4.1": 6,
            "1.0.6.14.4.0.5.0": 111,
            "1.0.6.14.4.0.5.1": 222,
            "1.0.6.14.4.0.0": 13,
            "1.0.6.14.4.0.1": 23,
            "1.0.6.14.4.0.2": 1
        })
        e = Encoder(input_dict=test_inp)
        self.assertEqual(e.encode(),
                         '78010106270EE0630153000DE08202066F000206DE000260800201A7000102A6000140800120B200020BC700')

    def test_driver_modbus_3(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.1.6.0.0": 14,
            "1.1.6.0.1": 2,
            "1.1.6.0.2": 1,
            "1.1.6.0.3": 1,
            "1.1.6.0.4": 1,
            "1.1.6.14.0": 1,
            "1.1.6.14.1": 3,
            "1.1.6.14.2": 1,
            "1.1.6.14.3": 1,
            "1.1.6.14.4.2.3.0": 3,
            "1.1.6.14.4.2.3.1": 1,
            "1.1.6.14.4.2.4.0": 31,
            "1.1.6.14.4.2.4.1": 1,
            "1.1.6.14.4.2.5.0": 67,
            "1.1.6.14.4.2.5.1": 76,
            "1.1.6.14.4.2.0": 1,
            "1.1.6.14.4.2.1": 2,
            "1.1.6.14.4.2.2": 1,
            "1.1.6.14.4.1.3.0": 2,
            "1.1.6.14.4.1.3.1": 1,
            "1.1.6.14.4.1.4.0": 1,
            "1.1.6.14.4.1.4.1": 2,
            "1.1.6.14.4.1.5.0": 88,
            "1.1.6.14.4.1.5.1": 123,
            "1.1.6.14.4.1.0": 2,
            "1.1.6.14.4.1.1": 3,
            "1.1.6.14.4.1.2": 1,
            "1.1.6.14.4.0.3.0": 2,
            "1.1.6.14.4.0.3.1": 2,
            "1.1.6.14.4.0.4.0": 6,
            "1.1.6.14.4.0.4.1": 6,
            "1.1.6.14.4.0.5.0": 34,
            "1.1.6.14.4.0.5.1": 211,
            "1.1.6.14.4.0.0": 13,
            "1.1.6.14.4.0.1": 23,
            "1.1.6.14.4.0.2": 1,
            "1.0.6.0.0": 14,
            "1.0.6.0.1": 2,
            "1.0.6.0.2": 1,
            "1.0.6.0.3": 1,
            "1.0.6.0.4": 1,
            "1.0.6.14.0": 1,
            "1.0.6.14.1": 3,
            "1.0.6.14.2": 1,
            "1.0.6.14.3": 1,
            "1.0.6.14.4.2.3.0": 1,
            "1.0.6.14.4.2.3.1": 2,
            "1.0.6.14.4.2.4.0": 32,
            "1.0.6.14.4.2.4.1": 11,
            "1.0.6.14.4.2.5.0": 251,
            "1.0.6.14.4.2.5.1": 222,
            "1.0.6.14.4.2.0": 1,
            "1.0.6.14.4.2.1": 2,
            "1.0.6.14.4.2.2": 1,
            "1.0.6.14.4.1.3.0": 2,
            "1.0.6.14.4.1.3.1": 1,
            "1.0.6.14.4.1.4.0": 1,
            "1.0.6.14.4.1.4.1": 2,
            "1.0.6.14.4.1.5.0": 164,
            "1.0.6.14.4.1.5.1": 189,
            "1.0.6.14.4.1.0": 2,
            "1.0.6.14.4.1.1": 3,
            "1.0.6.14.4.1.2": 1,
            "1.0.6.14.4.0.3.0": 2,
            "1.0.6.14.4.0.3.1": 2,
            "1.0.6.14.4.0.4.0": 6,
            "1.0.6.14.4.0.4.1": 6,
            "1.0.6.14.4.0.5.0": 95,
            "1.0.6.14.4.0.5.1": 101,
            "1.0.6.14.4.0.0": 13,
            "1.0.6.14.4.0.1": 23,
            "1.0.6.14.4.0.2": 1
        })
        e = Encoder(input_dict=test_inp)
        self.assertEqual(e.encode(),
                         '78010106270EE0630153000DE08202065F00020665000260800201A4000102BD000140800120FB00020BDE0006270EE0630153000DE082020622000206D3000260800201580001027B00014080031F430001014C00')

    def test_driver_modbus_4(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.1.6.0.0": 14,
            "1.1.6.0.1": 2,
            "1.1.6.0.2": 1,
            "1.1.6.0.3": 0,
            "1.1.6.0.4": 1,
            "1.1.6.14.0": 1,
            "1.1.6.14.1": 3,
            "1.1.6.14.2": 1,
            "1.1.6.14.3": 1,
            "1.1.6.14.4.2.3.0": 3,
            "1.1.6.14.4.2.3.1": 1,
            "1.1.6.14.4.2.4.0": 31,
            "1.1.6.14.4.2.4.1": 1,
            "1.1.6.14.4.2.5.0": 234,
            "1.1.6.14.4.2.5.1": 76,
            "1.1.6.14.4.2.0": 1,
            "1.1.6.14.4.2.1": 2,
            "1.1.6.14.4.2.2": 1,
            "1.1.6.14.4.1.3.0": 2,
            "1.1.6.14.4.1.3.1": 1,
            "1.1.6.14.4.1.4.0": 1,
            "1.1.6.14.4.1.4.1": 2,
            "1.1.6.14.4.1.5.0": 111,
            "1.1.6.14.4.1.5.1": 49,
            "1.1.6.14.4.1.0": 2,
            "1.1.6.14.4.1.1": 3,
            "1.1.6.14.4.1.2": 1,
            "1.1.6.14.4.0.3.0": 2,
            "1.1.6.14.4.0.3.1": 2,
            "1.1.6.14.4.0.4.0": 6,
            "1.1.6.14.4.0.4.1": 6,
            "1.1.6.14.4.0.5.0": 34,
            "1.1.6.14.4.0.5.1": 199,
            "1.1.6.14.4.0.0": 13,
            "1.1.6.14.4.0.1": 23,
            "1.1.6.14.4.0.2": 1,
            "1.0.6.0.0": 14,
            "1.0.6.0.1": 2,
            "1.0.6.0.2": 1,
            "1.0.6.0.3": 1,
            "1.0.6.0.4": 1,
            "1.0.6.14.0": 1,
            "1.0.6.14.1": 3,
            "1.0.6.14.2": 1,
            "1.0.6.14.3": 1,
            "1.0.6.14.4.2.3.0": 1,
            "1.0.6.14.4.2.3.1": 2,
            "1.0.6.14.4.2.4.0": 32,
            "1.0.6.14.4.2.4.1": 11,
            "1.0.6.14.4.2.5.0": 231,
            "1.0.6.14.4.2.5.1": 246,
            "1.0.6.14.4.2.0": 1,
            "1.0.6.14.4.2.1": 2,
            "1.0.6.14.4.2.2": 1,
            "1.0.6.14.4.1.3.0": 2,
            "1.0.6.14.4.1.3.1": 1,
            "1.0.6.14.4.1.4.0": 1,
            "1.0.6.14.4.1.4.1": 2,
            "1.0.6.14.4.1.5.0": 123,
            "1.0.6.14.4.1.5.1": 149,
            "1.0.6.14.4.1.0": 2,
            "1.0.6.14.4.1.1": 3,
            "1.0.6.14.4.1.2": 1,
            "1.0.6.14.4.0.3.0": 2,
            "1.0.6.14.4.0.3.1": 2,
            "1.0.6.14.4.0.4.0": 6,
            "1.0.6.14.4.0.4.1": 6,
            "1.0.6.14.4.0.5.0": 80,
            "1.0.6.14.4.0.5.1": 222,
            "1.0.6.14.4.0.0": 13,
            "1.0.6.14.4.0.1": 23,
            "1.0.6.14.4.0.2": 1
        })
        e = Encoder(input_dict=test_inp)
        self.assertEqual(e.encode(),
                         '78010106270EE0630153000DE082020650000206DE0002608002017B00010295000140800120E700020BF60006030E6000')

    def test_driver_energy_meter_0(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.1.6.0.0": 3,
            "1.1.6.0.1": 2,
            "1.1.6.0.2": 1,
            "1.1.6.0.3": 1,
            "1.1.6.0.4": 1,
            "1.1.6.3.1": "67671",
            "1.1.6.3.0": "1367206",
            "1.1.6.3.5": 1,
            "1.1.6.3.6": 3,
            "1.1.6.3.2": 3,
            "1.1.6.3.7": 17,
            "1.1.6.3.3": 1,
            "1.1.6.3.4": 1,
            "1.1.6.3.8.0.0": 1,
            "1.1.6.3.8.0.1": 1,
            "1.1.6.3.8.0.2": 1,
            "1.1.6.3.8.0.3": 1,
            "1.1.6.3.8.0.4": [5, 2, 1],
            "1.1.6.3.8.1.0": 1,
            "1.1.6.3.8.1.1": 1,
            "1.1.6.3.8.1.2": 1,
            "1.1.6.3.8.1.3": 1,
            "1.1.6.3.8.1.4": [1, 2, 9]
        })
        e = Encoder(input_dict=test_inp)
        self.assertEqual(e.encode(),

                         '780101062103E05D303031333637323036303637363731DF1100012084260000012084060200')

    def test_driver_energy_meter_1(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.0.6.0.0": 3,
            "1.0.6.0.1": 2,
            "1.0.6.0.2": 1,
            "1.0.6.0.3": 1,
            "1.0.6.0.4": 1,
            "1.0.6.3.1": "123456789",
            "1.0.6.3.0": "777777",
            "1.0.6.3.5": 1,
            "1.0.6.3.6": 1,
            "1.0.6.3.2": 1,
            "1.0.6.3.7": 0,
            "1.0.6.3.3": 0,
            "1.0.6.3.4": 0,
            "1.0.6.3.8.0.0": 1,
            "1.0.6.3.8.0.1": 1,
            "1.0.6.3.8.0.2": 1,
            "1.0.6.3.8.0.3": 1,
            "1.0.6.3.8.0.4": [
                5,
                2,
                1
            ],
            "1.0.6.3.8.1.0": 234,
            "1.0.6.3.8.1.1": 3,
            "1.0.6.3.8.1.2": 2,
            "1.0.6.3.8.1.3": 0,
            "1.0.6.3.8.2.0": 444,
            "1.0.6.3.8.2.1": 3,
            "1.0.6.3.8.2.2": 3,
            "1.0.6.3.8.2.3": 1,
            "1.0.6.3.8.2.4": [
                4, 7, 11
            ],
            "1.0.6.3.8.3.0": 8191,
            "1.0.6.3.8.3.1": 2,
            "1.0.6.3.8.3.2": 1,
            "1.0.6.3.8.3.3": 0
        })
        e = Encoder(input_dict=test_inp)
        self.assertEqual(e.encode(),
                         '780101062A03E066303030373737373737313233343536373800395100012084260000EA6008BC618C900800FF5F04')

    def test_driver_mercury20_0(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.0.6.0.0": 2,
            "1.0.6.0.1": 2,
            "1.0.6.0.2": 1,
            "1.0.6.0.3": 1,
            "1.0.6.0.4": 1,
            "1.0.6.2.1": 1,
            "1.0.6.2.0": "111111111",
            "1.0.6.2.5": 1,
            "1.0.6.2.6": 1,
            "1.0.6.2.2": 1,
            "1.0.6.2.7": 0,
            "1.0.6.2.3": 0,
            "1.0.6.2.4": 0,
            "1.0.6.2.8.0.0": 1,
            "1.0.6.2.8.0.1": 1,
            "1.0.6.2.8.0.2": 1,
            "1.0.6.2.8.0.3": 1,
            "1.0.6.2.8.0.4": [
                5,
                2,
                1
            ]
        })
        e = Encoder(input_dict=test_inp)
        self.assertEqual(e.encode(),
                         '780101061002E04CC76B9F06450100012084260000')

    def test_driver_mercury20_1(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.0.6.0.0": 2,
            "1.0.6.0.1": 2,
            "1.0.6.0.2": 0,
            "1.0.6.0.3": 1,
            "1.0.6.0.4": 1,
            "1.0.6.2.1": 2,
            "1.0.6.2.0": 343434,
            "1.0.6.2.5": 3,
            "1.0.6.2.6": 0,
            "1.0.6.2.2": 1,
            "1.0.6.2.7": 15,
            "1.0.6.2.3": 1,
            "1.0.6.2.4": 1,
            "1.0.6.2.8.0.0": 1123,
            "1.0.6.2.8.0.1": 31,
            "1.0.6.2.8.0.2": 3,
            "1.0.6.2.8.0.3": 1,
            "1.0.6.2.8.0.4": [
                1, 2, 3, 4, 5, 23
            ],
            "1.0.6.2.8.1.0": 8111,
            "1.0.6.2.8.1.1": 22,
            "1.0.6.2.8.1.2": 1,
            "1.0.6.2.8.1.3": 1,
            "1.0.6.2.8.1.4": [
                5, 7, 8, 9, 12
            ],
            "1.0.6.2.8.2.0": 3455,
            "1.0.6.2.8.2.1": 23,
            "1.0.6.2.8.2.2": 0,
            "1.0.6.2.8.2.3": 1,
            "1.0.6.2.8.2.4": [
                23, 22, 13
            ],
            "1.0.6.2.8.3.0": 3222,
            "1.0.6.2.8.3.1": 31,
            "1.0.6.2.8.3.2": 3,
            "1.0.6.2.8.3.3": 1,
            "1.0.6.2.8.3.4": [
                0, 23
            ]
        })
        e = Encoder(input_dict=test_inp)

        self.assertEqual(e.encode(),
                         '780101062202A05E8A3D0500DEF00063E48F3E0080AFDF86A013007FED820020C096EC8F010080')

    def test_driver_mercury20_2(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.1.6.0.0": 2,
            "1.1.6.0.1": 2,
            "1.1.6.0.2": 0,
            "1.1.6.0.3": 1,
            "1.1.6.0.4": 1,
            "1.1.6.2.1": 2,
            "1.1.6.2.0": 343434,
            "1.1.6.2.5": 3,
            "1.1.6.2.6": 0,
            "1.1.6.2.2": 1,
            "1.1.6.2.7": 15,
            "1.1.6.2.3": 1,
            "1.1.6.2.4": 1,
            "1.1.6.2.8.0.0": 1123,
            "1.1.6.2.8.0.1": 31,
            "1.1.6.2.8.0.2": 3,
            "1.1.6.2.8.0.3": 1,
            "1.1.6.2.8.0.4": [
                1, 2, 3, 4, 5, 23
            ],
            "1.1.6.2.8.1.0": 8111,
            "1.1.6.2.8.1.1": 22,
            "1.1.6.2.8.1.2": 1,
            "1.1.6.2.8.1.3": 1,
            "1.1.6.2.8.1.4": [
                5, 7, 8, 9, 12
            ],
            "1.1.6.2.8.2.0": 3455,
            "1.1.6.2.8.2.1": 23,
            "1.1.6.2.8.2.2": 0,
            "1.1.6.2.8.2.3": 1,
            "1.1.6.2.8.2.4": [
                23, 22, 13
            ],
            "1.1.6.2.8.3.0": 3222,
            "1.1.6.2.8.3.1": 31,
            "1.1.6.2.8.3.2": 3,
            "1.1.6.2.8.3.3": 1,
            "1.1.6.2.8.3.4": [
                0, 23
            ],
            "1.0.6.0.0": 2,
            "1.0.6.0.1": 2,
            "1.0.6.0.2": 0,
            "1.0.6.0.3": 1,
            "1.0.6.0.4": 1,
            "1.0.6.2.1": 2,
            "1.0.6.2.0": 343434,
            "1.0.6.2.5": 3,
            "1.0.6.2.6": 0,
            "1.0.6.2.2": 1,
            "1.0.6.2.7": 15,
            "1.0.6.2.3": 1,
            "1.0.6.2.4": 1,
            "1.0.6.2.8.0.0": 1123,
            "1.0.6.2.8.0.1": 31,
            "1.0.6.2.8.0.2": 3,
            "1.0.6.2.8.0.3": 1,
            "1.0.6.2.8.0.4": [
                1, 2, 3, 4, 5, 23
            ],
            "1.0.6.2.8.1.0": 8111,
            "1.0.6.2.8.1.1": 22,
            "1.0.6.2.8.1.2": 1,
            "1.0.6.2.8.1.3": 1,
            "1.0.6.2.8.1.4": [
                5, 7, 8, 9, 12
            ],
            "1.0.6.2.8.2.0": 3455,
            "1.0.6.2.8.2.1": 23,
            "1.0.6.2.8.2.2": 0,
            "1.0.6.2.8.2.3": 1,
            "1.0.6.2.8.2.4": [
                23, 22, 13
            ],
            "1.0.6.2.8.3.0": 3222,
            "1.0.6.2.8.3.1": 31,
            "1.0.6.2.8.3.2": 3,
            "1.0.6.2.8.3.3": 1,
            "1.0.6.2.8.3.4": [
                0, 23
            ]
        })
        e = Encoder(input_dict=test_inp)

        self.assertEqual(e.encode(),
                         '780101062202A05E8A3D0500DEF00063E48F3E0080AFDF86A013007FED820020C096EC8F010080062202A05E8A3D0500DEF00063E48F3E0080AFDF86A013007FED820020C096EC8F010080')

    def test_driver_mercury23_0(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.1.6.0.0": 1,
            "1.1.6.0.1": 2,
            "1.1.6.0.2": 0,
            "1.1.6.0.3": 1,
            "1.1.6.0.4": 1,
            "1.1.6.1.1": 1,
            "1.1.6.1.0": 254,
            "1.1.6.1.5": 1,
            "1.1.6.1.6": 3,
            "1.1.6.1.2": 123456,
            "1.1.6.1.7": 1,
            "1.1.6.1.3": 1,
            "1.1.6.1.4": 0,
            "1.1.6.1.8.0.0": 1123,
            "1.1.6.1.8.0.1": 23,
            "1.1.6.1.8.0.2": 7,
            "1.1.6.1.8.0.3": 1,
            "1.1.6.1.8.0.4": [
                1, 2, 3, 4, 5, 23, 28
            ]
        })
        e = Encoder(input_dict=test_inp)

        self.assertEqual(e.encode(),
                         '780101061401A050FE013132333435367963E41E803E008010')

    def test_driver_mercury23_1(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.1.6.0.0": 1,
            "1.1.6.0.1": 2,
            "1.1.6.0.2": 0,
            "1.1.6.0.3": 1,
            "1.1.6.0.4": 1,
            "1.1.6.1.1": 2,
            "1.1.6.1.0": 187,
            "1.1.6.1.5": 1,
            "1.1.6.1.6": 3,
            "1.1.6.1.2": 654321,
            "1.1.6.1.7": 1,
            "1.1.6.1.3": 1,
            "1.1.6.1.4": 1,
            "1.1.6.1.8.3.0": 3456,
            "1.1.6.1.8.3.1": 23,
            "1.1.6.1.8.3.2": 0,
            "1.1.6.1.8.3.3": 1,
            "1.1.6.1.8.3.4": [
                7, 8, 15
            ],
            "1.1.6.1.8.1.0": 1999,
            "1.1.6.1.8.1.1": 2,
            "1.1.6.1.8.1.2": 7,
            "1.1.6.1.8.1.3": 1,
            "1.1.6.1.8.1.4": [
                9, 10, 11
            ],
            "1.1.6.1.8.2.0": 7011,
            "1.1.6.1.8.2.1": 17,
            "1.1.6.1.8.2.2": 1,
            "1.1.6.1.8.2.3": 0,
            "1.1.6.1.8.0.0": 8000,
            "1.1.6.1.8.0.1": 21,
            "1.1.6.1.8.0.2": 1,
            "1.1.6.1.8.0.3": 1,
            "1.1.6.1.8.0.4": [
                1, 2, 3, 4, 5, 23, 28
            ]
        })
        e = Encoder(input_dict=test_inp)

        self.assertEqual(e.encode(),
                         '780101062801A064BB023635343332317D40BF06803E008010CF471C80000E0000633B060080ED028080810000')

    def test_driver_mercury23_2_mercury20(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.1.6.0.0": 1,
            "1.1.6.0.1": 2,
            "1.1.6.0.2": 0,
            "1.1.6.0.3": 1,
            "1.1.6.0.4": 1,
            "1.1.6.1.1": 2,
            "1.1.6.1.0": 187,
            "1.1.6.1.5": 1,
            "1.1.6.1.6": 3,
            "1.1.6.1.2": 654321,
            "1.1.6.1.7": 1,
            "1.1.6.1.3": 1,
            "1.1.6.1.4": 1,
            "1.1.6.1.8.3.0": 3456,
            "1.1.6.1.8.3.1": 23,
            "1.1.6.1.8.3.2": 0,
            "1.1.6.1.8.3.3": 1,
            "1.1.6.1.8.3.4": [
                7,
                8,
                15
            ],
            "1.1.6.1.8.1.0": 1999,
            "1.1.6.1.8.1.1": 2,
            "1.1.6.1.8.1.2": 7,
            "1.1.6.1.8.1.3": 1,
            "1.1.6.1.8.1.4": [
                9,
                10,
                11
            ],
            "1.1.6.1.8.2.0": 7011,
            "1.1.6.1.8.2.1": 17,
            "1.1.6.1.8.2.2": 1,
            "1.1.6.1.8.2.3": 0,
            "1.1.6.1.8.0.0": 8000,
            "1.1.6.1.8.0.1": 21,
            "1.1.6.1.8.0.2": 1,
            "1.1.6.1.8.0.3": 1,
            "1.1.6.1.8.0.4": [
                1,
                2,
                3,
                4,
                5,
                23,
                28
            ],
            "1.0.6.0.0": 2,
            "1.0.6.0.1": 2,
            "1.0.6.0.2": 0,
            "1.0.6.0.3": 1,
            "1.0.6.0.4": 1,
            "1.0.6.2.1": 2,
            "1.0.6.2.0": 343434,
            "1.0.6.2.5": 3,
            "1.0.6.2.6": 0,
            "1.0.6.2.2": 1,
            "1.0.6.2.7": 15,
            "1.0.6.2.3": 1,
            "1.0.6.2.4": 1,
            "1.0.6.2.8.0.0": 1123,
            "1.0.6.2.8.0.1": 31,
            "1.0.6.2.8.0.2": 3,
            "1.0.6.2.8.0.3": 1,
            "1.0.6.2.8.0.4": [
                1,
                2,
                3,
                4,
                5,
                23
            ],
            "1.0.6.2.8.1.0": 8111,
            "1.0.6.2.8.1.1": 22,
            "1.0.6.2.8.1.2": 1,
            "1.0.6.2.8.1.3": 1,
            "1.0.6.2.8.1.4": [
                5,
                7,
                8,
                9,
                12
            ],
            "1.0.6.2.8.2.0": 3455,
            "1.0.6.2.8.2.1": 23,
            "1.0.6.2.8.2.2": 0,
            "1.0.6.2.8.2.3": 1,
            "1.0.6.2.8.2.4": [
                23,
                22,
                13
            ],
            "1.0.6.2.8.3.0": 3222,
            "1.0.6.2.8.3.1": 31,
            "1.0.6.2.8.3.2": 3,
            "1.0.6.2.8.3.3": 1,
            "1.0.6.2.8.3.4": [
                0,
                23
            ]
        })
        e = Encoder(input_dict=test_inp)

        self.assertEqual(e.encode(),
                         '780101062202A05E8A3D0500DEF00063E48F3E0080AFDF86A013007FED820020C096EC8F010080062801A064BB023635343332317D40BF06803E008010CF471C80000E0000633B060080ED028080810000')

    def test_mercury20_modbus(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.1.6.0.0": 1,
            "1.1.6.0.1": 2,
            "1.1.6.0.2": 0,
            "1.1.6.0.3": 1,
            "1.1.6.0.4": 1,
            "1.1.6.1.1": 2,
            "1.1.6.1.0": 187,
            "1.1.6.1.5": 1,
            "1.1.6.1.6": 3,
            "1.1.6.1.2": 654321,
            "1.1.6.1.7": 1,
            "1.1.6.1.3": 1,
            "1.1.6.1.4": 1,
            "1.1.6.1.8.3.0": 3456,
            "1.1.6.1.8.3.1": 23,
            "1.1.6.1.8.3.2": 0,
            "1.1.6.1.8.3.3": 1,
            "1.1.6.1.8.3.4": [
                7,
                8,
                15
            ],
            "1.1.6.1.8.1.0": 1999,
            "1.1.6.1.8.1.1": 2,
            "1.1.6.1.8.1.2": 7,
            "1.1.6.1.8.1.3": 1,
            "1.1.6.1.8.1.4": [
                9,
                10,
                11
            ],
            "1.1.6.1.8.2.0": 7011,
            "1.1.6.1.8.2.1": 17,
            "1.1.6.1.8.2.2": 1,
            "1.1.6.1.8.2.3": 0,
            "1.1.6.1.8.0.0": 8000,
            "1.1.6.1.8.0.1": 21,
            "1.1.6.1.8.0.2": 1,
            "1.1.6.1.8.0.3": 1,
            "1.1.6.1.8.0.4": [
                1,
                2,
                3,
                4,
                5,
                23,
                28
            ],
            "1.0.6.0.0": 14,
            "1.0.6.0.1": 2,
            "1.0.6.0.2": 1,
            "1.0.6.0.3": 1,
            "1.0.6.0.4": 1,
            "1.0.6.14.0": 1,
            "1.0.6.14.1": 3,
            "1.0.6.14.2": 1,
            "1.0.6.14.3": 1,
            "1.0.6.14.4.0.3.0": 2,
            "1.0.6.14.4.0.3.1": 2,
            "1.0.6.14.4.0.4.0": 6,
            "1.0.6.14.4.0.4.1": 6,
            "1.0.6.14.4.0.5.0": 56,
            "1.0.6.14.4.0.5.1": 189,
            "1.0.6.14.4.0.0": 13,
            "1.0.6.14.4.0.1": 23,
            "1.0.6.14.4.0.2": 1
        })
        e = Encoder(input_dict=test_inp)

        self.assertEqual(e.encode(),
                         '78010106110EE04D0153000DE082020638000206BD00062801A064BB023635343332317D40BF06803E008010CF471C80000E0000633B060080ED028080810000')

    def test_driver_saiman_iec_0(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.1.6.0.0": 13,
            "1.1.6.0.1": 2,
            "1.1.6.0.2": 0,
            "1.1.6.0.3": 1,
            "1.1.6.0.4": 1,
            "1.1.6.13.1": "12345678",
            "1.1.6.13.0": 187111,
            "1.1.6.13.5": 2,
            "1.1.6.13.6": 3,
            "1.1.6.13.2": 2,
            "1.1.6.13.3": 1,
            "1.1.6.13.4": 1,
            "1.1.6.13.7.0.0": 8000,
            "1.1.6.13.7.0.1": 21,
            "1.1.6.13.7.0.2": 2,
            "1.1.6.13.7.0.3": 5,
            "1.1.6.13.7.0.4": 1,
            "1.1.6.13.7.0.5": [
                1,
                2,
                3,
                4,
                5,
                23,
                28
            ]
        })
        e = Encoder(input_dict=test_inp)

        self.assertEqual(e.encode(),
                         '780101061B0DA0573138373131313132333435363738EE0040BFAA803E008010')

    def test_driver_saiman_iec_1(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.0.6.0.0": 13,
            "1.0.6.0.1": 2,
            "1.0.6.0.2": 0,
            "1.0.6.0.3": 1,
            "1.0.6.0.4": 1,
            "1.0.6.13.1": "00000789",
            "1.0.6.13.0": "000000",
            "1.0.6.13.5": 3,
            "1.0.6.13.6": 1,
            "1.0.6.13.2": 0,
            "1.0.6.13.3": 1,
            "1.0.6.13.4": 1,
            "1.0.6.13.7.1.0": 555,
            "1.0.6.13.7.1.1": 22,
            "1.0.6.13.7.1.2": 7,
            "1.0.6.13.7.1.3": 30,
            "1.0.6.13.7.1.4": 1,
            "1.0.6.13.7.1.5": [
                0,
                31
            ]
        })
        e = Encoder(input_dict=test_inp)

        self.assertEqual(e.encode(),
                         '780101061B0DA05730303030303030303030303738397C002BC2DE8301000080')

    def test_driver_saiman_iec_2(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.1.6.0.0": 13,
            "1.1.6.0.1": 2,
            "1.1.6.0.2": 0,
            "1.1.6.0.3": 1,
            "1.1.6.0.4": 1,
            "1.1.6.13.1": "12345678",
            "1.1.6.13.0": 187111,
            "1.1.6.13.5": 2,
            "1.1.6.13.6": 3,
            "1.1.6.13.2": 2,
            "1.1.6.13.3": 1,
            "1.1.6.13.4": 1,
            "1.1.6.13.7.2.0": 6999,
            "1.1.6.13.7.2.1": 23,
            "1.1.6.13.7.2.2": 1,
            "1.1.6.13.7.2.3": 30,
            "1.1.6.13.7.2.4": 1,
            "1.1.6.13.7.2.5": [
                0, 1, 2, 3
            ],
            "1.1.6.13.7.1.0": 8111,
            "1.1.6.13.7.1.1": 31,
            "1.1.6.13.7.1.2": 4,
            "1.1.6.13.7.1.3": 29,
            "1.1.6.13.7.1.4": 1,
            "1.1.6.13.7.1.5": [
                0
            ],
            "1.1.6.13.7.0.0": 8000,
            "1.1.6.13.7.0.1": 21,
            "1.1.6.13.7.0.2": 2,
            "1.1.6.13.7.0.3": 5,
            "1.1.6.13.7.0.4": 0,
            "1.1.6.13.7.3.0": 1234,
            "1.1.6.13.7.3.1": 15,
            "1.1.6.13.7.3.2": 7,
            "1.1.6.13.7.3.3": 30,
            "1.1.6.13.7.3.4": 1,
            "1.1.6.13.7.3.5": [
                2, 4, 6, 8, 10, 12, 14, 16, 18, 20
            ]
        })
        e = Encoder(input_dict=test_inp)

        self.assertEqual(e.encode(),
                         '780101062F0DA06B3138373131313132333435363738EE0040BFAA00AFFFB3830100000057FBC6830F000000D2E4DD8354551500')

    def test_driver_saiman_iec_3(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.0.6.0.0": 13,
            "1.0.6.0.1": 2,
            "1.0.6.0.2": 0,
            "1.0.6.0.3": 1,
            "1.0.6.0.4": 1,
            "1.0.6.13.1": "123321",
            "1.0.6.13.0": 1,
            "1.0.6.13.5": 3,
            "1.0.6.13.6": 1,
            "1.0.6.13.2": 0,
            "1.0.6.13.3": 1,
            "1.0.6.13.4": 1,
            "1.0.6.13.7.1.0": 555,
            "1.0.6.13.7.1.1": 22,
            "1.0.6.13.7.1.2": 7,
            "1.0.6.13.7.1.3": 30,
            "1.0.6.13.7.1.4": 1,
            "1.0.6.13.7.1.5": [
                0, 31
            ],
            "1.0.6.13.7.0.0": 118,
            "1.0.6.13.7.0.1": 31,
            "1.0.6.13.7.0.2": 3,
            "1.0.6.13.7.0.3": 26,
            "1.0.6.13.7.0.4": 1,
            "1.0.6.13.7.0.5": [
                0, 3, 6, 9, 12
            ],
            "1.0.6.13.7.3.0": 4321,
            "1.0.6.13.7.3.1": 11,
            "1.0.6.13.7.3.2": 7,
            "1.0.6.13.7.3.3": 30,
            "1.0.6.13.7.3.4": 1,
            "1.0.6.13.7.3.5": [
                20, 18, 16, 14, 12, 10
            ],
            "1.1.6.0.0": 13,
            "1.1.6.0.1": 2,
            "1.1.6.0.2": 0,
            "1.1.6.0.3": 1,
            "1.1.6.0.4": 1,
            "1.1.6.13.1": 11111111,
            "1.1.6.13.0": 456789,
            "1.1.6.13.5": 2,
            "1.1.6.13.6": 3,
            "1.1.6.13.2": 2,
            "1.1.6.13.3": 1,
            "1.1.6.13.4": 1,
            "1.1.6.13.7.2.0": 6999,
            "1.1.6.13.7.2.1": 23,
            "1.1.6.13.7.2.2": 1,
            "1.1.6.13.7.2.3": 30,
            "1.1.6.13.7.2.4": 1,
            "1.1.6.13.7.2.5": [
                0, 1, 2, 3
            ],
            "1.1.6.13.7.1.0": 8111,
            "1.1.6.13.7.1.1": 31,
            "1.1.6.13.7.1.2": 4,
            "1.1.6.13.7.1.3": 29,
            "1.1.6.13.7.1.4": 1,
            "1.1.6.13.7.1.5": [
                0
            ],
            "1.1.6.13.7.0.0": 8000,
            "1.1.6.13.7.0.1": 21,
            "1.1.6.13.7.0.2": 2,
            "1.1.6.13.7.0.3": 5,
            "1.1.6.13.7.0.4": 0,
            "1.1.6.13.7.3.0": 1234,
            "1.1.6.13.7.3.1": 15,
            "1.1.6.13.7.3.2": 7,
            "1.1.6.13.7.3.3": 30,
            "1.1.6.13.7.3.4": 1,
            "1.1.6.13.7.3.5": [
                2, 4, 6, 8, 10, 12, 14, 16, 18, 20
            ]
        })
        e = Encoder(input_dict=test_inp)

        self.assertEqual(e.encode(),
                         '780101062B0DA06730303030303130303132333332317C0076E04F83491200002BC2DE8301000080E170DD8300541500062F0DA06B3435363738393131313131313131EE0040BFAA00AFFFB3830100000057FBC6830F000000D2E4DD8354551500')

    def test_driver_saiman_dala_0(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.0.6.0.0": 12,
            "1.0.6.0.1": 2,
            "1.0.6.0.2": 0,
            "1.0.6.0.3": 1,
            "1.0.6.0.4": 1,
            "1.0.6.12.1": 1,
            "1.0.6.12.0": 123,
            "1.0.6.12.5": 1,
            "1.0.6.12.6": 3,
            "1.0.6.12.2": "123456",
            "1.0.6.12.3": 3,
            "1.0.6.12.4": 1,
            "1.0.6.12.7": 1,
            "1.0.6.12.8.1.0": 5555,
            "1.0.6.12.8.1.1": 23,
            "1.0.6.12.8.1.2": 1,
            "1.0.6.12.8.1.3": [
                0, 3
            ],
            "1.0.6.12.8.0.0": 118,
            "1.0.6.12.8.0.1": 31,
            "1.0.6.12.8.0.2": 1,
            "1.0.6.12.8.0.3": [
                1, 2, 4
            ]
        })
        e = Encoder(input_dict=test_inp)

        self.assertEqual(e.encode(),
                         '78010106170CA0537B013132333435367F0076E0831600B3F5820900')

    def test_driver_saiman_dala_1(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.0.6.0.0": 12,
            "1.0.6.0.1": 2,
            "1.0.6.0.2": 0,
            "1.0.6.0.3": 1,
            "1.0.6.0.4": 1,
            "1.0.6.12.1": 1,
            "1.0.6.12.0": 123,
            "1.0.6.12.5": 1,
            "1.0.6.12.6": 3,
            "1.0.6.12.2": "123456",
            "1.0.6.12.3": 3,
            "1.0.6.12.4": 1,
            "1.0.6.12.7": 1,
            "1.0.6.12.8.1.0": 1399,
            "1.0.6.12.8.1.1": 31,
            "1.0.6.12.8.1.2": 1,
            "1.0.6.12.8.1.3": [
                0,
                3
            ],
            "1.0.6.12.8.0.0": 118,
            "1.0.6.12.8.0.1": 31,
            "1.0.6.12.8.0.2": 1,
            "1.0.6.12.8.0.3": [
                1,
                2,
                4
            ],
            "1.0.6.12.8.2.0": 7888,
            "1.0.6.12.8.2.1": 19,
            "1.0.6.12.8.2.2": 1,
            "1.0.6.12.8.2.3": [
                4
            ],
            "1.0.6.12.8.3.0": 500,
            "1.0.6.12.8.3.1": 11,
            "1.0.6.12.8.3.2": 0
        })
        e = Encoder(input_dict=test_inp)
        self.assertEqual(e.encode(),
                         '780101061F0CA05B7B013132333435367F0076E083160077E5830900D07E821000F46101')

    def test_driver_saiman_dala_2(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.1.6.0.0": 12,
            "1.1.6.0.1": 2,
            "1.1.6.0.2": 0,
            "1.1.6.0.3": 1,
            "1.1.6.0.4": 1,
            "1.1.6.12.1": 1,
            "1.1.6.12.0": 123,
            "1.1.6.12.5": 1,
            "1.1.6.12.6": 3,
            "1.1.6.12.2": "123456",
            "1.1.6.12.3": 3,
            "1.1.6.12.4": 1,
            "1.1.6.12.7": 1,
            "1.1.6.12.8.1.0": 1399,
            "1.1.6.12.8.1.1": 31,
            "1.1.6.12.8.1.2": 1,
            "1.1.6.12.8.1.3": [
                0,
                3
            ],
            "1.1.6.12.8.0.0": 118,
            "1.1.6.12.8.0.1": 31,
            "1.1.6.12.8.0.2": 1,
            "1.1.6.12.8.0.3": [
                1,
                2,
                4
            ],
            "1.1.6.12.8.2.0": 7888,
            "1.1.6.12.8.2.1": 19,
            "1.1.6.12.8.2.2": 1,
            "1.1.6.12.8.2.3": [
                4
            ],
            "1.1.6.12.8.3.0": 500,
            "1.1.6.12.8.3.1": 11,
            "1.1.6.12.8.3.2": 0,
            "1.0.6.0.0": 12,
            "1.0.6.0.1": 2,
            "1.0.6.0.2": 0,
            "1.0.6.0.3": 1,
            "1.0.6.0.4": 1,
            "1.0.6.12.1": 2,
            "1.0.6.12.0": 255,
            "1.0.6.12.5": 1,
            "1.0.6.12.6": 2,
            "1.0.6.12.2": "777777",
            "1.0.6.12.3": 2,
            "1.0.6.12.4": 1,
            "1.0.6.12.7": 0,
            "1.0.6.12.8.1.0": 900,
            "1.0.6.12.8.1.1": 31,
            "1.0.6.12.8.1.2": 1,
            "1.0.6.12.8.1.3": [
                0, 1, 2
            ],
            "1.0.6.12.8.0.0": 786,
            "1.0.6.12.8.0.1": 22,
            "1.0.6.12.8.0.2": 1,
            "1.0.6.12.8.0.3": [
                1, 3, 4
            ],
            "1.0.6.12.8.2.0": 11,
            "1.0.6.12.8.2.1": 12,
            "1.0.6.12.8.2.2": 1,
            "1.0.6.12.8.2.3": [
                2
            ],
            "1.0.6.12.8.3.0": 555,
            "1.0.6.12.8.3.1": 4,
            "1.0.6.12.8.3.2": 1,
            "1.0.6.12.8.3.3": [
                0, 1, 2, 3
            ]
        })
        e = Encoder(input_dict=test_inp)

        self.assertEqual(e.encode(),
                         '78010106210CA05DFF023737373737372E0012C3821A0084E38307000B808104002B82800F00061F0CA05B7B013132333435367F0076E083160077E5830900D07E821000F46101')

    def test_driver_saiman_plc_0(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.0.6.0.0": 11,
            "1.0.6.0.1": 2,
            "1.0.6.0.2": 0,
            "1.0.6.0.3": 1,
            "1.0.6.0.4": 1,
            "1.0.6.11.1": "222",
            "1.0.6.11.0": "255255",
            "1.0.6.11.5": 1,
            "1.0.6.11.6": 2,
            "1.0.6.11.2": 17,
            "1.0.6.11.3": 2,
            "1.0.6.11.4": 1,
            "1.0.6.11.7": 0,
            "1.0.6.11.8.1.0": 900,
            "1.0.6.11.8.1.1": 31,
            "1.0.6.11.8.1.2": 3,
            "1.0.6.11.8.1.3": 1,
            "1.0.6.11.8.1.4": [
                0, 1, 2
            ],
            "1.0.6.11.8.0.0": 786,
            "1.0.6.11.8.0.1": 22,
            "1.0.6.11.8.0.2": 1,
            "1.0.6.11.8.0.3": 1,
            "1.0.6.11.8.0.4": [
                1, 3, 4
            ]
        })
        e = Encoder(input_dict=test_inp)

        self.assertEqual(e.encode(),
                         '780101061B0BA057000000255255000222112E0012C3861A000084E38F070000')

    def test_driver_saiman_plc_1(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.1.6.0.0": 11,
            "1.1.6.0.1": 2,
            "1.1.6.0.2": 0,
            "1.1.6.0.3": 1,
            "1.1.6.0.4": 1,
            "1.1.6.11.1": "890",
            "1.1.6.11.0": "234567",
            "1.1.6.11.5": 1,
            "1.1.6.11.6": 0,
            "1.1.6.11.2": 0,
            "1.1.6.11.3": 3,
            "1.1.6.11.4": 1,
            "1.1.6.11.7": 0,
            "1.1.6.11.8.1.0": 900,
            "1.1.6.11.8.1.1": 31,
            "1.1.6.11.8.1.2": 3,
            "1.1.6.11.8.1.3": 1,
            "1.1.6.11.8.1.4": [
                0,
                1,
                2
            ],
            "1.1.6.11.8.0.0": 786,
            "1.1.6.11.8.0.1": 22,
            "1.1.6.11.8.0.2": 1,
            "1.1.6.11.8.0.3": 1,
            "1.1.6.11.8.0.4": [
                1,
                3,
                4
            ],
            "1.1.6.11.8.2.0": 6788,
            "1.1.6.11.8.2.1": 31,
            "1.1.6.11.8.2.2": 3,
            "1.1.6.11.8.2.3": 1,
            "1.1.6.11.8.2.4": [
                0, 23, 21
            ],
            "1.1.6.11.8.3.0": 999,
            "1.1.6.11.8.3.1": 20,
            "1.1.6.11.8.3.2": 0,
            "1.1.6.11.8.3.3": 0,
            "1.0.6.0.0": 11,
            "1.0.6.0.1": 2,
            "1.0.6.0.2": 0,
            "1.0.6.0.3": 1,
            "1.0.6.0.4": 1,
            "1.0.6.11.1": 999,
            "1.0.6.11.0": 341245,
            "1.0.6.11.5": 1,
            "1.0.6.11.6": 3,
            "1.0.6.11.2": 51,
            "1.0.6.11.3": 1,
            "1.0.6.11.4": 1,
            "1.0.6.11.7": 1,
            "1.0.6.11.8.1.0": 900,
            "1.0.6.11.8.1.1": 31,
            "1.0.6.11.8.1.2": 3,
            "1.0.6.11.8.1.3": 1,
            "1.0.6.11.8.1.4": [
                0,
                1,
                2
            ],
            "1.0.6.11.8.0.0": 786,
            "1.0.6.11.8.0.1": 22,
            "1.0.6.11.8.0.2": 1,
            "1.0.6.11.8.0.3": 1,
            "1.0.6.11.8.0.4": [
                1,
                3,
                4
            ],
            "1.0.6.11.8.2.0": 6788,
            "1.0.6.11.8.2.1": 31,
            "1.0.6.11.8.2.2": 3,
            "1.0.6.11.8.2.3": 1,
            "1.0.6.11.8.2.4": [
                0, 23, 21
            ],
            "1.0.6.11.8.3.0": 999,
            "1.0.6.11.8.3.1": 20,
            "1.0.6.11.8.3.2": 0,
            "1.0.6.11.8.3.3": 0
        })
        e = Encoder(input_dict=test_inp)

        self.assertEqual(e.encode(),
                         '78010106240BA060000000341245000999337D0012C3861A000084E38F07000084FA8F0100A0E7830206240BA060000000234567000890000F0012C3861A000084E38F07000084FA8F0100A0E78302')

    def test_driver_saiman_plc_2(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.0.6.0.0": 11,
            "1.0.6.0.1": 2,
            "1.0.6.0.2": 0,
            "1.0.6.0.3": 1,
            "1.0.6.0.4": 1,
            "1.0.6.11.1": "999",
            "1.0.6.11.0": "341245",
            "1.0.6.11.5": 1,
            "1.0.6.11.6": 3,
            "1.0.6.11.2": 51,
            "1.0.6.11.3": 1,
            "1.0.6.11.4": 1,
            "1.0.6.11.7": 1,
            "1.0.6.11.8.1.0": 900,
            "1.0.6.11.8.1.1": 31,
            "1.0.6.11.8.1.2": 3,
            "1.0.6.11.8.1.3": 1,
            "1.0.6.11.8.1.4": [
                0,
                1,
                2
            ],
            "1.0.6.11.8.0.0": 786,
            "1.0.6.11.8.0.1": 22,
            "1.0.6.11.8.0.2": 1,
            "1.0.6.11.8.0.3": 1,
            "1.0.6.11.8.0.4": [
                1,
                3,
                4
            ],
            "1.0.6.11.8.2.0": 6788,
            "1.0.6.11.8.2.1": 31,
            "1.0.6.11.8.2.2": 3,
            "1.0.6.11.8.2.3": 1,
            "1.0.6.11.8.2.4": [
                0, 23, 21
            ],
            "1.0.6.11.8.3.0": 999,
            "1.0.6.11.8.3.1": 20,
            "1.0.6.11.8.3.2": 0,
            "1.0.6.11.8.3.3": 0
        })
        e = Encoder(input_dict=test_inp)

        self.assertEqual(e.encode(),
                         '78010106240BA060000000341245000999337D0012C3861A000084E38F07000084FA8F0100A0E78302')

    def test_driver_mars_pulsar_0(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.0.6.0.0": 4,
            "1.0.6.0.1": 2,
            "1.0.6.0.2": 1,
            "1.0.6.0.3": 1,
            "1.0.6.0.4": 1,
            "1.0.6.4.1": 111,
            "1.0.6.4.0": "01010102",
            "1.0.6.4.5": 1,
            "1.0.6.4.2": 1,
            "1.0.6.4.3": 1,
            "1.0.6.4.4": 1,
            "1.0.6.4.6.1.0": 900,
            "1.0.6.4.6.1.1": 10,
            "1.0.6.4.6.1.2": 3,
            "1.0.6.4.6.1.3": 1,
            "1.0.6.4.6.1.4": [
                0,
                1,
                2
            ],
            "1.0.6.4.6.0.0": 786,
            "1.0.6.4.6.0.1": 22,
            "1.0.6.4.6.0.2": 1,
            "1.0.6.4.6.0.3": 1,
            "1.0.6.4.6.0.4": [
                1,
                3,
                4
            ]
        })
        e = Encoder(input_dict=test_inp)

        self.assertEqual(e.encode(),
                         '780101061404E050020101016F5C0012C3861A0084438D0700')

    def test_driver_mars_pulsar_1(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.0.6.0.0": 4,
            "1.0.6.0.1": 2,
            "1.0.6.0.2": 1,
            "1.0.6.0.3": 1,
            "1.0.6.0.4": 1,
            "1.0.6.4.1": 254,
            "1.0.6.4.0": "89129045",
            "1.0.6.4.5": 0,
            "1.0.6.4.2": 1,
            "1.0.6.4.3": 1,
            "1.0.6.4.4": 1,
            "1.0.6.4.6.1.0": 8001,
            "1.0.6.4.6.1.1": 23,
            "1.0.6.4.6.1.2": 3,
            "1.0.6.4.6.1.3": 1,
            "1.0.6.4.6.1.4": [
                0,
                1,
                2
            ],
            "1.0.6.4.6.0.0": 786,
            "1.0.6.4.6.0.1": 22,
            "1.0.6.4.6.0.2": 1,
            "1.0.6.4.6.0.3": 1,
            "1.0.6.4.6.0.4": [
                1,
                3,
                4
            ],
            "1.0.6.4.6.2.0": 564,
            "1.0.6.4.6.2.1": 13,
            "1.0.6.4.6.2.2": 0,
            "1.0.6.4.6.2.3": 1,
            "1.0.6.4.6.2.4": [
                14,
                13,
                1
            ],
            "1.0.6.4.6.3.0": 1026,
            "1.0.6.4.6.3.1": 0,
            "1.0.6.4.6.3.2": 3,
            "1.0.6.4.6.3.3": 0
        })
        e = Encoder(input_dict=test_inp)

        self.assertEqual(e.encode(),
                         '780101061C04E05845901289FE1C0012C3861A0041FF8E070034A281026002040C')

    def test_driver_mars_pulsar_2(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.0.6.0.0": 4,
            "1.0.6.0.1": 2,
            "1.0.6.0.2": 1,
            "1.0.6.0.3": 1,
            "1.0.6.0.4": 1,
            "1.0.6.4.1": 99,
            "1.0.6.4.0": "67885634",
            "1.0.6.4.5": 1,
            "1.0.6.4.2": 0,
            "1.0.6.4.3": 1,
            "1.0.6.4.4": 0,
            "1.0.6.4.6.1.0": 2300,
            "1.0.6.4.6.1.1": 17,
            "1.0.6.4.6.1.2": 0,
            "1.0.6.4.6.1.3": 1,
            "1.0.6.4.6.1.4": [
                8, 9, 10
            ],
            "1.0.6.4.6.0.0": 1122,
            "1.0.6.4.6.0.1": 15,
            "1.0.6.4.6.0.2": 1,
            "1.0.6.4.6.0.3": 1,
            "1.0.6.4.6.0.4": [
                6, 7, 8, 9, 10, 11
            ],
            "1.0.6.4.6.2.0": 8123,
            "1.0.6.4.6.2.1": 9,
            "1.0.6.4.6.2.2": 3,
            "1.0.6.4.6.2.3": 1,
            "1.0.6.4.6.2.4": [
                0, 3, 6, 9, 12, 15
            ],
            "1.0.6.4.6.3.0": 5590,
            "1.0.6.4.6.3.1": 1,
            "1.0.6.4.6.3.2": 1,
            "1.0.6.4.6.3.3": 1,
            "1.0.6.4.6.3.4": [
                1, 4, 9, 13
            ],
            "1.1.6.0.0": 4,
            "1.1.6.0.1": 2,
            "1.1.6.0.2": 1,
            "1.1.6.0.3": 1,
            "1.1.6.0.4": 1,
            "1.1.6.4.1": 254,
            "1.1.6.4.0": "89129045",
            "1.1.6.4.5": 0,
            "1.1.6.4.2": 1,
            "1.1.6.4.3": 1,
            "1.1.6.4.4": 1,
            "1.1.6.4.6.1.0": 8001,
            "1.1.6.4.6.1.1": 23,
            "1.1.6.4.6.1.2": 3,
            "1.1.6.4.6.1.3": 1,
            "1.1.6.4.6.1.4": [
                0,
                1,
                2
            ],
            "1.1.6.4.6.0.0": 786,
            "1.1.6.4.6.0.1": 22,
            "1.1.6.4.6.0.2": 1,
            "1.1.6.4.6.0.3": 1,
            "1.1.6.4.6.0.4": [
                1,
                3,
                4
            ],
            "1.1.6.4.6.2.0": 564,
            "1.1.6.4.6.2.1": 13,
            "1.1.6.4.6.2.2": 0,
            "1.1.6.4.6.2.3": 1,
            "1.1.6.4.6.2.4": [
                14,
                13,
                1
            ],
            "1.1.6.4.6.3.0": 1026,
            "1.1.6.4.6.3.1": 0,
            "1.1.6.4.6.3.2": 3,
            "1.1.6.4.6.3.3": 0
        })
        e = Encoder(input_dict=test_inp)

        self.assertEqual(e.encode(),
                         '780101061E04E05A3456886763480062E485C00FFC28820007BB3F8D4992D635841222061C04E05845901289FE1C0012C3861A0041FF8E070034A281026002040C')

    def test_driver_mirtek_0(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.0.6.0.0": 16,
            "1.0.6.0.1": 2,
            "1.0.6.0.2": 1,
            "1.0.6.0.3": 1,
            "1.0.6.0.4": 1,
            "1.0.6.16.1": 6,
            "1.0.6.16.0": 7,
            "1.0.6.16.5": 2,
            "1.0.6.16.2": 1,
            "1.0.6.16.3": 1,
            "1.0.6.16.4": 0,
            "1.0.6.16.6": 3,
            "1.0.6.16.7.1.0": 900,
            "1.0.6.16.7.1.1": 10,
            "1.0.6.16.7.1.2": 3,
            "1.0.6.16.7.1.3": 1,
            "1.0.6.16.7.1.4": [
                0,
                1,
                2
            ],
            "1.0.6.16.7.0.0": 786,
            "1.0.6.16.7.0.1": 22,
            "1.0.6.16.7.0.2": 3,
            "1.0.6.16.7.0.3": 1,
            "1.0.6.16.7.0.4": [
                1,
                3,
                4
            ],
            "1.0.6.16.7.2.0": 5123,
            "1.0.6.16.7.2.1": 19,
            "1.0.6.16.7.2.2": 3,
            "1.0.6.16.7.2.3": 1,
            "1.0.6.16.7.2.4": [
                23, 24, 25
            ],
            "1.0.6.16.7.3.0": 7600,
            "1.0.6.16.7.3.1": 22,
            "1.0.6.16.7.3.2": 3,
            "1.0.6.16.7.3.3": 1,
            "1.0.6.16.7.3.4": [
                31
            ]
        })
        e = Encoder(input_dict=test_inp)

        self.assertEqual(e.encode(),
                         '780101062210E05E77E40012C38E1A00000084438D0700000003748E00008003B0DD8E00000080')

    def test_driver_mirtek_1(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.0.6.0.0": 16,
            "1.0.6.0.1": 2,
            "1.0.6.0.2": 1,
            "1.0.6.0.3": 1,
            "1.0.6.0.4": 1,
            "1.0.6.16.1": 7,
            "1.0.6.16.0": 5,
            "1.0.6.16.5": 3,
            "1.0.6.16.2": 0,
            "1.0.6.16.3": 0,
            "1.0.6.16.4": 0,
            "1.0.6.16.6": 1,
            "1.0.6.16.7.1.0": 900,
            "1.0.6.16.7.1.1": 10,
            "1.0.6.16.7.1.2": 3,
            "1.0.6.16.7.1.3": 1,
            "1.0.6.16.7.1.4": [
                0,
                1,
                2
            ],
            "1.0.6.16.7.0.0": 786,
            "1.0.6.16.7.0.1": 22,
            "1.0.6.16.7.0.2": 3,
            "1.0.6.16.7.0.3": 1,
            "1.0.6.16.7.0.4": [
                1,
                3,
                4
            ],
            "1.0.6.16.7.2.0": 5123,
            "1.0.6.16.7.2.1": 19,
            "1.0.6.16.7.2.2": 3,
            "1.0.6.16.7.2.3": 1,
            "1.0.6.16.7.2.4": [
                23, 24, 25
            ],
            "1.0.6.16.7.3.0": 7600,
            "1.0.6.16.7.3.1": 22,
            "1.0.6.16.7.3.2": 3,
            "1.0.6.16.7.3.3": 1,
            "1.0.6.16.7.3.4": [
                31
            ]
        })
        e = Encoder(input_dict=test_inp)

        self.assertEqual(e.encode(),
                         '780101062210E05E3D700012C38E1A00000084438D0700000003748E00008003B0DD8E00000080')

    def test_driver_mirtek_2(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.1.6.0.0": 16,
            "1.1.6.0.1": 2,
            "1.1.6.0.2": 1,
            "1.1.6.0.3": 1,
            "1.1.6.0.4": 1,
            "1.1.6.16.1": 7,
            "1.1.6.16.0": 5,
            "1.1.6.16.5": 3,
            "1.1.6.16.2": 0,
            "1.1.6.16.3": 0,
            "1.1.6.16.4": 0,
            "1.1.6.16.6": 1,
            "1.1.6.16.7.1.0": 900,
            "1.1.6.16.7.1.1": 10,
            "1.1.6.16.7.1.2": 3,
            "1.1.6.16.7.1.3": 1,
            "1.1.6.16.7.1.4": [
                0,
                1,
                2
            ],
            "1.1.6.16.7.0.0": 786,
            "1.1.6.16.7.0.1": 22,
            "1.1.6.16.7.0.2": 3,
            "1.1.6.16.7.0.3": 1,
            "1.1.6.16.7.0.4": [
                1,
                3,
                4
            ],
            "1.1.6.16.7.2.0": 5123,
            "1.1.6.16.7.2.1": 19,
            "1.1.6.16.7.2.2": 3,
            "1.1.6.16.7.2.3": 1,
            "1.1.6.16.7.2.4": [
                23,
                24,
                25
            ],
            "1.1.6.16.7.3.0": 7600,
            "1.1.6.16.7.3.1": 22,
            "1.1.6.16.7.3.2": 3,
            "1.1.6.16.7.3.3": 1,
            "1.1.6.16.7.3.4": [
                31
            ],
            "1.0.6.0.0": 16,
            "1.0.6.0.1": 2,
            "1.0.6.0.2": 1,
            "1.0.6.0.3": 1,
            "1.0.6.0.4": 1,
            "1.0.6.16.1": 3,
            "1.0.6.16.0": 0,
            "1.0.6.16.5": 2,
            "1.0.6.16.2": 1,
            "1.0.6.16.3": 1,
            "1.0.6.16.4": 1,
            "1.0.6.16.6": 0,
            "1.0.6.16.7.1.0": 900,
            "1.0.6.16.7.1.1": 10,
            "1.0.6.16.7.1.2": 3,
            "1.0.6.16.7.1.3": 1,
            "1.0.6.16.7.1.4": [
                0,
                1,
                2
            ],
            "1.0.6.16.7.0.0": 786,
            "1.0.6.16.7.0.1": 22,
            "1.0.6.16.7.0.2": 3,
            "1.0.6.16.7.0.3": 1,
            "1.0.6.16.7.0.4": [
                1,
                3,
                4
            ],
            "1.0.6.16.7.2.0": 5123,
            "1.0.6.16.7.2.1": 19,
            "1.0.6.16.7.2.2": 3,
            "1.0.6.16.7.2.3": 1,
            "1.0.6.16.7.2.4": [
                23,
                24,
                25
            ],
            "1.0.6.16.7.3.0": 7600,
            "1.0.6.16.7.3.1": 22,
            "1.0.6.16.7.3.2": 3,
            "1.0.6.16.7.3.3": 1,
            "1.0.6.16.7.3.4": [
                31
            ]
        })
        e = Encoder(input_dict=test_inp)

        self.assertEqual(e.encode(),
                         '780101062210E05E582C0012C38E1A00000084438D0700000003748E00008003B0DD8E00000080062210E05E3D700012C38E1A00000084438D0700000003748E00008003B0DD8E00000080')

    def test_driver_command_saimanIEC_0(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.0.6.0.0": 13,
            "1.0.6.0.1": 2,
            "1.0.6.0.2": 1,
            "1.0.6.0.3": 1,
            "1.0.6.0.4": 3,
            "1.0.6.13.0": 0,
            "1.0.6.13.1": 0,
            "1.0.6.13.2": 0
        })
        e = Encoder(input_dict=test_inp)
        self.assertEqual(e.encode(),
                         '78010106060DE0C2000000')

    def test_driver_command_saimanIEC_1(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.0.6.0.0": 13,
            "1.0.6.0.1": 2,
            "1.0.6.0.2": 1,
            "1.0.6.0.3": 1,
            "1.0.6.0.4": 3,
            "1.0.6.13.0": 0,
            "1.0.6.13.1": 0,
            "1.0.6.13.2": 255
        })
        e = Encoder(input_dict=test_inp)
        self.assertEqual(e.encode(),
                         '78010106060DE0C20000FF')

    def test_driver_command_saimanIEC_2(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.0.6.0.0": 13,
            "1.0.6.0.1": 2,
            "1.0.6.0.2": 1,
            "1.0.6.0.3": 1,
            "1.0.6.0.4": 3,
            "1.0.6.13.0": 1,
            "1.0.6.13.1": [1,10],
            "1.0.6.13.1.1": 1,
            "1.0.6.13.1.10": 1
        })
        e = Encoder(input_dict=test_inp)
        self.assertEqual(e.encode(),
                         '78010106080DE0C40102040000')

    def test_driver_command_saimanIEC_3(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.0.6.0.0": 13,
            "1.0.6.0.1": 2,
            "1.0.6.0.2": 1,
            "1.0.6.0.3": 1,
            "1.0.6.0.4": 3,
            "1.0.6.13.0": 1,
            "1.0.6.13.1.31": 1,
            "1.0.6.13.1.0": 1
        })
        e = Encoder(input_dict=test_inp)
        self.assertEqual(e.encode(),
                         '78010106080DE0C40101000080')

    def test_driver_command_saiman_dala_0(self):

        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.0.6.0.0": 12,
            "1.0.6.0.1": 2,
            "1.0.6.0.2": 1,
            "1.0.6.0.3": 1,
            "1.0.6.0.4": 3,
            "1.0.6.12.0": 0,
            "1.0.6.12.1": 13455
        })
        e = Encoder(input_dict=test_inp)
        self.assertEqual(e.encode(),
                         '78010106070CE0C3008F3400')

    def test_driver_command_saiman_dala_1(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.1.6.0.0": 12,
            "1.1.6.0.1": 2,
            "1.1.6.0.2": 1,
            "1.1.6.0.3": 1,
            "1.1.6.0.4": 3,
            "1.1.6.12.0": 0,
            "1.1.6.12.1": 13455,

            "1.0.6.0.0": 12,
            "1.0.6.0.1": 2,
            "1.0.6.0.2": 1,
            "1.0.6.0.3": 1,
            "1.0.6.0.4": 3,
            "1.0.6.12.0": 0,
            "1.0.6.12.1": 166611

        })
        e = Encoder(input_dict=test_inp)
        self.assertEqual(e.encode(),
                         '78010106070CE0C300D38A0206070CE0C3008F3400')

    def test_driver_command_mercury20_0(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.1.6.0.0": 2,
            "1.1.6.0.1": 2,
            "1.1.6.0.2": 1,
            "1.1.6.0.3": 1,
            "1.1.6.0.4": 3,
            "1.1.6.2.0": 0,
            "1.1.6.2.1": 0,
            "1.1.6.2.2": 1
        })
        e = Encoder(input_dict=test_inp)
        self.assertEqual(e.encode(),
                         '780101060602E0C2000001')

    def test_driver_command_mercury20_1(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.1.6.0.0": 2,
            "1.1.6.0.1": 2,
            "1.1.6.0.2": 1,
            "1.1.6.0.3": 1,
            "1.1.6.0.4": 3,
            "1.1.6.2.0": 0,
            "1.1.6.2.1": 0,
            "1.1.6.2.2": 255
        })
        e = Encoder(input_dict=test_inp)
        self.assertEqual(e.encode(),
                         '780101060602E0C20000FF')

    def test_driver_command_saimanPLC_0(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.1.6.0.0": 11,
            "1.1.6.0.1": 2,
            "1.1.6.0.2": 1,
            "1.1.6.0.3": 1,
            "1.1.6.0.4": 3,
            "1.1.6.11.0": 2,
            "1.1.6.11.1": 23,
            "1.1.6.11.2": 1
        })
        e = Encoder(input_dict=test_inp)
        self.assertEqual(e.encode(),
                         '78010106070BE0C302170100')

    def test_driver_command_saimanPLC_1(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.1.6.0.0": 11,
            "1.1.6.0.1": 2,
            "1.1.6.0.2": 1,
            "1.1.6.0.3": 1,
            "1.1.6.0.4": 3,
            "1.1.6.11.0": 3,
            "1.1.6.11.1": 2,
            "1.1.6.11.2": 15
        })
        e = Encoder(input_dict=test_inp)
        self.assertEqual(e.encode(),
                         '78010106070BE0C303020F00')

    def test_driver_command_saimanPLC_2(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.1.6.0.0": 11,
            "1.1.6.0.1": 2,
            "1.1.6.0.2": 1,
            "1.1.6.0.3": 1,
            "1.1.6.0.4": 3,
            "1.1.6.11.0": 1,
            "1.1.6.11.1": 2
        })
        e = Encoder(input_dict=test_inp)
        self.assertEqual(e.encode(),
                         '78010106070BE0C301020000')

    def test_driver_command_saimanPLC_3(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.1.6.0.0": 11,
            "1.1.6.0.1": 2,
            "1.1.6.0.2": 1,
            "1.1.6.0.3": 1,
            "1.1.6.0.4": 3,
            "1.1.6.11.0": 0,
            "1.1.6.11.1": 255
        })
        e = Encoder(input_dict=test_inp)
        self.assertEqual(e.encode(),
                         '78010106050BE0C100FF')

    def test_driver_command_mirtek_0(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.1.6.0.0": 15,
            "1.1.6.0.1": 2,
            "1.1.6.0.2": 1,
            "1.1.6.0.3": 1,
            "1.1.6.0.4": 3,
            "1.1.6.15.0": 3,
            "1.1.6.15.1": 2,
            "1.1.6.15.2": 255,
            "1.1.6.15.3": 254
        })
        e = Encoder(input_dict=test_inp)
        self.assertEqual(e.encode(),
                         '78010106090FE0C50302FFFE0000')

    def test_driver_command_mirtek_1(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.1.6.0.0": 15,
            "1.1.6.0.1": 2,
            "1.1.6.0.2": 1,
            "1.1.6.0.3": 1,
            "1.1.6.0.4": 3,
            "1.1.6.15.0": 2,
            "1.1.6.15.1": 25,
            "1.1.6.15.2": 12,
            "1.1.6.15.3": 254,
            "1.1.6.15.4": 0
        })
        e = Encoder(input_dict=test_inp)
        self.assertEqual(e.encode(),
                         '780101060A0FE0C602190CFE000000')

    def test_driver_command_mirtek_2(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.1.6.0.0": 15,
            "1.1.6.0.1": 2,
            "1.1.6.0.2": 1,
            "1.1.6.0.3": 1,
            "1.1.6.0.4": 3,
            "1.1.6.15.0": 2,
            "1.1.6.15.1": 25,
            "1.1.6.15.2": 0,
            "1.1.6.15.3": 211,
            "1.1.6.15.4": 155899
        })
        e = Encoder(input_dict=test_inp)
        self.assertEqual(e.encode(),
                         '780101060A0FE0C6021900D3FB6002')

    def test_driver_command_sempal_0(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.1.6.0.0": 16,
            "1.1.6.0.1": 2,
            "1.1.6.0.2": 1,
            "1.1.6.0.3": 1,
            "1.1.6.0.4": 3,
            "1.1.6.16.0": 2,
            "1.1.6.16.1": 25,
            "1.1.6.16.2": 0,
            "1.1.6.16.3": 211,
            "1.1.6.16.4": 155899
        })
        e = Encoder(input_dict=test_inp)
        self.assertEqual(e.encode(),
                         '780101060A10E0C6021900D3FB6002')

    def test_driver_command_sempal_1(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.1.6.0.0": 16,
            "1.1.6.0.1": 2,
            "1.1.6.0.2": 1,
            "1.1.6.0.3": 1,
            "1.1.6.0.4": 3,
            "1.1.6.16.0": 2,
            "1.1.6.16.1": 31,
            "1.1.6.16.2": 3,
            "1.1.6.16.3": 56,
            "1.1.6.16.4": 1558991
        })
        e = Encoder(input_dict=test_inp)
        self.assertEqual(e.encode(),
                         '780101060A10E0C6021F0338CFC917')

    def test_driver_command_sempal_2(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.1.6.0.0": 16,
            "1.1.6.0.1": 2,
            "1.1.6.0.2": 1,
            "1.1.6.0.3": 1,
            "1.1.6.0.4": 3,
            "1.1.6.16.0": 3,
            "1.1.6.16.1": 12,
            "1.1.6.16.2": 3,
            "1.1.6.16.3": 56
        })
        e = Encoder(input_dict=test_inp)
        self.assertEqual(e.encode(),
                         '780101060910E0C5030C03380000')

    def test_request_archive_0(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.1.16.1": 1,
            "1.1.16.0": 0,
            "1.1.16.2": "2014-07-19 18:54:55",
            "1.1.16.3": 15,
            "1.0.16.0": 1,
            "1.0.16.1": 2,
            "1.0.16.2": "2019-07-19 18:54:55",
            "1.0.16.3": 62
        }
        )
        e = Encoder(input_dict=test_inp)
        self.assertEqual(e.encode(),
                         '7801011006217FCEC4243E1006107F7B5D1B0F')

    def test_request_archive_1(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.0.16.0": 0,
            "1.0.16.1": 0,
            "1.0.16.2": "2000-01-07 10:20:55",
            "1.0.16.3": 63
        })
        e = Encoder(input_dict=test_inp)
        self.assertEqual(e.encode(),
                         '780101100600877A08003F')

    def test_request_archive_not_enough_data(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.0.16.0": 1,
            "1.0.16.2": "2000-01-01 00:18:37",
            "1.0.16.3": 64
        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidAddressError, lambda: e.encode())

    def test_request_archive_invalid_value(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.0.16.0": 1,
            "1.0.16.1": 2,
            "1.0.16.2": "2000-01-01 00:18:37",
            "1.0.16.3": 65
        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidFormatOrValueError, lambda: e.encode())

    def test_request_service_info_not_enough_data(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.5.17.0": 14

        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidFormatOrValueError, lambda: e.encode())

    def test_request_service_info_not_valied_value(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.5.17.4": 0
        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidAddressError, lambda: e.encode())

    def test_request_service_0(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.5.17.0": 1,
            "1.1.17.0": 0
        })
        e = Encoder(input_dict=test_inp)
        self.assertEqual(e.encode(),
                         '780101110100110101')

    def test_request_service_1(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.1.17.0": 0
        })
        e = Encoder(input_dict=test_inp)
        self.assertEqual(e.encode(),
                         '780101110100')

    def test_serial_port_0(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.1.7.1.3": 2,
            "1.1.7.1.2": 2,
            "1.1.7.1.0": 12,
            "1.1.7.1.1": 2,
            "1.0.7.1.3": 1,
            "1.0.7.1.2": 0,
            "1.0.7.1.0": 0,
            "1.0.7.1.1": 0
        })
        e = Encoder(input_dict=test_inp)
        self.assertEqual(e.encode(),
                         '78010107040000000107040000AC02')

    def test_serial_port_1(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.0.7.1.3": 2,
            "1.0.7.1.2": 3,
            "1.0.7.1.0": 9,
            "1.0.7.1.1": 2
        })
        e = Encoder(input_dict=test_inp)
        self.assertEqual(e.encode(),
                         '78010107040000E902')

    def test_serial_port_not_enough_value(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.0.7.1.3": 2,
            "1.0.7.1.2": 3,
            "1.0.7.1.1": 2
        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidFormatOrValueError, lambda: e.encode())

    def test_serial_port_not_valid_value(self):
        test_inp = self.BASE_INPUT.copy()
        test_inp.update({
            "0.0": 1,
            "0.1.0": 1,
            "1.0.7.1.3": 2,
            "1.0.7.1.2": 3,
            "1.0.7.1.0": 19,
            "1.0.7.1.1": 2
        })
        e = Encoder(input_dict=test_inp)
        self.assertRaises(InvalidFormatOrValueError, lambda: e.encode())


if __name__ == '__main__':
    unittest.main()


def check_base(str1, str2=None):
    str1 += '00'
    byte_str = bytearray.fromhex(str1)
    int_val = struct.unpack('I', byte_str)[0]
    print('cfg:', (int_val & 0x1fff), (int_val & 0x1f << 13) >> 13, (int_val & 0x3 << 18) >> 18,
          (int_val & 0x1 << 23) >> 23)
    if str2:

        byte_str = bytearray.fromhex(str2)
        int_val = struct.unpack('I', byte_str)[0]
        bin_val = format(int_val, '032b')[::-1]
        param_list = []
        for i in range(len(bin_val)):
            if bin_val[i] == '1':
                param_list.append(i)
        print('params:', param_list)
