var debug = false;
var convert_timestamp = false;


const DEFAULT_TIME_OFFSET = 946684800;

function log(...args) {
    if (debug) console.log(args.join(' '));
}

function hexToBytes(hex) {
    for (var bytes = [], c = 0; c < hex.length; c += 2) {
        bytes.push(parseInt(hex.substr(c, 2), 16));
    }
    return bytes;
}

function createDataView(data, size) {
    var buffer = new ArrayBuffer(size);
    var uint8s = new Uint8Array(buffer);

    for (var i = 0; i < uint8s.length; i++) {
        uint8s[i] = data[i];
    }

    return new DataView(buffer);
}

function isFloat(n) {
    return Number(n) === n && n % 1 !== 0;
}

function multipl(val, factor, n=2) {
    var res = val * factor;
    if (isFloat(res))
        res = res.toFixed(n);
    return res;
}

function get_str_by_template(template, kwargs) {
    for (k in kwargs) {
        template = template.replace('%('+k+')s', kwargs[k]);
    }
    return template;
}

function unpack(format, data) {
    var data_view = createDataView(data, data.length);
    var res = '';
    var littleEndian = true;
    var offset = 0;
    var values = [];

    for (var i = 0; i < format.length; i ++) {
        var character = format.substr(i, 1);
        var add_offset = 0;

        if (character === '>') {
            littleEndian = false;
            continue;
        } else if (character == '<') {
            continue;
        }

        res = 'get';

        // Check upper
        if (character == character.toUpperCase()) {
            res += 'Uint';
        } else {
            res += 'Int';
        }

        if (character.toUpperCase() == 'B') {
            res += '8';
            add_offset += 1;
        } else if (character.toUpperCase() == 'H') {
            res += '16';
            add_offset += 2;
        } else if (character.toUpperCase() == 'I' ||
            character.toUpperCase() == 'L') {
            res += '32';
            add_offset += 4;
        } else if (character.toUpperCase() == 'Q') {
            res += '64';
            add_offset += 8;
        } else {
            return ['Wrong format'];
        }

        values.push(data_view[res](offset, littleEndian));
        offset += add_offset;

    }
    return values;
}

function bin(s) {
    var pads = new Array(1 + BytesToHex([s]).length*4).join('0');
    return (pads+s.toString(2)).slice(-BytesToHex([s]).length*4);
}

function BytesToHex(array) {
  var result = "";
  for (var i = 0; i < array.length; i++) {
    result += ('00'+array[i].toString(16)).slice(-2);
  }
  return result;
}

function strReverse (str) {
  return [...str].reverse().join('');
}

function lhex(h) {
    // swap hex to little endian
    return BytesToHex(hexToBytes(h).reverse());
}

function shift(number, shift) {
    return number * Math.pow(2, shift);
}

function parse_regular(payload_data) {
    const REGULAR_COUNTER_SIZE = 3;
    const REGULAR_FLAGS_SIZE = 1;
    const REGULAR_FLAGS = {
      0: "Канал 1 используется",
      1: "Канал 2 используется",
      2: "Канал 3 используется",
      3: "Канал 1, 32 бита (24, если 0)",
      4: "Канал 2, 32 бита (24, если 0)",
      5: "Канал 3, 32 бита (24, если 0)"
    };

    log('--------------------');

    var byte_from = 0;

    var flags_hex = BytesToHex(payload_data.slice(byte_from, byte_from + REGULAR_FLAGS_SIZE));
    byte_from += REGULAR_FLAGS_SIZE;
    
    log('flags', flags_hex);
    
    var flags = {};
    for (var i = 0; i < bin(parseInt(lhex(flags_hex), 16)).length; i++) {
        if (strReverse(bin(parseInt(lhex(flags_hex), 16)))[i] == '1') {
            flags[i] = REGULAR_FLAGS[i];
            log(flags[i]);
        }
    };

    timestamp_hex = BytesToHex(payload_data.slice(byte_from, byte_from + 4));
    byte_from += 4;
    timestamp = parseInt(lhex(timestamp_hex), 16) + 946684800;
    log('timestamp', timestamp - 946684800, timestamp_hex);

    values = {};

    for (var k = 0; k < 3; k++) {
        if (Object.keys(flags).indexOf((k).toString()) === -1)
            continue;

        var counter_num = k + 1;
        var counter_size = REGULAR_COUNTER_SIZE;

        if (Object.keys(flags).indexOf((k + 3).toString()) != -1)
            counter_size += 1;

        var byte_to = byte_from + counter_size;

        counter_hex = BytesToHex(payload_data.slice(byte_from, byte_to));

        if (counter_size == 3)
            counter_hex += '00';

        byte_from = byte_to;
        counter = parseInt(lhex(counter_hex), 16);

        // SIGNED
        if (counter > parseInt('7f' + 'f'.repeat(counter_hex.length - 2), 16))
            counter -= parseInt('10' + '0'.repeat(counter_hex.length - 1), 16);

        log('counter' + counter_num, counter, counter_hex);
        values['COUNTER' + counter_num] = counter;
    }

    payload_value = {'TIMESTAMP': timestamp};
    payload_value = Object.assign({}, payload_value, values);
    payload_data = payload_data.slice(byte_from);
    return [payload_value, payload_data];
}

function get_bits(p_in, pos, size) {
    var val = [];
    for (var i = 0; i < p_in.length; i++) {
        val.push(strReverse(bin(p_in[i])));
    };
    return parseInt(strReverse(val.join('').slice(pos, pos + size)), 2);
}


function parse_archive(payload_data, time_offset=DEFAULT_TIME_OFFSET) {
    const ARCHIVE_INIT_VAL_SIZE = 4;
    const ARCHIVE_FLAGS_SIZE = 2;

    const ARCHIVE_N_POS = 0;
    const ARCHIVE_N_MASK = 0x1f;
    const ARCHIVE_N_DESCR = "Кол-во БИТ на diff ( [0..31]+1 )";
    const ARCHIVE_DIV10_POS = 5;
    const ARCHIVE_DIV10_MASK = 0x1;
    const ARCHIVE_DIV10_DESCR = "10 DIFF";
    const ARCHIVE_RLE_POS = 6;
    const ARCHIVE_RLE_MASK = 0x1;
    const ARCHIVE_RLE_DESCR = "BIT RLE COMPRESSION";
    const ARCHIVE_SIGNED_POS = 7;
    const ARCHIVE_SIGNED_MASK = 0x1;
    const ARCHIVE_SIGNED_DESCR = "DIFF SIGNED";
    const ARCHIVE_REC_NUM_POS = 8;
    const ARCHIVE_REC_NUM_MASK = 0x3f;
    const ARCHIVE_REC_NUM_DESCR = "Кол-во записей (до 64)";
    const ARCHIVE_RESOL_POS = 14;
    const ARCHIVE_RESOL_MASK = 0x1;
    const ARCHIVE_RESOL_DESCR = "Individual bit resolution";
    const ARCHIVE_ALL_DIFF_ZERO_POS = 15;
    const ARCHIVE_ALL_DIFF_ZERO_MASK = 0x1;
    const ARCHIVE_ALL_DIFF_ZERO_DESCR = "All diff zero";

    log('--------------------');
    
    var byte_from = 0;
    
    var timestamp_hex = BytesToHex(payload_data.slice(byte_from, byte_from + 4));
    byte_from += 4;
    var timestamp = parseInt(lhex(timestamp_hex), 16) + time_offset;
    log('timestamp', timestamp - time_offset, timestamp_hex);

    var init_val_hex = BytesToHex(payload_data.slice(byte_from, byte_from + ARCHIVE_INIT_VAL_SIZE));
    byte_from += ARCHIVE_INIT_VAL_SIZE;
    var init_val = parseInt(lhex(init_val_hex), 16);

    // SIGNED
    if (init_val > parseInt('7f' + 'f'.repeat(init_val_hex.length - 2), 16))
        init_val -= parseInt('10' + '0'.repeat(init_val_hex.length - 1), 16);

    log('init_val', init_val, init_val_hex);

    var flags_hex = BytesToHex(payload_data.slice(byte_from, byte_from + ARCHIVE_FLAGS_SIZE));
    byte_from += ARCHIVE_FLAGS_SIZE;
    var flags = parseInt(lhex(flags_hex), 16);
    log('flags', flags, flags_hex, bin(flags));

    var n = (flags & ARCHIVE_N_MASK << ARCHIVE_N_POS) >> ARCHIVE_N_POS;
    var div10 = (flags & ARCHIVE_DIV10_MASK << ARCHIVE_DIV10_POS) >> ARCHIVE_DIV10_POS;
    var rle = (flags & ARCHIVE_RLE_MASK << ARCHIVE_RLE_POS) >> ARCHIVE_RLE_POS;
    var signed = (flags & ARCHIVE_SIGNED_MASK << ARCHIVE_SIGNED_POS) >> ARCHIVE_SIGNED_POS;
    var rec_num = (flags & ARCHIVE_REC_NUM_MASK << ARCHIVE_REC_NUM_POS) >> ARCHIVE_REC_NUM_POS;
    var resol = (flags & ARCHIVE_RESOL_MASK << ARCHIVE_RESOL_POS) >> ARCHIVE_RESOL_POS;
    var all_diff_zero = (flags & ARCHIVE_ALL_DIFF_ZERO_MASK << ARCHIVE_ALL_DIFF_ZERO_POS) >> ARCHIVE_ALL_DIFF_ZERO_POS;

    log('n:', n);
    log('div10:', div10);
    log('rle:', rle);
    log('signed:', signed);
    log('rec_num:', rec_num);
    log('resol:', resol);

    var data_byte = payload_data.slice(byte_from);

    var prev_value = init_val;
    var individual = resol;
    var bypass_individal_bit = false;
    var bit_num = 0;
    
    var values = [prev_value];
    var tmp;

    while (rec_num > 0) {
        if (all_diff_zero) {
            values.push(prev_value);
            rec_num -= 1;
            continue;
        }
        if (individual && !bypass_individal_bit) {
            tmp = get_bits(data_byte, bit_num, 1);
            bit_num += 1;

            if (tmp != 0) {
                n = get_bits(data_byte, bit_num, 5);
                bit_num += 5;
            }
        }

        bypass_individal_bit = false;

        if (rle == 1) {
            tmp = get_bits(data_byte, bit_num, 1);
            bit_num += 1;

            if (tmp != 0) {
                values.push(prev_value);
                rec_num -= 1;

                tmp = get_bits(data_byte, bit_num, 5);
                bit_num += 5;

                if (rec_num < tmp) {
                    return null, null;
                }

                for (var i = 0; i < tmp; i++) {
                    values.push(prev_value);
                    rec_num -= 1;
                }

                bypass_individal_bit = true;
                continue;
            }
        }
        var diff = get_bits(data_byte, bit_num, n + 1);
        bit_num += n + 1;

        if (signed && n < 31) {
            mask = 1 << n;
            diff = (diff ^ mask) - mask;
        }

        prev_value += diff;
        values.push(prev_value);
        rec_num -= 1;
    }

    byte_from += bit_num / 8;
    if (bit_num % 8) byte_from += 1;

    payload_value = {'timestamp': timestamp, 'values': values};
    payload_data = payload_data.slice(byte_from);
    return [payload_value, payload_data];
}


function parse_archive_hour_offset(payload_data) {
    return parse_archive(payload_data, time_offset=DEFAULT_TIME_OFFSET+3600)
}

function parse_tamper(payload_data) {
    const TAMPER_EV_COUNT_MASK = 0x3f;
    const TAMPER_EVENT_SIZE = 1;
    const TAMPER_EVENT = {
        0: "Геркон замкнут",
        1: "Крышка открыта",
        4: "Геркон разомкнут",
        5: "Крышка закрыта"
    }
    const TAMPER_TS_DELTA_SIZE = 2;

    var byte_from = 0;

    log('--------------------');

    var ev_count_hex = BytesToHex(payload_data.slice(byte_from, 1));
    byte_from += 1;
    var ev_count = parseInt(ev_count_hex, 16) & TAMPER_EV_COUNT_MASK;
    log('ev_count', ev_count, ev_count_hex);

    var timestamp_hex = BytesToHex(payload_data.slice(byte_from, byte_from + 4));
    byte_from += 4;
    var timestamp = parseInt(lhex(timestamp_hex)) + 946684800;
    log('timestamp', timestamp - 946684800, timestamp_hex);

    var events = [];
    if (ev_count > 0) {
        for (var event_num = 0; event_num < ev_count; event_num++) {
            var ts_delta = timestamp;

            if (event_num > 0) {
                var ts_delta_hex = BytesToHex(payload_data.slice(byte_from, byte_from + TAMPER_TS_DELTA_SIZE));
                byte_from += TAMPER_TS_DELTA_SIZE;
                ts_delta = parseInt(lhex(ts_delta_hex)) + timestamp;
                log('ts_delta', ts_delta, ts_delta_hex);
            }

            event_hex = BytesToHex(payload_data.slice(byte_from, byte_from + TAMPER_EVENT_SIZE));
            byte_from += TAMPER_EVENT_SIZE;
            event = {};
            log('event: ', event_hex);
            for (var i = 0; i < bin(parseInt(event_hex, 16)).length; i++) {
                if (strReverse(bin(parseInt(event_hex, 16)))[i] == '1') {
                    event[i] = TAMPER_EVENT[i];
                    log('EVENT', i, TAMPER_EVENT[i]);
                }
            }

            events.push({'events': event, 'timestamp': ts_delta});
        }
    }

    payload_value = {'event_count': ev_count, 'events': events};
    payload_data = payload_data.slice(byte_from);

    return [payload_value, payload_data];
}

function parse_service_data(payload_data) {
    const SERVICE_TYPE = {
        0: "Информация о версии FW и причина перезагрузки, этот payload отправляется при включении или перезагрузке",
        1: "Информация о времени работы в режиме передачи, отправляется по запросу",
        2: "CFG RESULT, отправляется как ответ на входящий пакет с конфигурацией",
        3: "CFG_ID, отправляется как ответ на входящий пакет с конфигурацией, которая имеет идентификатор (версию)",
        4: "CUR FW, отправляется как ответ (состояние и запрос следующего блока) на входящий пакет обновления FW",
        5: "Значение напряжения батареи и температура, отправляется по запросу",
        6: "RSSI_SNR_INFO, информация об RSSI и SNR для последнеого принятого пакета"
    };

    const GIT_SHA1_SIZE = 4;
    const FW_VER_SIZE = 4;
    const FW_VER_RESET_SOURCE = {
        0: "Firewall",
        1: "Options Clrd",
        2: "Reset Pin",
        3: "POR",
        4: "By software",
        5: "I-watchdog",
        6: "W-watchdog",
        7: "low power"
    };
    const FW_VER_ADDR_SIZE = 3;
    const FW_VER_ADDR_BIT23_POS = 23;
    const FW_VER_ADDR_BIT23_MASK = 0x1;

    const FW_VER_ADDR_POS = 0;
    const FW_VER_ADDR_MASK = 0x7FFFFF;

    const CFG_RESULT_SIZE = 2;

    const CFG_ID_VER_BLOCK_ID_SIZE = 1;
    const CFG_ID_SIZE = 1;

    const CFG_PL_HEADER_FORMAT = {
        0: {"name": "SYSTEM_CFG", "descr": "Время, перезагрузка и т.д."},
        1: {"name": "SCHEDULERS", "descr": "Планировщики (циклические и др., которые не относятся ко входам)"},
        2: {"name": "DI_CFG", "descr": "Конфиг. дискретных/счетных входов"},
        3: {"name": "DO_CFG", "descr": "Конфиг. дискретных/импульсных выходов"},
        4: {"name": "AI_CFG", "descr": "Конфиг. аналоговых входов"},
        5: {"name": "AO_CFG", "descr": "Конфиг. аналоговых выходов"},
        6: {"name": "SERIAL_CFG", "descr": "Конфиг. устройств на последовательном порту (включая RS485)"},
        7: {"name": "ARCHIVES", "descr": "Конфигурация архивирования"},
        8: {"name": "LIGHT_CONTROL", "descr": "Конфиг. управления освещением"},
        16: {"name": "REQUEST_ARC", "descr": "Запрос данных из архивов"}
    };

    const RSSI_INFO_SIZE = 1;
    const SNR_INFO_SIZE = 1;

    var byte_from = 0;

    log('--------------------');

    var service_type = parseInt(BytesToHex(payload_data.slice(byte_from, 1)), 16);
    byte_from += 1;
    log('service_type', service_type, SERVICE_TYPE[service_type]);

    var service_value;
    if (service_type == 0) {
        var git_sha1 = BytesToHex(payload_data.slice(byte_from, byte_from + GIT_SHA1_SIZE));
        byte_from += GIT_SHA1_SIZE;
        log('git_sha1:', git_sha1);
        
        var fw_ver = BytesToHex(payload_data.slice(byte_from, byte_from + FW_VER_SIZE));
        byte_from += FW_VER_SIZE;
        log('fw_ver:', fw_ver);

        var reset_source_hex = BytesToHex(payload_data.slice(byte_from, byte_from + 1));
        byte_from += 1;
        var reset_source_bin = strReverse(bin(parseInt(reset_source_hex, 16)));
        var reset_source = {};
        for (var i = 0; i < reset_source_bin; i++) {
            if (reset_source_bin[i] == '1') {
                reset_source[i] = FW_VER_RESET_SOURCE[i];
            }
        }
        log('reset_source:', JSON.stringify(reset_source), reset_source_hex, reset_source_bin);
        
        var addr_hex = BytesToHex(payload_data.slice(byte_from, byte_from + FW_VER_ADDR_SIZE));
        byte_from += FW_VER_ADDR_SIZE;
        var addr_bit23 = (parseInt(lhex(addr_hex), 16) & FW_VER_ADDR_BIT23_MASK << FW_VER_ADDR_BIT23_POS) >> FW_VER_ADDR_BIT23_POS;
        if (addr_bit23 == 0) {
            addr_bit23 = {0: 'general'};
        } else {
            addr_bit23 = {1: 'HardFault'};
        }
        log('addr_bit23:', JSON.stringify(addr_bit23), addr_hex);

        var address = (parseInt(lhex(addr_hex), 16) & FW_VER_ADDR_MASK << FW_VER_ADDR_POS) >> FW_VER_ADDR_POS;
        log('address:', address);

        service_value = {
            'git_sha1': git_sha1,
            'fw_ver': fw_ver,
            'reset_source': reset_source,
            'address': address,
            'addr_bit23': addr_bit23
        }
    } else if (service_type == 2) {
        data_hex = BytesToHex(payload_data.slice(byte_from, byte_from + CFG_RESULT_SIZE));
        byte_from += CFG_RESULT_SIZE;
        data = JSON.parse("[" + strReverse(bin(parseInt(lhex(data_hex), 16))) + "]");
        log('data:', data, data_hex);

        service_value = {
            'data': data
        }
    } else if (service_type == 3) {
        block_id_hex = BytesToHex(payload_data.slice(byte_from, byte_from + CFG_ID_VER_BLOCK_ID_SIZE));
        byte_from += CFG_ID_VER_BLOCK_ID_SIZE;
        block_id = parseInt(lhex(block_id_hex), 16);
        log('block_id:', block_id, block_id_hex);

        cfg_id_hex = BytesToHex(payload_data.slice(byte_from, byte_from + CFG_ID_SIZE));
        byte_from += CFG_ID_SIZE;
        cfg_id = parseInt(lhex(cfg_id_hex), 16);
        log('cfg_id:', cfg_id, cfg_id_hex);

        service_value = {
            'block_id': block_id,
            'format': CFG_PL_HEADER_FORMAT[block_id]['name'],
            'cfg_id': cfg_id
        }
    } else if (service_type == 6) {
        rssi_hex = BytesToHex(payload_data.slice(byte_from, byte_from + RSSI_INFO_SIZE));
        byte_from += RSSI_INFO_SIZE;
        rssi = parseInt(lhex(rssi_hex), 16);
        log('rssi:', rssi, rssi_hex);

        snr_hex = BytesToHex(payload_data.slice(byte_from, byte_from + SNR_INFO_SIZE));
        byte_from += SNR_INFO_SIZE;
        snr = parseInt(lhex(snr_hex), 16);
        log('snr:', snr, snr_hex);

        service_value = {
            'rssi': rssi,
            'snr': snr
        }
    }

    payload_value = {service_type: service_value, 'descr': SERVICE_TYPE[service_type]};
    payload_data = payload_data.slice(byte_from);

    return [payload_value, payload_data];
}

function parse_various(payload_data) {
    const VARIOUS_FLAGS_SIZE = 1;

    const CHANNEL_NUMBER_POS = 0;
    const CHANNEL_NUMBER_MASK = 0x7;
    const CHANNEL_NUMBER_DESCR = 'Channel number';
    const PARAMETERS_COUNT_POS = 3;
    const PARAMETERS_COUNT_MASK = 0xf;
    const PARAMETERS_COUNT_DESCR = 'Parameters count';
    const BIT16_IDS_POS = 7;
    const BIT16_IDS_MASK = 0x1;
    const BIT16_IDS_DESCR = '16 bit IDs';

    const PARAM_IDS = {
        0: {'size': 8, 'name': 'GNSS Simple', 'descr': 'Координаты GNSS, сокращенный вариант'},
        1: {'size': 0, 'name': 'GNSS Extended', 'descr': 'Координаты GNSS, расширенный вариант'},
        2: {'size': 2, 'name': 'Speed', 'descr': 'Скорость'},
        3: {'size': 2, 'name': 'Altitude', 'descr': 'Высота'},
        4: {'size': 2, 'name': 'Azimuth', 'descr': 'Азимут'},
        5: {'size': 2, 'name': 'Temperature', 'descr': 'Температура'},
        6: {'size': 2, 'name': 'Pressure', 'descr': 'Давление'},
        7: {'size': 1, 'name': 'Humidity', 'descr': 'Влажность'},
        8: {'size': 2, 'name': 'Illumination', 'descr': 'Попугаи, uint16_t Освещенность'},
        9: {'size': 2, 'name': 'CO2', 'descr': 'Углекислый газ, ppm'},
        10: {'size': 2, 'name': 'Phase', 'descr': 'Фаза'},
        11: {'size': 2, 'name': 'Frequency', 'descr': 'Частота'},
        12: {'size': 2, 'name': 'Voltage0', 'descr': 'В, множитель x10 Напряжение, фаза A'},
        13: {'size': 2, 'name': 'Voltage1', 'descr': 'В, множитель x10 Напряжение, фаза B'},
        14: {'size': 2, 'name': 'Voltage2', 'descr': 'В, множитель x10 Напряжение, фаза C'},
        15: {'size': 2, 'name': 'Current0', 'descr': 'мА  Ток, фаза A'},
        16: {'size': 2, 'name': 'Current1', 'descr': 'мА  Ток, фаза B'},
        17: {'size': 2, 'name': 'Current2', 'descr': 'мА  Ток, фаза C'},
    };

    log('--------------------');
    var byte_from = 0;

    var timestamp_hex = BytesToHex(payload_data.slice(byte_from, byte_from + 4));
    byte_from += 4;
    var timestamp = parseInt(lhex(timestamp_hex), 16) + 946684800;
    log('timestamp', timestamp - 946684800, timestamp_hex);

    var flags_hex = BytesToHex(payload_data.slice(byte_from, byte_from + VARIOUS_FLAGS_SIZE));
    byte_from += VARIOUS_FLAGS_SIZE;
    var flags_int = parseInt(flags_hex, 16);
    
    var chanel_num = (flags_int & CHANNEL_NUMBER_MASK << CHANNEL_NUMBER_POS) >> CHANNEL_NUMBER_POS;
    var params_count = (flags_int & PARAMETERS_COUNT_MASK << PARAMETERS_COUNT_POS) >> PARAMETERS_COUNT_POS;
    var b16_ids = (flags_int & BIT16_IDS_MASK << BIT16_IDS_POS) >> BIT16_IDS_POS;
    
    var flags = {
        [CHANNEL_NUMBER_DESCR]: chanel_num,
        [PARAMETERS_COUNT_DESCR]: params_count,
        [BIT16_IDS_DESCR]: b16_ids
    };

    log('flags', flags_hex);
    for (var x in flags) {
        log(x, flags[x]);
    };

    var params = {};
    if (params_count > 0) {
        for (var p = 0; p < params_count; p++) {
            var id_len = 1
            if (b16_ids == '1') id_len += 1;

            var param_id_hex = BytesToHex(payload_data.slice(byte_from, byte_from + id_len));
            byte_from += id_len;
            var param_id = parseInt(lhex(param_id_hex), 16);
            var param = PARAM_IDS[param_id];
            log('param', param_id, JSON.stringify(param), param_id_hex)

            var factor = 1;
            if ([12, 13, 14].includes(param_id)) factor = 10;

            var value_hex = BytesToHex(payload_data.slice(byte_from, byte_from + param['size']));
            byte_from += param['size'];
            var value = parseInt(lhex(value_hex), 16) / factor;
            
            // Parse GNSS Simple
            if (param_id == 0) {
                var latitude_val = value & 0xFFFFFFFF;
                var latitude = (latitude_val >> 23) + (latitude_val & 0x7FFFFF) / 10000 / 60;

                var longitude_val = shift(value, -32) & 0xFFFFFFFF;
                var longitude = (longitude_val >> 23) + (longitude_val & 0x7FFFFF) / 10000 / 60;
                
                value = {'latitude': latitude, 'longitude': longitude};
            }

            log('value', JSON.stringify(value), value_hex);

            params[param['name']] = value;
        }
    }

    payload_value = {'TIMESTAMP': timestamp, 'FLAGS': flags, 'PARAMS': params};
    payload_data = payload_data.slice(byte_from);
    return [payload_value, payload_data];
}

function parse_schedule_lighting(data) {
    const PROP_MODE = {
        0: 'Запись не используется',
        1: 'Указанное в timeoffset з',
        2: 'Относительно восхода',
        3: 'Относительно заката',
    }

    const PROP_COMMAND = {
        0: 'Отключить',
        1: 'Включить (фиксир. диммирование)',
        2: 'Включить (димм. от освещенности)',
    }

    const DATE_FORMAT = {
        0: 'ONLY DOW',
        1: 'WEEKNO / DOW',
        2: 'MONTH / DOW',
        3: 'EXACT DATE',
    }

    const MONTHS = {
        52: 'весь январь',
        53: 'весь февраль',
        54: 'весь март',
        55: 'весь апрель',
        56: 'весь май',
        57: 'весь июнь',
        58: 'весь июль',
        59: 'весь август',
        60: 'весь сентябрь',
        61: 'весь октябрь',
        62: 'весь ноябрь',
        63: 'весь декабрь',
    }

    const DOW = {
        0: 'понедельник',
        1: 'вторник',
        2: 'среда',
        3: 'четверг',
        4: 'пятница',
        5: 'суббота',
        6: 'воскресенье',
    }

    data = hexToBytes(data);

    var prop = unpack('B', data.slice(0, 1))[0];
    var dim = unpack('B', data.slice(1, 2))[0];
    // var rfu = unpack('B', data.slice(2, 3))[0];
    // var rfu = unpack('B', data.slice(3, 4))[0];
    var week_month = unpack('B', data.slice(4, 5))[0];
    var day_of_week = unpack('B', data.slice(5, 6))[0];

    var prop_mode = prop & 0x3;
    var prop_mode_descr = PROP_MODE[prop_mode];
    var prop_ifnogeo = (prop & 0x1 << 2) >> 2;
    var prop_priority = (prop & 0x3 << 3) >> 3;
    var prop_command = (prop & 0x7 << 5) >> 5;
    var prop_command_descr = PROP_COMMAND[prop_command];

    var dimming = dim & 0x7f;

    var date_format = week_month & 0x3;
    var date_format_descr = DATE_FORMAT[date_format];

    var week_month_val = (week_month & 0x3f << 2) >> 2;

    var week_no = {};
    var month = {};
    if (date_format == 1 || date_format == 2) {
        if (week_month_val <= 51) {
            week_no = {'week_no': week_month_val};
        } else {
            month = {'month': week_month_val};
            var month_descr = MONTHS[month];
        }
    }

    var dow = {};
    var date = {};
    if (date_format < 3) {
        dow = strReverse(bin(day_of_week & 0x7f).slice(1));
        var dow_list = [];
        var dow_descr = [];
        for (var i = 0; i < dow.length; i++) {
            dow_list.push(parseInt(dow[i]));
            dow_descr.push(DOW[parseInt(dow[i])]);
        }
        dow = {'day_of_week': dow_list};
    } else {
        date = {'date': day_of_week & 0xf};
    }

    var high_bit = day_of_week > 127 ? '01' : '00';
    var time_offset_hex = high_bit + BytesToHex(data.slice(6).reverse());
    var time_offset = parseInt(time_offset_hex, 16);

    if ((prop_mode == 2 || prop_mode == 3) && time_offset > 65535) {
        time_offset = (131072 - time_offset) * -1;
    }

    var scheduler = {
        'prop_mode': prop_mode,
        'prop_ifnogeo': prop_ifnogeo,
        'prop_priority': prop_priority,
        'prop_command': prop_command,
        'dimming': dimming,
        'date_format': date_format,
        'time_offset': time_offset,
    }

    scheduler = Object.assign(scheduler, week_no, month, dow, date);

    return scheduler;
}

function parse_lighting(payload_data) {
    const LIGHTING_TIMESTAMP_SIZE = 4;
    const LIGHTING_FLAGS_SIZE = 1;
    const LIGHTING_PARAM_COUNT_POS = 3;
    const LIGHTING_PARAM_COUNT_MASK = 0xf;
    const LIGHTING_PARAM_COUNT_DESCR = 'Кол-во параметров + 1 (т.е. значение 0 = 1 параметр, значение 15 = 16 параметров )';

    const PARAMS = {
        0: {'size': 8, 'name': 'position_simple', 'format': 'II', 'descr': 'packed data'},
        1: {'size': 1, 'name': 'temperature', 'format': 'b', 'descr': 'degC'},
        2: {'size': 2, 'name': 'temp_min_max', 'format': 'bb', 'descr': 'degC'},
        3: {'size': 2, 'name': 'phase', 'format': 'BH', 'descr': 'мкс относительно 1PPS на начало часа'},
        4: {'size': 2, 'name': 'frequency', 'format': 'H', 'descr': 'Гц, коэффициент 0,01', 'factor': 100},
        5: {'size': 2, 'name': 'voltage0', 'format': 'H', 'descr': 'В, коэффициент x0.1'},
        6: {'size': 2, 'name': 'current0', 'format': 'H', 'descr': 'мА'},
        7: {'size': 4, 'name': 'energy_A+', 'format': 'I', 'descr': 'кВт, коэффициент 0,01'},
        8: {'size': 4, 'name': 'energy_A-', 'format': 'I', 'descr': 'кВт, коэффициент 0,01'},
        9: {'size': 4, 'name': 'energy_R+', 'format': 'I', 'descr': 'кВАр, коэффициент 0,01'},
        10: {'size': 4, 'name': 'energy_R-', 'format': 'I', 'descr': 'кВАр, коэффициент 0,01'},
        21: {'size': 2, 'name': 'illumination', 'format': 'H', 'descr': 'попугаи, uint16_t'},
        22: {'size': 4, 'name': 'worktime_lighting', 'format': 'I', 'descr': 'сек'},
        23: {'size': 4, 'name': 'worktime_lamp', 'format': 'I', 'descr': 'сек'},
        32: {'size': 9, 'name': 'schedule_lighting', 'format': 'xxxxxxxbQ', 'descr': 'packed data'},
    };

    log('--------------------');
    var byte_from = 0;

    var timestamp_hex = BytesToHex(payload_data.slice(byte_from, byte_from + LIGHTING_TIMESTAMP_SIZE));
    byte_from += LIGHTING_TIMESTAMP_SIZE;
    var timestamp = parseInt(lhex(timestamp_hex), 16) + 946684800;
    log('timestamp', timestamp - 946684800, timestamp_hex);

    var flags_hex = BytesToHex(payload_data.slice(byte_from, byte_from + LIGHTING_FLAGS_SIZE));
    byte_from += LIGHTING_FLAGS_SIZE;
    var param_count = parseInt(flags_hex, 16);
    param_count = (param_count & LIGHTING_PARAM_COUNT_MASK << LIGHTING_PARAM_COUNT_POS) >> LIGHTING_PARAM_COUNT_POS;
    param_count += 1
    log('param_count', param_count)

    var params = {};
    if (param_count > 0) {
        for (var i = 0; i < param_count; i++) {
            var param_id_hex = BytesToHex(payload_data.slice(byte_from, byte_from + 1));
            byte_from += 1;
            var param_id = parseInt(lhex(param_id_hex), 16);
            var param = PARAMS[param_id];

            var value_hex = BytesToHex(payload_data.slice(byte_from, byte_from + param['size']));
            byte_from += param['size'];

            log(param_id, value_hex, param['name']);

            if (param_id == 32) {
                var cell_index = parseInt(lhex(value_hex.slice(0, 2)), 16);
                var schedule = parse_schedule_lighting(value_hex.slice(2));
                var value = {'cell_index': cell_index, 'schedule': schedule};
            } else {
                var value = unpack(param['format'], hexToBytes(value_hex));
            }

            if (param['format'].length == 1) {
                var factor = 1;
                if (param['factor']) {
                    factor = param['factor'];
                }
                value = value[0] / factor;
            }

            if (param_id == 0) {
                var latitude_val = value[0] & 0xFFFFFFFF;
                var latitude = (latitude_val >> 23) + (latitude_val & 0x7FFFFF) / 10000 / 60;

                var longitude_val = value[1] & 0xFFFFFFFF;
                var longitude = (longitude_val >> 23) + (longitude_val & 0x7FFFFF) / 10000 / 60;
                
                value = {'latitude': latitude, 'longitude': longitude};
            }

            log('value', JSON.stringify(value), value_hex);

            params[param['name']] = value;
        }

    }

    payload_value = {'TIMESTAMP': timestamp, 'PARAMS': params};
    payload_data = payload_data.slice(byte_from);
    return [payload_value, payload_data];
}

function parse_electric(payload) {
    const TIMESTAMP = {'SIZE': 4, 'POS': 0, 'FORMAT': 'I'};
    
    const FLAGS = {'SIZE': 1, 'POS': 4, 'FORMAT': 'B'};
    const DEVICE_NUMBER = {'MASK': 0x3, 'POS': 0, 'DESCR': 'Номер устройства на шине (0..3)'};
    const PARAMS_COUNT = {'MASK': 0x1f, 'POS': 2, 'DESCR': 'Количество параметров + 1'};

    const PARAM_HDR = {'SIZE': 1, 'POS': 5, 'FORMAT': 'B'};
    const PARAM_ID = {'MASK': 0x7, 'POS': 0, 'DESCR': 'Параметр'};
    const PARAM_ID_VALUES = {
        0: {'name': 'power', 'size': 2, 'format': 'H', 'factor': 10, 'template': 'CURP%(character)s%(phase)s', 'descr': 'Мощность, Вт'},
        1: {'name': 'full_power', 'size': 2, 'format': 'H', 'factor': 10, 'template': 'CURFP%(character)s%(phase)s', 'descr': 'Мощность полная, ВА'},
        2: {'name': 'energy', 'size': 4, 'format': 'I', 'factor': 10, 'template': 'TOTAE%(tariff)s_%(character)s%(direction)s', 'descr': 'Энергия, Вт*ч / ВАр*ч'},
        3: {'name': 'voltage', 'size': 2, 'format': 'H', 'factor': 0.1, 'template': 'CURV%(phase)s', 'descr': 'Напряжение, В'},
        4: {'name': 'amperage', 'size': 2, 'format': 'H', 'factor': 0.1, 'template': 'CURI%(phase)s', 'descr': 'Ток, А'},
        5: {'name': 'power_factor', 'size': 2, 'format': 'H', 'factor': 0.01, 'template': 'COS_PHI%(phase)s', 'descr': 'Коэффициент мощности (cos phi), %'},
        6: {'name': 'frequency', 'size': 2, 'format': 'H', 'factor': 0.01, 'template': 'FREQ', 'descr': 'Частота, Гц'},
    };
    const DIRECTION = {'MASK': 0x1, 'POS': 3, 'DESCR': 'Направление'};
    const DIRECTION_VALUES = {
        0: {'name': '+', 'descr': 'Потребление'},
        1: {'name': '-', 'descr': 'Генерация'},
    };

    const CHARACTER = {'MASK': 0x1, 'POS': 4, 'DESCR': 'Характер'};
    const CHARACTER_VALUES = {
        0: {'name': 'A', 'descr': 'Активная'},
        1: {'name': 'R', 'descr': 'Реактивная'},
    };

    const PHASE_TARIFF = {'MASK': 0x7, 'POS': 5, 'DESCR': 'Фаза/Тариф'};
    const PHASE_VALUES = {
        0: {'name': '', 'descr': 'однофазный / сумма или не применимо понятие "фаза"'},
        1: {'name': '_A', 'descr': 'A'},
        2: {'name': '_B', 'descr': 'B'},
        3: {'name': '_C', 'descr': 'C'},
        4: {'name': '_ABC', 'descr': 'ABC (в value передается массив из 3-х значений)'},
        5: {'name': 'n/a', 'descr': 'n/a'},
        6: {'name': 'n/a', 'descr': 'n/a'},
        7: {'name': 'n/a', 'descr': 'n/a'},
    };
    const TARIFF_VALUES = {
        0: {'name': '', 'descr': 'Без тарифа'},
        1: {'name': '1', 'descr': 'Тариф 1'},
        2: {'name': '2', 'descr': 'Тариф 2'},
        3: {'name': '3', 'descr': 'Тариф 3'},
        4: {'name': '4', 'descr': 'Тариф 4'},
        5: {'name': '1/2/3/4', 'descr': 'T1/2/3/4 (в value передается массив из 4-х значений)'},
        6: {'name': 'n/a', 'descr': 'n/a'},
        7: {'name': 'n/a', 'descr': 'n/a'},
    };

    log('--------------------');

    var timestamp = unpack(TIMESTAMP['FORMAT'], payload.slice(TIMESTAMP['POS'], TIMESTAMP['POS'] + TIMESTAMP['SIZE']))[0] + DEFAULT_TIME_OFFSET;

    if (convert_timestamp) 
        timestamp = dt_from_ts(timestamp);

    log('timestamp', timestamp);

    var flags = unpack(FLAGS['FORMAT'], payload.slice(FLAGS['POS'], FLAGS['POS'] + FLAGS['SIZE']))[0];
    var device_number = (flags & DEVICE_NUMBER['MASK'] << DEVICE_NUMBER['POS']) >> DEVICE_NUMBER['POS'];
    var params_count = ((flags & PARAMS_COUNT['MASK'] << PARAMS_COUNT['POS']) >> PARAMS_COUNT['POS']) + 1;

    log('flags', flags);
    log('- device_number', device_number);
    log('- params_count', params_count);

    var params = {};
    var pos_offset = PARAM_HDR['POS'];
    for (var i = 0; i < params_count; i++) {
        var param_hdr = unpack(PARAM_HDR['FORMAT'], payload.slice(pos_offset, pos_offset + PARAM_HDR['SIZE']))[0];
        pos_offset += PARAM_HDR['SIZE'];
        
        var param_id = (param_hdr & PARAM_ID['MASK'] << PARAM_ID['POS']) >> PARAM_ID['POS'];
        var param_id_value = PARAM_ID_VALUES[param_id];

        var direction = (param_hdr & DIRECTION['MASK'] << DIRECTION['POS']) >> DIRECTION['POS'];
        var direction_value = DIRECTION_VALUES[direction];

        var character = (param_hdr & CHARACTER['MASK'] << CHARACTER['POS']) >> CHARACTER['POS'];
        var character_value = CHARACTER_VALUES[character];

        var phase_tariff = (param_hdr & PHASE_TARIFF['MASK'] << PHASE_TARIFF['POS']) >> PHASE_TARIFF['POS'];

        if (phase_tariff == 4 && param_id_value['template'].indexOf('phase') != -1) {
            param_id_value['format'] = param_id_value['format'].repeat(3);
            param_id_value['size'] *= 3;
        } else if (phase_tariff == 5 && param_id_value['template'].indexOf('tariff') != -1) {
            param_id_value['format'] = param_id_value['format'].repeat(4);
            param_id_value['size'] *= 4;
        };

        var value_trans = {
            'name': param_id_value['name'],
            'direction': direction_value['name'],
            'character': character_value['name'],
            'phase': PHASE_VALUES[phase_tariff]['name'],
            'tariff': TARIFF_VALUES[phase_tariff]['name'],
        };

        var value_name = get_str_by_template(param_id_value['template'], value_trans);
        var parsed_values = unpack(param_id_value['format'], payload.slice(pos_offset, pos_offset + param_id_value['size']));

        parsed_values = parsed_values.map(v => multipl(v, param_id_value['factor']));
        
        if (parsed_values.length == 1)
            parsed_values = parsed_values[0];

        params[value_name] = parsed_values;

        pos_offset += param_id_value['size'];

        log('-----');
        log('param_hdr', param_hdr);
        log('param_id_value', param_id_value['name'], param_id_value['descr']);
        log('direction_value', direction_value['name'], direction_value['descr']);
        log('character_value', character_value['name'], character_value['descr']);
        log('phase', PHASE_VALUES[phase_tariff]['name'], PHASE_VALUES[phase_tariff]['descr']);
        log('tariff', TARIFF_VALUES[phase_tariff]['name'], TARIFF_VALUES[phase_tariff]['descr']);
        log('value', value_name, parsed_values);
    };

    var payload_value = {'TIMESTAMP': timestamp, 'PARAMS': params};
    payload = payload.slice(pos_offset);
    return [payload_value, payload];
}

function arzamas_decoder(data) {
    const PACKET_FLAGS = {
        0: "Батарея разряжена",
        1: "Низкая температура",
        2: "Высокая температура",
        3: "Геркон, состояние",
        4: "Датчик вскрытия, состояние",
        5: "Время устройства требует обновления"
    };

    const PAYLOAD_FORMATS = {
        0: {'name': "regular", 'action': parse_regular},
        1: {'name': "archive, day, CNT0", 'action': parse_archive_hour_offset},
        2: {'name': "archive, day, CNT0", 'action': parse_archive},
        3: {'name': "archive, day, CNT1", 'action': parse_archive_hour_offset},
        4: {'name': "archive, day, CNT1", 'action': parse_archive},
        5: {'name': "archive, day, CNT2", 'action': parse_archive_hour_offset},
        6: {'name': "archive, day, CNT2", 'action': parse_archive},
        7: {'name': "tamper", 'action': parse_tamper},
        8: {'name': "service", 'action': parse_service_data},
        16: {'name': "lighting", 'action': parse_lighting},
        17: {'name': "electric", 'action': parse_electric},
        254: {'name': "various", 'action': parse_various}
    };

    var byte_data = hexToBytes(data);

    // PACKET HEADER
    log('DATA:', data);
    log('DATA LEN:', byte_data.length);

    log('\n---PACKET----------');

    var packet_id = parseInt(data.substr(0, 2));
    var packet_seq = byte_data[1];
    var packet_flags = {};

    for (var i = 0; i < byte_data[2]; i++) {
        if (strReverse(byte_data[2].toString(2))[i] == '1') packet_flags[i] = PACKET_FLAGS[i];
    }

    byte_data.splice(0, 3);
    payload = byte_data;

    log('packet_id:', packet_id);
    log('packet_seq:', packet_seq);

    var packet_value = {'packet_flags': packet_flags};

    log('packet_flags:');
    for (var x in packet_flags) {
        log(packet_flags[x]);
    };

    while (payload.length > 0) {
        // PAYLOAD HEADER
        var payload_format_num = payload[0];

        if (!(payload_format_num in PAYLOAD_FORMATS))
            return {'Error': 'Invalid data format'};

        var payload_format = PAYLOAD_FORMATS[payload_format_num];
        var payload_data = payload.slice(1);
        var payload_value = null;

        log('\n+++PAYLOAD+++++++++');
        log('payload_data', BytesToHex(payload_data));
        log('payload_format:', payload_format_num, payload_format['name']);

        [payload_value, payload] = payload_format['action'](payload_data);

        if (payload_value) packet_value[payload_format['name']] = payload_value;
    }

    log('\npacket_value:', JSON.stringify(packet_value));
    return packet_value;
}

module.exports = {
    decoder: arzamas_decoder
}
