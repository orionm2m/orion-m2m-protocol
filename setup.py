from setuptools import setup, find_packages
from os.path import join, dirname

setup(
    name='decoderorion',
    version='1.0',
    packages=find_packages(),
    package_data={'': ['*.json']},
    long_description=open(join(dirname(__file__), 'README.txt')).read(),
)