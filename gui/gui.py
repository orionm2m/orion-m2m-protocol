# -- coding: utf-8 --
import sys, os
import json
sys.path.append('..')

from orionm2m.data.decoder import Decoder
from windows import Ui_MainWindow
from PyQt5.QtWidgets import QApplication, QMainWindow, qApp
from PyQt5.QtGui import QIcon, QPixmap

def resource_path(relative_path):
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS, relative_path)
    return os.path.join(os.path.abspath("."), relative_path)

class orionm2m_gui(QMainWindow):
    def __init__(self):
        super(orionm2m_gui, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)        
        self.setFixedSize(self.size())
        self.ui.pushButton.clicked.connect(self.btnClicked)
        self.ui.action_Exit.triggered.connect(self.exitClicked)
        
    def exitClicked(self):
        qApp.quit()

    def btnClicked(self):
        input = self.ui.payload.text()  
        lang = self.ui.lang.currentText()    
        try:
            output = Decoder(input, lang)
            self.ui.decodedJson.setText(json.dumps(output.final_output(), indent=4,ensure_ascii=False))
        except:
            self.ui.decodedJson.setText('ERROR')


def main():
    app = QApplication([])
    application = orionm2m_gui()
    #path = os.path.join(os.path.dirname(sys.modules[__name__].__file__), 'logo.png')
    app.setWindowIcon(QIcon(resource_path('images/icon.png')))
    application.show()
    sys.exit(app.exec())


if __name__ == '__main__':
    main()