const arzamas = require('./arzamas_decoder.js');

var BDT = require('./byte_data_type.json');

function hexToBytes(hex) {
    for (var bytes = [], c = 0; c < hex.length; c += 2)
        bytes.push(parseInt(hex.substr(c, 2), 16));
    return bytes;
}

function BytesToHex(array) {
  var result = "";
  for (var i = 0; i < array.length; i++) {
    result += array[i].toString(16);
  }
  return result;
}


function createDataView(data, size) {
    var buffer = new ArrayBuffer(size);
    var uint8s = new Uint8Array(buffer);

    for (var i = 0; i < uint8s.length; i++) {
        uint8s[i] = data[i];
    }

    return new DataView(buffer);
}

function getFormatSize(f, s) {
    var f_size = s
    if (f == 'short') {
        f_size = 2
    } else if (f == 'int' || f == 'float') {
        f_size = 4
    } else if (f == 'long' || f == 'double') {
        f_size = 8
    } 
    return f_size;
}

function format_chr(f, o=null) {
    var f_chr = null;
    var param = '';

    if (f == 'char') {
        f_chr = param + 'getUint8'
    } else if (f == 'signed char') {
        f_chr = 'getUint8'
    } else if (f == 'short') {
        f_chr = 'getUint16'
    } else if (f == 'int') {
        f_chr = 'getUint32'
    } else if (f == 'long') {
        f_chr = 'getUint64'
    } else if (f == 'float') {
        f_chr = 'getFloat32'
    } else if (f == 'double') {
        f_chr = 'getUint64'
    } else if (f == 'string') {
        f_chr = 'getUint8'
    }
    return f_chr;
}

function meter_decode(data) {
    data = hexToBytes(data);
    data.splice(0, 4);

    var payload = data;
    var good_data = [];
    var events = [];

    while (payload.length > 0) {
        var payload_dv = createDataView(payload, 2);
        var p_type = payload_dv.getUint8(0);
        var p_len = payload_dv.getUint8(1);

        payload.splice(0, 2);
        var p_data = payload.splice(0, p_len-2);
        
        // console.log('------------');
        // console.log('p_len', p_len);
        // console.log('p_data', p_data);

        if (!(p_type in BDT)) break;

        var part_data = {};

        while (p_data.length > 0){

            if (p_type == 4) {
                var d_dv = createDataView(p_data, 6);
                var timestamp = d_dv.getUint32(0, true) + 946684800;
                var source = BDT[p_type][d_dv.getUint8(4)]['name'];
                var code = d_dv.getUint8(5);
                
                events.push({'TIMESTAMP': timestamp, 'SOURCE': source});
                p_data = p_data.splice(6, p_len);
                continue;
            }

            var p_dv = createDataView(p_data, 1);
            var d_type = p_dv.getUint8(0);

            // console.log('+++++++++');
            // console.log('d_type', d_type);

            if (!(d_type in BDT[p_type])) break;

            var f = BDT[p_type][d_type].format;
            var s = BDT[p_type][d_type].size;
            var n = BDT[p_type][d_type].name;
            var o = BDT[p_type][d_type].opt;
            var d = BDT[p_type][d_type].discr;

            p_data.splice(0, 1);

            // console.log('bytes: ', p_data);
            // console.log('format', f);
            // console.log('size', s);
            // console.log('name', n);
            
            if (f === null || f.length < 1) {
                console.log("Data type has't format");
                console.log(d_type, BDT[p_type][d_type]);
                break;
            }

            var d_data;
            if (f == 'list') {
                d_data = [];
                var val_data;
                for (var i in o) {
                    var val_f = o[i]['format'];
                    var val_s = o[i]['size'];
                    var val_n = o[i]['name'];
                    var val_o = o[i]['opt'];
                    var val_d = o[i]['discr'];

                    if (val_f == 'bin') {
                        if (val_o == '<') {
                            val_data = p_data.splice(0, val_s).reverse();
                        } else {
                            val_data = p_data.splice(0, val_s);
                        }

                        var bytes = '';
                        for (var i = 0; i < val_data.length; i++) {
                            var byte = val_data[i].toString(16);
                            bytes += byte.length == 1 ? '0' + byte : byte;
                        }
                        var res = {};
                        res[val_n] = bytes;
                        d_data.push(res);
                        // part_data[val_n] = bytes;

                    } else if (val_f == 'boolean') {
                        var val_v =  o[i]['values'];
                        val_data = p_data.splice(0, val_s);

                        var bits = '';
                        for (var i = 0; i < val_data.length; i++) {
                            bits += val_data[i].toString(2);
                        }

                        bits = '0'.repeat(8*val_s).slice(0, 8*val_s-bits.length) + bits;
                        bits = bits.split("").reverse().join("");

                        var val_res = {};
                        val_res[val_n] = [];
                        for (var k in val_v) {
                            var vval = {};
                            vval[val_v[k]] = bits[k];
                            val_res[val_n].push(vval);
                            // part_data[val_v[k]] = bits[k];
                        }

                        d_data.push(val_res);

                    } else if (val_f == 'ADDR'){
                        var ddata = p_data.splice(0, val_s).reverse()
                        if (parseInt(BytesToHex(ddata), 16) & 0x00800000) {
                            d_data.push({'GH': 'HardFault'});
                        } else {
                            d_data.push({'GH': 'General'});
                        }
                        var addr = (parseInt(BytesToHex(ddata), 16) & 0x007FFFFF) | 0x08000000
                        d_data.push({'Addr': addr});
                    } else {
                        var d_d = p_data.splice(0, val_s);
                        var d_dv = createDataView(d_d, val_s);
                        val_data = d_dv[format_chr(val_f, val_o)](0, true);
                        if (val_d != null) {
                            val_data *= val_d;
                        }
                        var res = {};
                        res[val_n] = val_data;
                        d_data.push(res);
                        // part_data[val_n] = val_data;
                    }
                }

            } else {

                // Add null bytes
                if (f == 'int' && s < 4) {
                    for (var i = 0; i < 4-s; i++) {
                        p_data.splice(s, 0, 0);
                    }
                    s = 4;
                }

                var d_count = s / getFormatSize(f, s);
                if (d_count > 1) {
                    d_data = []
                    var d_d = p_data.splice(0, s);
                    var data_view = createDataView(d_d, s);

                    for (var i = 0; i < d_count; i++) {
                        var d_data_part = data_view[format_chr(f, o)](i*2, true);
                        
                        if (d != null) {
                            d_data_part *= d;
                        }

                        d_data.push(d_data_part);
                    }
                } else {
                    var d_d = p_data.splice(0, s);
                    var data_view = createDataView(d_d, s);
                    d_data = data_view[format_chr(f, o)](0, true);

                    if (d != null) {
                        d_data *= d;
                    }
                }

                console.log('DATA: ', d_data);
            }

            if (n == 'TIMESTAMP') d_data += 946684800;

            part_data[n] = d_data;

        }

        if (Object.keys(part_data).length !== 0) good_data.push(part_data);
    }

    if (events.length) good_data.push({'EVENTS': events});
    return good_data;
}

function hex_decoder(data) {
    // data - Data to be decrypted
    // return decrypted data
    if (data.slice(0, 2) == '77'){
        return meter_decode(data);
    } else if (data.slice(0, 2) == '78') {
        return arzamas.decoder(data);
    }
}
