# -*- coding: utf-8 -*-
from datetime import datetime
import struct


def dt_from_ts(ts):
    return datetime.fromtimestamp(int(ts))


def us1_parse(x):

    def parse_payload(p):
        # DIF Data Information Field
        # VIF Value Information Field
        p = p.decode('hex')

        res = {
            # 'vol_dif_byte': struct.unpack('B', p[0:1])[0],
            # 'vol_vif_byte': struct.unpack('B', p[1:2])[0],
            'volume': struct.unpack('<L', p[2:6])[0],
            # 'time_dif_byte': struct.unpack('B', p[6:7])[0],
            # 'time_vif_byte': struct.unpack('B', p[7:8])[0],
            'TIMESTAMP': dt_from_ts(struct.unpack('<L', p[8:12])[0]),
            # 'time_we_dif_byte': struct.unpack('B', p[12:13])[0],
            # 'time_we_vif_byte': struct.unpack('B', p[13:14])[0],
            'time_without_errors': dt_from_ts(struct.unpack('<L', p[14:18])[0]),
            # 'err_dif_byte': struct.unpack('B', p[18:19])[0],
            # 'err_vif_byte': struct.unpack('B', p[19:20])[0],
            # 'err_ext_vif_byte': struct.unpack('B', p[20:21])[0],
            'bit_field_ns': struct.unpack('B', p[21:22])[0],
        }
        return res

    data = {
        'sequence': struct.unpack('>H', x[:2])[0],
        'dev_type': struct.unpack('>H', x[2:4])[0],
        'dev_serial': struct.unpack('>Q', x[4:12])[0],
        'packet_count': struct.unpack('B', x[12:13])[0],
        'index': struct.unpack('B', x[13:14])[0],
        'archive': struct.unpack('B', x[14:15])[0],
        'data': x[15:].encode('hex'),
    }

    data['data'] = parse_payload(data['data'])
    return data


def aspd_05hr(x):
    data = {
        'sequence': struct.unpack('>H', x[:2])[0],
        'dev_type': struct.unpack('>H', x[2:4])[0],
        'dev_serial': struct.unpack('>Q', x[4:12])[0],
        'packet_count': struct.unpack('B', x[12:13])[0],
        'index': struct.unpack('B', x[13:14])[0],
        'archive': struct.unpack('B', x[14:15])[0],
        'data': x[15:].encode('hex'),
    }
    return data


def hourly_archive(x):
    x = x.decode('hex')

    fortlop = format(struct.unpack('B', x[55:56])[0], '08b')[::-1]
    flags_of_reactions_to_lack_of_power = {
        'pr1': int(fortlop[:2], 2),
        'pr2': int(fortlop[2:4], 2),
        'pr3': int(fortlop[4:6], 2),
    }

    data = {
        'device_address': struct.unpack('B', x[:1])[0],
        'func_number': struct.unpack('B', x[1:2])[0],
        'data_length': struct.unpack('B', x[2:3])[0],
        'TIMESTAMP': dt_from_ts(struct.unpack('>L', x[3:7])[0]),
        'TOTAE_F32[0]': struct.unpack('>L', x[7:11])[0] * 0.001,  # consumed_heat_w4
        'TOTAE_F32[1]': struct.unpack('>L', x[11:15])[0] * 0.001,  # consumed_heat_w5
        'TOTAE_F32[2]': struct.unpack('>L', x[15:19])[0] * 0.001,  # consumed_heat_w6
        'VOLUME[0]': struct.unpack('>L', x[19:23])[0] * 0.001,  # mass_or_volume_ch1
        'VOLUME[1]': struct.unpack('>L', x[23:27])[0] * 0.001,  # mass_or_volume_ch2
        'VOLUME[2]': struct.unpack('>L', x[27:31])[0] * 0.001,  # mass_or_volume_ch3
        'TEMP_IN[0]': struct.unpack('>h', x[31:33])[0] * 0.01,  # average_temperature_per_interval_1
        'TEMP_IN[1]': struct.unpack('>h', x[33:35])[0] * 0.01,  # average_temperature_per_interval_2
        'TEMP_IN[2]': struct.unpack('>h', x[35:37])[0] * 0.01,  # average_temperature_per_interval_3
        'status_word_per_interval': [TS_STATUS[b[0]] for b in enumerate(format(struct.unpack('H', x[37:39])[0], '016b')[::-1]) if b[1] == '1'],
        'total_operating_time': struct.unpack('>L', x[39:43])[0],
        'total_downtime': struct.unpack('>L', x[43:47])[0],
        'action_time_per_interval_ns1': struct.unpack('B', x[47:48])[0],
        'action_time_per_interval_ns2': struct.unpack('B', x[48:49])[0],
        'action_time_per_interval_ns3': struct.unpack('B', x[49:50])[0],
        'action_time_per_interval_ns4': struct.unpack('B', x[50:51])[0],
        'action_time_per_interval_ns5': struct.unpack('B', x[51:52])[0],
        'DURAT2[0]': struct.unpack('B', x[52:53])[0],  # power_off_time_per_interval_pr1
        'DURAT2[1]': struct.unpack('B', x[53:54])[0],  # power_off_time_per_interval_pr2
        'DURAT2[2]': struct.unpack('B', x[54:55])[0],  # power_off_time_per_interval_pr3
        'flags_of_reactions_to_lack_of_power': flags_of_reactions_to_lack_of_power,
    }

    return data


TS_STATUS = {
    0: 'NS1',
    1: 'NS2',
    2: 'NS3',
    3: 'NS4',
    4: 'NS5',
    5: 'Power failure PR1',
    6: 'Power failure PR2',
    7: 'Power failure PR3',
    8: 'Frequency overrun PR1',
    9: 'Frequency overrun PR2',
    10: 'Frequency overrun PR3',
    11: 0,
    12: 'Failure PT1',
    13: 'Failure PT2',
    14: 'Failure PT3',
    15: 0,
}


def daily_monthly_archive(x):
    x = x.decode('hex')

    data = {
        'device_address': struct.unpack('B', x[:1])[0],
        'func_number': struct.unpack('B', x[1:2])[0],
        'data_length': struct.unpack('B', x[2:3])[0],
        'TIMESTAMP': dt_from_ts(struct.unpack('>L', x[3:7])[0]),
        'TOTAE_F32[0]': struct.unpack('>L', x[7:11])[0] * 0.001,  # consumed_heat_w4
        'TOTAE_F32[1]': struct.unpack('>L', x[11:15])[0] * 0.001,  # consumed_heat_w5
        'TOTAE_F32[2]': struct.unpack('>L', x[15:19])[0] * 0.001,  # consumed_heat_w6
        'VOLUME[0]': struct.unpack('>L', x[19:23])[0] * 0.001,  # mass_or_volume_ch1
        'VOLUME[1]': struct.unpack('>L', x[23:27])[0] * 0.001,  # mass_or_volume_ch2
        'VOLUME[2]': struct.unpack('>L', x[27:31])[0] * 0.001,  # mass_or_volume_ch3
        'TEMP_IN[0]': struct.unpack('>h', x[31:33])[0] * 0.01,  # average_temperature_per_interval_1
        'TEMP_IN[1]': struct.unpack('>h', x[33:35])[0] * 0.01,  # average_temperature_per_interval_2
        'TEMP_IN[2]': struct.unpack('>h', x[35:37])[0] * 0.01,  # average_temperature_per_interval_3
        'status_word_per_interval': [TS_STATUS[b[0]] for b in enumerate(format(struct.unpack('H', x[37:39])[0], '016b')[::-1]) if b[1] == '1'],
        'total_operating_time': struct.unpack('>L', x[39:43])[0],
        'total_downtime': struct.unpack('>L', x[43:47])[0],
        'action_time_per_interval_ns1': struct.unpack('B', x[47:48])[0],
        'action_time_per_interval_ns2': struct.unpack('B', x[48:49])[0],
        'action_time_per_interval_ns3': struct.unpack('B', x[49:50])[0],
        'action_time_per_interval_ns4': struct.unpack('B', x[50:51])[0],
        'action_time_per_interval_ns5': struct.unpack('B', x[51:52])[0],
        'DURAT2[0]': struct.unpack('B', x[52:53])[0],  # power_off_time_per_interval_pr1
        'DURAT2[1]': struct.unpack('B', x[53:54])[0],  # power_off_time_per_interval_pr2
        'DURAT2[2]': struct.unpack('B', x[54:55])[0],  # power_off_time_per_interval_pr3
    }

    return data


def archive_parse(archive, data):
    arc_parser = {
        0: hourly_archive,
        1: daily_monthly_archive,
        2: daily_monthly_archive,
    }
    return arc_parser[archive](data)


def parser(x):
    x = x.decode('hex')
    dev_type = struct.unpack('>H', x[2:4])[0]

    data = {}

    # US1
    if dev_type == 115:
        data = us1_parse(x)
    # ASPD
    elif dev_type == 41:
        data = aspd_05hr(x)
    
    return data
