# -*- coding: utf-8 -*-
import struct
from datetime import datetime
import pytz  # $ pip install tzlocal


debug = False
convert_timestamp = True

DEFAULT_TIME_OFFSET = 946684800


def log(*args):
    if debug:
        print ' '.join([str(x) for x in args])


def lhex(h):
    # swap hex to little endian
    return ''.join(list(reversed(h.decode('hex')))).encode('hex')


def dt_from_ts(ts):
    return datetime.fromtimestamp(int(ts), pytz.utc)


def multipl(val, factor, n=2):
    res = val * factor
    if isinstance(res, float):
        res = round(res, n)
    return res


def parse_regular(payload_data):
    REGULAR_COUNTER_SIZE = 3
    REGULAR_FLAGS_SIZE = 1
    REGULAR_FLAGS = {
      0: u"Канал 1 используется",
      1: u"Канал 2 используется",
      2: u"Канал 3 используется",
      3: u"Канал 1, 32 бита (24, если 0)",
      4: u"Канал 2, 32 бита (24, если 0)",
      5: u"Канал 3, 32 бита (24, если 0)"
    }

    log('-'*20)

    byte_from = 0

    flags_hex = payload_data[byte_from:byte_from + REGULAR_FLAGS_SIZE].encode('hex')
    byte_from += REGULAR_FLAGS_SIZE
    flags = {i: REGULAR_FLAGS.get(i) for i, f in enumerate(format(int(lhex(flags_hex), 16), '08b')[::-1]) if f == '1'}
    log('flags', flags_hex)
    for x in flags:
        log(REGULAR_FLAGS[x])

    timestamp_hex = payload_data[byte_from:byte_from + 4].encode('hex')
    byte_from += 4
    timestamp = struct.unpack('<I', timestamp_hex.decode('hex'))[0] + 946684800
    if convert_timestamp:
        timestamp = dt_from_ts(timestamp)
    log('timestamp', timestamp, timestamp_hex)

    values = {}

    for k in range(3):
        if k not in (flags.keys()):
            continue
        
        counter_num = k + 1
        counter_size = REGULAR_COUNTER_SIZE

        if k + 3 in (flags.keys()):
            counter_size += 1

        byte_to = byte_from + counter_size

        counter_hex = payload_data[byte_from:byte_to].encode('hex')

        if counter_size == 3:
            counter_hex += '00'

        byte_from = byte_to

        counter = struct.unpack('<i', counter_hex.decode('hex'))[0]

        log('counter%s' % counter_num, counter, counter_hex)
        values['COUNTER%s' % counter_num] = counter

    payload_value = {'TIMESTAMP': timestamp}
    payload_value.update(values)
    payload_data = payload_data[byte_from:]
    return payload_value, payload_data


def get_bits(p_in, pos, size):
    return int(''.join([format(b, '08b')[::-1] for b in struct.unpack('B' * len(p_in), p_in)])[pos:pos + size][::-1], 2)


def parse_archive(payload_data, time_offset=DEFAULT_TIME_OFFSET):
    ARCHIVE_INIT_VAL_SIZE = 4
    ARCHIVE_FLAGS_SIZE = 2

    ARCHIVE_N_POS = 0
    ARCHIVE_N_MASK = 0x1f
    ARCHIVE_N_DESCR = u"Кол-во БИТ на diff ( [0..31]+1 )"
    ARCHIVE_DIV10_POS = 5
    ARCHIVE_DIV10_MASK = 0x1
    ARCHIVE_DIV10_DESCR = u"10 DIFF"
    ARCHIVE_RLE_POS = 6
    ARCHIVE_RLE_MASK = 0x1
    ARCHIVE_RLE_DESCR = u"BIT RLE COMPRESSION"
    ARCHIVE_SIGNED_POS = 7
    ARCHIVE_SIGNED_MASK = 0x1
    ARCHIVE_SIGNED_DESCR = u"DIFF SIGNED"
    ARCHIVE_REC_NUM_POS = 8
    ARCHIVE_REC_NUM_MASK = 0x3f
    ARCHIVE_REC_NUM_DESCR = u"Кол-во записей (до 64)"
    ARCHIVE_RESOL_POS = 14
    ARCHIVE_RESOL_MASK = 0x1
    ARCHIVE_RESOL_DESCR = u"Individual bit resolution"
    ARCHIVE_ALL_DIFF_ZERO_POS = 15
    ARCHIVE_ALL_DIFF_ZERO_MASK = 0x1
    ARCHIVE_ALL_DIFF_ZERO_DESCR = u"All diff zero"

    log('-'*20)
    
    byte_from = 0
    
    timestamp_hex = payload_data[byte_from:byte_from + 4].encode('hex')
    byte_from += 4
    timestamp = struct.unpack('I', timestamp_hex.decode('hex'))[0] + time_offset
    if convert_timestamp:
        timestamp = dt_from_ts(timestamp)
    log('timestamp', timestamp, timestamp_hex)

    init_val_hex = payload_data[byte_from:byte_from + ARCHIVE_INIT_VAL_SIZE].encode('hex')
    byte_from += ARCHIVE_INIT_VAL_SIZE
    init_val = struct.unpack('i', init_val_hex.decode('hex'))[0]
    log('init_val', init_val, init_val_hex)

    flags_hex = payload_data[byte_from:byte_from + ARCHIVE_FLAGS_SIZE].encode('hex')
    byte_from += ARCHIVE_FLAGS_SIZE
    flags = struct.unpack('H', flags_hex.decode('hex'))[0]
    log('flags', flags, flags_hex, format(flags, '016b'))

    n = (flags & ARCHIVE_N_MASK << ARCHIVE_N_POS) >> ARCHIVE_N_POS
    div10 = (flags & ARCHIVE_DIV10_MASK << ARCHIVE_DIV10_POS) >> ARCHIVE_DIV10_POS
    rle = (flags & ARCHIVE_RLE_MASK << ARCHIVE_RLE_POS) >> ARCHIVE_RLE_POS
    signed = (flags & ARCHIVE_SIGNED_MASK << ARCHIVE_SIGNED_POS) >> ARCHIVE_SIGNED_POS
    rec_num = (flags & ARCHIVE_REC_NUM_MASK << ARCHIVE_REC_NUM_POS) >> ARCHIVE_REC_NUM_POS
    resol = (flags & ARCHIVE_RESOL_MASK << ARCHIVE_RESOL_POS) >> ARCHIVE_RESOL_POS
    all_diff_zero = (flags & ARCHIVE_ALL_DIFF_ZERO_MASK << ARCHIVE_ALL_DIFF_ZERO_POS) >> ARCHIVE_ALL_DIFF_ZERO_POS

    log('n:', n)
    log('div10:', div10)
    log('rle:', rle)
    log('signed:', signed)
    log('rec_num:', rec_num)
    log('resol:', resol)

    data_byte = payload_data[byte_from:]

    prev_value = init_val
    individual = resol
    bypass_individal_bit = False
    bit_num = 0
    
    values = [prev_value]
    while rec_num:
        if all_diff_zero:
            values.append(prev_value)
            rec_num -= 1
            continue

        if individual and not bypass_individal_bit:
            tmp = get_bits(data_byte, bit_num, 1)
            bit_num += 1

            if tmp != 0:
                n = get_bits(data_byte, bit_num, 5)
                bit_num += 5

        bypass_individal_bit = False

        if rle:
            tmp = get_bits(data_byte, bit_num, 1)
            bit_num += 1

            if tmp != 0:
                values.append(prev_value)
                rec_num -= 1

                tmp = get_bits(data_byte, bit_num, 5)
                bit_num += 5

                if rec_num < tmp:
                    return None, None

                for t in xrange(tmp):
                    values.append(prev_value)
                    rec_num -= 1

                bypass_individal_bit = True
                continue

        diff = get_bits(data_byte, bit_num, n + 1)
        bit_num += n + 1

        if signed and n < 31:
            mask = 1L << n
            diff = (diff ^ mask) - mask

        prev_value += diff
        values.append(prev_value)
        rec_num -= 1

    byte_from += bit_num / 8
    if bit_num % 8:
        byte_from += 1

    payload_value = {'timestamp': timestamp, 'values': values}
    payload_data = payload_data[byte_from:]
    return payload_value, payload_data


def parse_archive_hour_offset(payload_data):
    return parse_archive(payload_data, time_offset=DEFAULT_TIME_OFFSET+3600)


def parse_tamper(payload_data):
    TAMPER_EV_COUNT_MASK = 0x3f
    TAMPER_EVENT_SIZE = 1
    TAMPER_EVENT = {
        0: u"Геркон замкнут",
        1: u"Крышка открыта",
        4: u"Геркон разомкнут",
        5: u"Крышка закрыта"
    }
    TAMPER_TS_DELTA_SIZE = 2

    byte_from = 0

    log('-'*20)

    ev_count_hex = payload_data[byte_from:1].encode('hex')
    byte_from += 1
    ev_count = int(ev_count_hex, 16) & TAMPER_EV_COUNT_MASK
    log('ev_count', ev_count, ev_count_hex)

    timestamp_hex = payload_data[byte_from:byte_from + 4].encode('hex')
    byte_from += 4
    timestamp = struct.unpack('I', timestamp_hex.decode('hex'))[0] + 946684800
    log('timestamp', dt_from_ts(timestamp), timestamp_hex)

    events = []
    if ev_count > 0:
        for event_num in xrange(ev_count):
            ts_delta = dt_from_ts(timestamp)

            if event_num > 0:
                ts_delta_hex = payload_data[byte_from:byte_from + TAMPER_TS_DELTA_SIZE].encode('hex')
                byte_from += TAMPER_TS_DELTA_SIZE
                ts_delta = struct.unpack('H', ts_delta_hex.decode('hex'))[0] + timestamp
                if convert_timestamp:
                    ts_delta = dt_from_ts(ts_delta)
                log('ts_delta', ts_delta, ts_delta_hex)

            event_hex = payload_data[byte_from:byte_from + TAMPER_EVENT_SIZE].encode('hex')
            byte_from += TAMPER_EVENT_SIZE
            event = {i: TAMPER_EVENT[i] for i, e in enumerate(format(int(event_hex, 16), '08b')[::-1]) if e == '1'}
            log('event', event, event_hex)

            events.append({'events': event, 'timestamp': ts_delta})

    payload_value = {'event_count': ev_count, 'events': events}
    payload_data = payload_data[byte_from:]

    return payload_value, payload_data


def parse_service_data(payload_data):
    SERVICE_TYPE = {
        0: u"Информация о версии FW и причина перезагрузки, этот payload отправляется при включении или перезагрузке",
        1: u"Информация о времени работы в режиме передачи, отправляется по запросу",
        2: u"CFG RESULT, отправляется как ответ на входящий пакет с конфигурацией",
        3: u"CFG_ID, отправляется как ответ на входящий пакет с конфигурацией, которая имеет идентификатор (версию)",
        4: u"CUR FW, отправляется как ответ (состояние и запрос следующего блока) на входящий пакет обновления FW",
        5: u"Значение напряжения батареи и температура, отправляется по запросу",
        6: u"RSSI_SNR_INFO, информация об RSSI и SNR для последнеого принятого пакета"
    }

    GIT_SHA1_SIZE = 4
    FW_VER_SIZE = 4
    FW_VER_RESET_SOURCE = {
        0: u"Firewall",
        1: u"Options Clrd",
        2: u"Reset Pin",
        3: u"POR",
        4: u"By software",
        5: u"I-watchdog",
        6: u"W-watchdog",
        7: u"low power"
    }
    FW_VER_ADDR_SIZE = 3
    FW_VER_ADDR_BIT23_POS = 23
    FW_VER_ADDR_BIT23_MASK = 0x1

    FW_VER_ADDR_POS = 0
    FW_VER_ADDR_MASK = 0x7FFFFF

    CFG_RESULT_SIZE = 2

    CFG_ID_VER_BLOCK_ID_SIZE = 1
    CFG_ID_SIZE = 1

    CFG_PL_HEADER_FORMAT = {
        0: {"name": "SYSTEM_CFG", "descr": u"Время, перезагрузка и т.д."},
        1: {"name": "SCHEDULERS", "descr": u"Планировщики (циклические и др., которые не относятся ко входам)"},
        2: {"name": "DI_CFG", "descr": u"Конфиг. дискретных/счетных входов"},
        3: {"name": "DO_CFG", "descr": u"Конфиг. дискретных/импульсных выходов"},
        4: {"name": "AI_CFG", "descr": u"Конфиг. аналоговых входов"},
        5: {"name": "AO_CFG", "descr": u"Конфиг. аналоговых выходов"},
        6: {"name": "SERIAL_CFG", "descr": u"Конфиг. устройств на последовательном порту (включая RS485)"},
        7: {"name": "ARCHIVES", "descr": u"Конфигурация архивирования"},
        8: {"name": "LIGHT_CONTROL", "descr": u"Конфиг. управления освещением"},
        16: {"name": "REQUEST_ARC", "descr": u"Запрос данных из архивов"}
    }

    RSSI_INFO_SIZE = 1
    SNR_INFO_SIZE = 1

    byte_from = 0

    log('-'*20)

    service_type = int(payload_data[byte_from:1].encode('hex'), 16)
    byte_from += 1
    log('service_type', service_type, SERVICE_TYPE[service_type])

    service_value = None
    if service_type == 0:
        git_sha1 = payload_data[byte_from:byte_from + GIT_SHA1_SIZE].encode('hex')
        byte_from += GIT_SHA1_SIZE
        log('git_sha1:', git_sha1)
        
        fw_ver = payload_data[byte_from:byte_from + FW_VER_SIZE].encode('hex')
        byte_from += FW_VER_SIZE
        log('fw_ver:', fw_ver)

        reset_source_hex = payload_data[byte_from:byte_from + 1].encode('hex')
        byte_from += 1
        reset_source_bin = format(int(reset_source_hex, 16), '08b')[::-1]
        reset_source = {i: FW_VER_RESET_SOURCE[i] for i in xrange(len(reset_source_bin)) if reset_source_bin[i] == '1'}
        log('reset_source:', reset_source, reset_source_hex, reset_source_bin)
        
        addr_hex = payload_data[byte_from:byte_from + FW_VER_ADDR_SIZE].encode('hex')
        byte_from += FW_VER_ADDR_SIZE
        addr_bit23 = (int(lhex(addr_hex), 16) & FW_VER_ADDR_BIT23_MASK << FW_VER_ADDR_BIT23_POS) >> FW_VER_ADDR_BIT23_POS
        if addr_bit23 == 0:
            addr_bit23 = {addr_bit23: 'general'}
        else:
            addr_bit23 = {addr_bit23: 'HardFault'}
        log('addr_bit23:', addr_bit23, addr_hex)

        address = (int(lhex(addr_hex), 16) & FW_VER_ADDR_MASK << FW_VER_ADDR_POS) >> FW_VER_ADDR_POS
        log('address:', address)

        service_value = {
            'git_sha1': git_sha1,
            'fw_ver': fw_ver,
            'reset_source': reset_source,
            'address': address
        }

    elif service_type == 2:
        data_hex = payload_data[byte_from:byte_from + CFG_RESULT_SIZE].encode('hex')
        byte_from += CFG_RESULT_SIZE
        data = list(format(int(lhex(data_hex), 16), '016b')[::-1])
        log('data:', data, data_hex)

        service_value = {
            'data': data
        }

    elif service_type == 3:
        block_id_hex = payload_data[byte_from:byte_from + CFG_ID_VER_BLOCK_ID_SIZE].encode('hex')
        byte_from += CFG_ID_VER_BLOCK_ID_SIZE
        block_id = int(lhex(block_id_hex), 16)
        log('block_id:', block_id, block_id_hex)

        cfg_id_hex = payload_data[byte_from:byte_from + CFG_ID_SIZE].encode('hex')
        byte_from += CFG_ID_SIZE
        cfg_id = int(lhex(cfg_id_hex), 16)
        log('cfg_id:', cfg_id, cfg_id_hex)

        service_value = {
            'block_id': block_id,
            'format': CFG_PL_HEADER_FORMAT[block_id]['name'],
            'cfg_id': cfg_id
        }

    elif service_type == 6:
        rssi_hex = payload_data[byte_from:byte_from + RSSI_INFO_SIZE].encode('hex')
        byte_from += RSSI_INFO_SIZE
        rssi = int(lhex(rssi_hex), 16)
        log('rssi:', rssi, rssi_hex)

        snr_hex = payload_data[byte_from:byte_from + SNR_INFO_SIZE].encode('hex')
        byte_from += SNR_INFO_SIZE
        snr = int(lhex(snr_hex), 16)
        log('snr:', snr, snr_hex)

        service_value = {
            'rssi': rssi,
            'snr': snr
        }

    payload_value = {service_type: service_value, 'descr': SERVICE_TYPE[service_type]}
    payload_data = payload_data[byte_from:]

    return payload_value, payload_data


def parse_various(payload_data):
    VARIOUS_FLAGS_SIZE = 1

    CHANNEL_NUMBER_POS = 0
    CHANNEL_NUMBER_MASK = 0x7
    CHANNEL_NUMBER_DESCR = u'Channel number'
    PARAMETERS_COUNT_POS = 3
    PARAMETERS_COUNT_MASK = 0xf
    PARAMETERS_COUNT_DESCR = u'Parameters count'
    BIT16_IDS_POS = 7
    BIT16_IDS_MASK = 0x1
    BIT16_IDS_DESCR = u'16 bit IDs'

    PARAM_IDS = {
        0: {'size': 8, 'name': 'GNSS Simple', 'descr': u'Координаты GNSS, сокращенный вариант'},
        1: {'size': 0, 'name': 'GNSS Extended', 'descr': u'Координаты GNSS, расширенный вариант'},
        2: {'size': 2, 'name': 'Speed', 'descr': u'Скорость'},
        3: {'size': 2, 'name': 'Altitude', 'descr': u'Высота'},
        4: {'size': 2, 'name': 'Azimuth', 'descr': u'Азимут'},
        5: {'size': 2, 'name': 'Temperature', 'descr': u'Температура'},
        6: {'size': 2, 'name': 'Pressure', 'descr': u'Давление'},
        7: {'size': 1, 'name': 'Humidity', 'descr': u'Влажность'},
        8: {'size': 2, 'name': 'Illumination', 'descr': u'Попугаи, uint16_t Освещенность'},
        9: {'size': 2, 'name': 'CO2', 'descr': u'Углекислый газ, ppm'},
        10: {'size': 2, 'name': 'Phase', 'descr': u'Фаза'},
        11: {'size': 2, 'name': 'Frequency', 'descr': u'Частота'},
        12: {'size': 2, 'name': 'Voltage0', 'descr': u'В, множитель x10 Напряжение, фаза A'},
        13: {'size': 2, 'name': 'Voltage1', 'descr': u'В, множитель x10 Напряжение, фаза B'},
        14: {'size': 2, 'name': 'Voltage2', 'descr': u'В, множитель x10 Напряжение, фаза C'},
        15: {'size': 2, 'name': 'Current0', 'descr': u'мА  Ток, фаза A'},
        16: {'size': 2, 'name': 'Current1', 'descr': u'мА  Ток, фаза B'},
        17: {'size': 2, 'name': 'Current2', 'descr': u'мА  Ток, фаза C'},
    }

    log('-'*20)
    byte_from = 0

    timestamp_hex = payload_data[byte_from:byte_from + 4].encode('hex')
    byte_from += 4
    timestamp = int(lhex(timestamp_hex), 16) + 946684800
    if convert_timestamp:
        timestamp = dt_from_ts(timestamp)
    log('timestamp', timestamp, timestamp_hex)

    flags_hex = payload_data[byte_from:byte_from + VARIOUS_FLAGS_SIZE].encode('hex')
    byte_from += VARIOUS_FLAGS_SIZE
    flags_int = int(flags_hex, 16)
    
    chanel_num = (flags_int & CHANNEL_NUMBER_MASK << CHANNEL_NUMBER_POS) >> CHANNEL_NUMBER_POS
    params_count = (flags_int & PARAMETERS_COUNT_MASK << PARAMETERS_COUNT_POS) >> PARAMETERS_COUNT_POS
    b16_ids = (flags_int & BIT16_IDS_MASK << BIT16_IDS_POS) >> BIT16_IDS_POS
    
    flags = {
        CHANNEL_NUMBER_DESCR: chanel_num,
        PARAMETERS_COUNT_DESCR: params_count,
        BIT16_IDS_DESCR: b16_ids
    }

    log('flags', flags_hex)
    for x in flags:
        log(x, flags[x])

    params = {}
    if params_count:
        for p in xrange(params_count):
            id_len = 1
            if b16_ids == '1':
                id_len += 1
            param_id_hex = payload_data[byte_from:byte_from + id_len].encode('hex')
            byte_from += id_len
            param_id = int(lhex(param_id_hex), 16)
            param = PARAM_IDS[param_id]
            log('param', param_id, param, param_id_hex)

            factor = 1
            if param_id in [12, 13, 14]:
                factor = 10

            value_hex = payload_data[byte_from:byte_from + param['size']].encode('hex')
            byte_from += param['size']
            value = int(lhex(value_hex), 16) / factor
            
            # Parse GNSS Simple
            if param_id == 0:
                latitude_val = value & 0xFFFFFFFF
                latitude = (latitude_val >> 23) + (latitude_val & 0x7FFFFF) / 10000.0 / 60

                longitude_val = (value & 0xFFFFFFFF << 32) >> 32
                longitude = (longitude_val >> 23) + (longitude_val & 0x7FFFFF) / 10000.0 / 60
                
                value = {'latitude': latitude, 'longitude': longitude}

            log('value', value, value_hex)

            params[param['name']] = value

    payload_value = {'TIMESTAMP': timestamp, 'FLAGS': flags, 'PARAMS': params}
    payload_data = payload_data[byte_from:]
    return payload_value, payload_data


def parse_schedule_lighting(data):
    PROP_MODE = {
        0: u'Запись не используется',
        1: u'Указанное в timeoffset з',
        2: u'Относительно восхода',
        3: u'Относительно заката',
    }

    PROP_COMMAND = {
        0: u'Отключить',
        1: u'Включить (фиксир. диммирование)',
        2: u'Включить (димм. от освещенности)',
    }

    DATE_FORMAT = {
        0: u'ONLY DOW',
        1: u'WEEKNO / DOW',
        2: u'MONTH / DOW',
        3: u'EXACT DATE',
    }

    MONTHS = {
        52: u'весь январь',
        53: u'весь февраль',
        54: u'весь март',
        55: u'весь апрель',
        56: u'весь май',
        57: u'весь июнь',
        58: u'весь июль',
        59: u'весь август',
        60: u'весь сентябрь',
        61: u'весь октябрь',
        62: u'весь ноябрь',
        63: u'весь декабрь',
    }

    DOW = {
        0: u'понедельник',
        1: u'вторник',
        2: u'среда',
        3: u'четверг',
        4: u'пятница',
        5: u'суббота',
        6: u'воскресенье',
    }

    prop = struct.unpack('B', data[0])[0]
    dim = struct.unpack('B', data[1])[0]
    # rfu = struct.unpack('B', data[2])[0]
    # rfu = struct.unpack('B', data[3])[0]
    week_month = struct.unpack('B', data[4])[0]
    day_of_week = struct.unpack('B', data[5])[0]

    prop_mode = prop & 0x3
    prop_mode_descr = PROP_MODE.get(prop_mode)
    prop_ifnogeo = (prop & 0x1 << 2) >> 2
    prop_priority = (prop & 0x3 << 3) >> 3
    prop_command = (prop & 0x7 << 5) >> 5
    prop_command_descr = PROP_COMMAND.get(prop_command)

    dimming = dim & 0x7f

    date_format = week_month & 0x3
    date_format_descr = DATE_FORMAT.get(date_format)

    week_month_val = (week_month & 0x3f << 2) >> 2

    week_no = {}
    month = {}
    if date_format in [1, 2]:
        if week_month_val <= 51:
            week_no = {'week_no': week_month_val}
        else:
            month = {'month': week_month_val}
            month_descr = MONTHS.get(month)

    dow = {}
    date = {}
    if date_format < 3:
        dow = format(day_of_week & 0x7f, '08b')[:0:-1]
        dow = [int(d) for d in dow]
        dow_descr = [DOW.get(d) for d in dow]
        dow = {'day_of_week': dow}
    else:
        date = {'date': day_of_week & 0xf}

    high_bit = '01' if day_of_week > 127 else '00'
    time_offset_hex = high_bit + data[6:][::-1].encode('hex')
    time_offset = int(time_offset_hex, 16)

    if prop_mode in [2, 3] and time_offset > 65535:
        time_offset = (131072 - time_offset) * -1

    scheduler = {
        'prop_mode': prop_mode,
        'prop_ifnogeo': prop_ifnogeo,
        'prop_priority': prop_priority,
        'prop_command': prop_command,
        'dimming': dimming,
        'date_format': date_format,
        'time_offset': time_offset,
    }

    scheduler.update(week_no)
    scheduler.update(month)
    scheduler.update(dow)
    scheduler.update(date)

    return scheduler


def parse_lighting(payload_data):
    LIGHTING_TIMESTAMP_SIZE = 4
    LIGHTING_FLAGS_SIZE = 1
    LIGHTING_PARAM_COUNT_POS = 3
    LIGHTING_PARAM_COUNT_MASK = 0xf
    LIGHTING_PARAM_COUNT_DESCR = u'Кол-во параметров + 1 (т.е. значение 0 = 1 параметр, значение 15 = 16 параметров )'

    PARAMS = {
        0: {'size': 8, 'name': 'position_simple', 'format': 'II', 'descr': u'packed data'},
        1: {'size': 1, 'name': 'temperature', 'format': 'b', 'descr': u'degC'},
        2: {'size': 2, 'name': 'temp_min_max', 'format': 'bb', 'descr': u'degC'},
        3: {'size': 2, 'name': 'phase', 'format': 'BH', 'descr': u'мкс относительно 1PPS на начало часа'},
        4: {'size': 2, 'name': 'frequency', 'format': 'H', 'descr': u'Гц, коэффициент 0,01', 'factor': 100},
        5: {'size': 2, 'name': 'voltage0', 'format': 'H', 'descr': u'В, коэффициент x0.1'},
        6: {'size': 2, 'name': 'current0', 'format': 'H', 'descr': u'мА'},
        7: {'size': 4, 'name': 'energy_A+', 'format': 'I', 'descr': u'кВт, коэффициент 0,01'},
        8: {'size': 4, 'name': 'energy_A-', 'format': 'I', 'descr': u'кВт, коэффициент 0,01'},
        9: {'size': 4, 'name': 'energy_R+', 'format': 'I', 'descr': u'кВАр, коэффициент 0,01'},
        10: {'size': 4, 'name': 'energy_R-', 'format': 'I', 'descr': u'кВАр, коэффициент 0,01'},
        21: {'size': 2, 'name': 'illumination', 'format': 'H', 'descr': u'попугаи, uint16_t'},
        22: {'size': 4, 'name': 'worktime_lighting', 'format': 'I', 'descr': u'сек'},
        23: {'size': 4, 'name': 'worktime_lamp', 'format': 'I', 'descr': u'сек'},
        32: {'size': 9, 'name': 'schedule_lighting', 'format': 'xxxxxxxbQ', 'descr': u'packed data'},
    }
    log('-'*20)

    byte_from = 0

    timestamp_hex = payload_data[byte_from:byte_from + LIGHTING_TIMESTAMP_SIZE].encode('hex')
    byte_from += LIGHTING_TIMESTAMP_SIZE
    timestamp = int(lhex(timestamp_hex), 16) + 946684800
    if convert_timestamp:
        timestamp = dt_from_ts(timestamp)
    log('timestamp', timestamp, timestamp_hex)

    flags_hex = payload_data[byte_from:byte_from + LIGHTING_FLAGS_SIZE].encode('hex')
    byte_from += LIGHTING_FLAGS_SIZE
    param_count = int(lhex(flags_hex), 16)
    param_count = (param_count & LIGHTING_PARAM_COUNT_MASK << LIGHTING_PARAM_COUNT_POS) >> LIGHTING_PARAM_COUNT_POS
    param_count += 1
    log('param_count', param_count)

    params = {}
    for i in xrange(param_count):
        param_id_hex = payload_data[byte_from:byte_from + 1].encode('hex')
        param_id = int(param_id_hex, 16)
        byte_from += 1
        param = PARAMS[param_id]

        value_hex = payload_data[byte_from:byte_from + param['size']].encode('hex')
        byte_from += param['size']
        
        log(param_id, value_hex, param['name'])
                
        if param_id == 32:
            cell_index = struct.unpack('B', value_hex.decode('hex')[0])[0]
            schedule = parse_schedule_lighting(value_hex.decode('hex')[1:])
            value = {'cell_index': cell_index, 'schedule': schedule}
        else:
            value = struct.unpack(param['format'], value_hex.decode('hex'))
        
        if len(param['format']) == 1:
            factor = param.get('factor', 1)
            value = value[0] / factor

        if param_id == 0:
            latitude_val = value[0] & 0xFFFFFFFF
            latitude = (latitude_val >> 23) + (latitude_val & 0x7FFFFF) / 10000.0 / 60

            longitude_val = value[1] & 0xFFFFFFFF
            longitude = (longitude_val >> 23) + (longitude_val & 0x7FFFFF) / 10000.0 / 60
            
            value = {'latitude': latitude, 'longitude': longitude}

        log('value', value, value_hex)

        params[param['name']] = value

    payload_value = {'TIMESTAMP': timestamp, 'PARAMS': params}
    payload_data = payload_data[byte_from:]
    return payload_value, payload_data


def parse_electric(payload):
    TIMESTAMP = {'SIZE': 4, 'POS': 0, 'FORMAT': 'I'}
    
    FLAGS = {'SIZE': 1, 'POS': 4, 'FORMAT': 'B'}
    DEVICE_NUMBER = {'MASK': 0x3, 'POS': 0, 'DESCR': 'Номер устройства на шине (0..3)'}
    PARAMS_COUNT = {'MASK': 0x1f, 'POS': 2, 'DESCR': 'Количество параметров + 1'}

    PARAM_HDR = {'SIZE': 1, 'POS': 5, 'FORMAT': 'B'}
    PARAM_ID = {'MASK': 0x7, 'POS': 0, 'DESCR': 'Параметр'}
    PARAM_ID_VALUES = {
        0: {'name': 'power', 'size': 2, 'format': 'H', 'factor': 10, 'template': 'CURP%(character)s%(phase)s', 'descr': 'Мощность, Вт'},
        1: {'name': 'full_power', 'size': 2, 'format': 'H', 'factor': 10, 'template': 'CURFP%(character)s%(phase)s', 'descr': 'Мощность полная, ВА'},
        2: {'name': 'energy', 'size': 4, 'format': 'I', 'factor': 10, 'template': 'TOTAE%(tariff)s_%(character)s%(direction)s', 'descr': 'Энергия, Вт*ч / ВАр*ч'},
        3: {'name': 'voltage', 'size': 2, 'format': 'H', 'factor': 0.1, 'template': 'CURV%(phase)s', 'descr': 'Напряжение, В'},
        4: {'name': 'amperage', 'size': 2, 'format': 'H', 'factor': 0.1, 'template': 'CURI%(phase)s', 'descr': 'Ток, А'},
        5: {'name': 'power_factor', 'size': 2, 'format': 'H', 'factor': 0.01, 'template': 'COS_PHI%(phase)s', 'descr': 'Коэффициент мощности (cos phi), %'},
        6: {'name': 'frequency', 'size': 2, 'format': 'H', 'factor': 0.01, 'template': 'FREQ', 'descr': 'Частота, Гц'},
    }
    DIRECTION = {'MASK': 0x1, 'POS': 3, 'DESCR': 'Направление'}
    DIRECTION_VALUES = {
        0: {'name': '+', 'descr': 'Потребление'},
        1: {'name': '-', 'descr': 'Генерация'},
    }

    CHARACTER = {'MASK': 0x1, 'POS': 4, 'DESCR': 'Характер'}
    CHARACTER_VALUES = {
        0: {'name': 'A', 'descr': 'Активная'},
        1: {'name': 'R', 'descr': 'Реактивная'},
    }

    PHASE_TARIFF = {'MASK': 0x7, 'POS': 5, 'DESCR': 'Фаза/Тариф'}
    PHASE_VALUES = {
        0: {'name': '', 'descr': 'однофазный / сумма или не применимо понятие "фаза"'},
        1: {'name': '_A', 'descr': 'A'},
        2: {'name': '_B', 'descr': 'B'},
        3: {'name': '_C', 'descr': 'C'},
        4: {'name': '_ABC', 'descr': 'ABC (в value передается массив из 3-х значений)'},
        5: {'name': 'n/a', 'descr': 'n/a'},
        6: {'name': 'n/a', 'descr': 'n/a'},
        7: {'name': 'n/a', 'descr': 'n/a'},
    }
    TARIFF_VALUES = {
        0: {'name': '', 'descr': 'Без тарифа'},
        1: {'name': '1', 'descr': 'Тариф 1'},
        2: {'name': '2', 'descr': 'Тариф 2'},
        3: {'name': '3', 'descr': 'Тариф 3'},
        4: {'name': '4', 'descr': 'Тариф 4'},
        5: {'name': '1/2/3/4', 'descr': 'T1/2/3/4 (в value передается массив из 4-х значений)'},
        6: {'name': 'n/a', 'descr': 'n/a'},
        7: {'name': 'n/a', 'descr': 'n/a'},
    }

    log('-'*20)

    timestamp = struct.unpack(TIMESTAMP['FORMAT'], payload[TIMESTAMP['POS']: TIMESTAMP['POS'] + TIMESTAMP['SIZE']])[0] + DEFAULT_TIME_OFFSET

    if convert_timestamp:
        timestamp = dt_from_ts(timestamp)

    log('timestamp', timestamp)

    flags = struct.unpack(FLAGS['FORMAT'], payload[FLAGS['POS']: FLAGS['POS'] + FLAGS['SIZE']])[0]
    device_number = (flags & DEVICE_NUMBER['MASK'] << DEVICE_NUMBER['POS']) >> DEVICE_NUMBER['POS']
    params_count = ((flags & PARAMS_COUNT['MASK'] << PARAMS_COUNT['POS']) >> PARAMS_COUNT['POS']) + 1

    log('flags', flags, '%02x' % flags)
    log('- device_number', device_number)
    log('- params_count', params_count)

    params = {}
    pos_offset = PARAM_HDR['POS']
    for i in xrange(params_count):
        param_hdr = struct.unpack(PARAM_HDR['FORMAT'], payload[pos_offset: pos_offset + PARAM_HDR['SIZE']])[0]
        
        pos_offset += PARAM_HDR['SIZE']
        
        param_id = (param_hdr & PARAM_ID['MASK'] << PARAM_ID['POS']) >> PARAM_ID['POS']
        param_id_value = PARAM_ID_VALUES[param_id]

        direction = (param_hdr & DIRECTION['MASK'] << DIRECTION['POS']) >> DIRECTION['POS']
        direction_value = DIRECTION_VALUES[direction]

        character = (param_hdr & CHARACTER['MASK'] << CHARACTER['POS']) >> CHARACTER['POS']
        character_value = CHARACTER_VALUES[character]

        phase_tariff = (param_hdr & PHASE_TARIFF['MASK'] << PHASE_TARIFF['POS']) >> PHASE_TARIFF['POS']

        if phase_tariff == 4 and 'phase' in param_id_value['template']:
            param_id_value['format'] *= 3
            param_id_value['size'] *= 3

        elif phase_tariff == 5 and 'tariff' in param_id_value['template']:
            param_id_value['format'] *= 4
            param_id_value['size'] *= 4

        value_trans = {
            'name': param_id_value['name'],
            'direction': direction_value['name'],
            'character': character_value['name'],
            'phase': PHASE_VALUES[phase_tariff]['name'],
            'tariff': TARIFF_VALUES[phase_tariff]['name'],
        }

        value_name = param_id_value['template'] % value_trans

        parsed_values = struct.unpack(param_id_value['format'], payload[pos_offset: pos_offset + param_id_value['size']])

        parsed_values = map(lambda v: multipl(v, param_id_value['factor']), parsed_values)
        
        if len(parsed_values) == 1:
            parsed_values = parsed_values[0]

        params[value_name] = parsed_values

        pos_offset += param_id_value['size']

        log('-'*5)
        log('param_hdr', param_hdr, '%02x' % param_hdr)
        log('param_id_value', param_id_value['name'], param_id_value['descr'])
        log('direction_value', direction_value['name'], direction_value['descr'])
        log('character_value', character_value['name'], character_value['descr'])
        log('phase', PHASE_VALUES[phase_tariff]['name'], PHASE_VALUES[phase_tariff]['descr'])
        log('tariff', TARIFF_VALUES[phase_tariff]['name'], TARIFF_VALUES[phase_tariff]['descr'])
        log('value', value_name, parsed_values)

    payload_value = {'TIMESTAMP': timestamp, 'PARAMS': params}
    payload = payload[pos_offset:]
    return payload_value, payload


def arzamas_decoder(data):
    PACKET_FLAGS = {
        0: u"Батарея разряжена",
        1: u"Низкая температура",
        2: u"Высокая температура",
        3: u"Геркон, состояние",
        4: u"Датчик вскрытия, состояние",
        5: u"Время устройства требует обновления"
    }

    PAYLOAD_FORMATS = {
        0: {'name': "regular", 'action': parse_regular},
        1: {'name': "archive, day, CNT0", 'action': parse_archive_hour_offset},
        2: {'name': "archive, day, CNT0", 'action': parse_archive},
        3: {'name': "archive, day, CNT1", 'action': parse_archive_hour_offset},
        4: {'name': "archive, day, CNT1", 'action': parse_archive},
        5: {'name': "archive, day, CNT2", 'action': parse_archive_hour_offset},
        6: {'name': "archive, day, CNT2", 'action': parse_archive},
        7: {'name': "tamper", 'action': parse_tamper},
        8: {'name': "service", 'action': parse_service_data},
        16: {'name': "lighting", 'action': parse_lighting},
        17: {'name': "electric", 'action': parse_electric},
        254: {'name': "various", 'action': parse_various},
    }

    hex_data = ['%s%s' % (x, y) for x, y in zip(data[::2], data[1::2])]
    byte_data = data.decode('hex')

    # PACKET HEADER
    log('DATA:', data)
    log('DATA LEN:', len(hex_data))

    log('\n---PACKET----------')

    packet_id = hex_data[0]
    packet_seq = struct.unpack('B', byte_data[1])[0]
    packet_flags = {i: PACKET_FLAGS.get(i) for i, f in enumerate(bin(struct.unpack('B', byte_data[2])[0])[2:][::-1]) if f == '1'}

    payload = byte_data[3:]

    log('packet_id:', packet_id)
    log('packet_seq:', packet_seq)

    packet_value = {'packet_flags': packet_flags}

    log('packet_flags:')
    for x in packet_flags:
        log(packet_flags[x])

    while payload:
        # PAYLOAD HEADER
        payload_format_num = struct.unpack('B', payload[0])[0]

        if payload_format_num not in PAYLOAD_FORMATS:
            return {'Error': ('Invalid data format %s' % payload_format_num)}

        payload_format = PAYLOAD_FORMATS[payload_format_num]
        payload_data = payload[1:]
        payload_value = None

        log('\n+++PAYLOAD+++++++++')
        log('payload_data', payload_data.encode('hex'))
        log('payload_format:', payload_format_num, payload_format['name'])

        payload_value, payload = payload_format['action'](payload_data)

        if payload_value:
            packet_value[payload_format['name']] = payload_value

    log('\npacket_value:', packet_value)
    return packet_value
