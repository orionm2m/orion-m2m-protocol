from datetime import datetime
import struct
import json
import pytz  # $ pip install tzlocal
from arzamas_decoder import arzamas_decoder
import vzlet
import os

debug = False
convert_timestamp = False


def log(*args):
    if debug:
        print ' '.join([str(x) for x in args])


def dt_from_ts(ts):
    return datetime.fromtimestamp(int(ts), pytz.utc)


def meter_decode(data):
    def fchr(f, o=None, s=None):
        f_chr = None
        param = ''

        fsize = s

        if o in ['<', '>']:
            param = o

        if f == 'char':
            f_chr = param + 'c'
        if f == 'signed char':
            f_chr = param + 'b'
        if f == 'short':
            f_chr = param + 'h'
            fsize = 2
        elif f == 'int':
            f_chr = param + 'i'
            fsize = 4
        elif f == 'long':
            f_chr = param + 'q'
        elif f == 'float':
            f_chr = param + 'f'
        elif f == 'double':
            f_chr = param + 'd'
        elif f == 'string':
            f_chr = param + '%ss' % (fsize if fsize else '')

        if s / fsize > 1:
            f_chr *= s / fsize
        return f_chr

    dirname = os.path.dirname(__file__)
    with open(os.path.join(dirname, 'byte_data_type.json')) as json_file:
        BDT = json.loads(json_file.read())

    data = data.decode('hex')

    payload = data[4:]

    good_data = []
    events = []

    log('Data', data.encode('hex'))

    while payload:
        p_len = struct.unpack('b', payload[1])[0]
        p_type = str(struct.unpack('B', payload[0])[0])
        p_data = payload[2:p_len]
        payload = payload[p_len:]

        log('----------')
        log('p_type', p_type)
        log('p_len', p_len)
        log('p_data', p_data.encode('hex'))

        data_part = {}
        if p_type not in BDT:
            log("Wrong data type")
            break

        while p_data:

            # IF EVENTS
            if p_type == '4':
                timestamp = struct.unpack('i', p_data[:4])[0] + 946684800
                source = BDT[p_type][str(struct.unpack('B', p_data[4])[0])]['name']
                code = BDT[p_type][str(struct.unpack('B', p_data[4])[0])]['code']

                log('-- EVENT')
                if code['format'] == 'bit':
                    size = code['size']
                    temp = '%0' + str(8 * size) + 'd'
                    bits = (temp % int(bin(int(p_data[5].encode('hex'), base=16))[2:]))

                    log('bits', bits)
                    offset = 0
                    code_res = []
                    for b in code['opt']:
                        log('b_name', b['name'])
                        log('b_size', b['size'])
                        res = int(bits[offset:offset + b['size']], 2)

                        log('res', res)

                        if 'values' in b:
                            res = b['values'][str(res)]
                        code_res.append({b['name']: res})
                        offset = b['size']

                        log('--')

                if convert_timestamp:
                    timestamp = dt_from_ts(timestamp)

                events.append({'TIMESTAMP': timestamp, 'SOURCE': source, 'CODE': code_res})
                p_data = p_data[6:p_len]
                continue

            d_type = str(struct.unpack('B', p_data[0])[0])
            log('+++++++++++')
            log('-- d_type', d_type)

            if d_type not in BDT[p_type]:
                break

            f = BDT[p_type][d_type]['format']
            s = BDT[p_type][d_type]['size']
            n = BDT[p_type][d_type]['name']
            o = BDT[p_type][d_type]['opt']
            d = BDT[p_type][d_type].get('discr', None)

            log('-- bytes: ', p_data[1:1+s].encode('hex'))
            log('-- size', s)
            log('-- name', n)
            log('-- discr', d)

            fp = fchr(f, o, s)

            log('-- format', f, '-', fp)
            
            if not f:
                # log("Data type has't format")
                # log(d_type, BDT[d_type])
                break

            if f == 'list':
                p_data = p_data[1:]
                d_data = []
                for val in o:
                    val_f = val['format']
                    val_s = val['size']
                    val_n = val['name']
                    val_o = val['opt']
                    val_d = val.get('discr', None)

                    val_fp = fchr(val_f, val_o, val_s)

                    log('----')
                    log('---- val_bytes: ', p_data[:val_s].encode('hex'))
                    log('---- val_format', val_f, '-', val_fp)
                    log('---- val_size', val_s)
                    log('---- val_name', val_n)

                    if val_f == 'bin':
                        if val_o == '<':
                            val_data = p_data[:val_s][::-1].encode('hex')
                        else:
                            val_data = p_data[:val_s].encode('hex')
                        d_data.append({val_n: val_data})

                    elif val_f == 'boolean':
                        val_v = val['values']
                        bits = bin(int(p_data[:val_s].encode('hex'), base=16))[2:]
                        bits = ('0'*8*val_s)[:8*val_s-len(bits)] + bits
                        log('---- bits', bits)
                        bits = bits[::-1]

                        val_data = {val_n: []}
                        for k in val_v:
                            if val_v[k] != 'RFU':
                                val_data[val_n].append({val_v[k]: int(bits[int(k)])})
                        d_data.append(val_data)

                    elif val_f == "ADDR":
                        if int(p_data[:val_s][::-1].encode('hex'), 16) & 0x00800000:
                            d_data.append({'GH': 'HardFault'})
                        else:
                            d_data.append({'GH': 'General'})
                        addr = (int(p_data[:val_s][::-1].encode('hex'), 16) & 0x007FFFFF) | 0x08000000
                        d_data.append({'Addr': addr})

                    elif val_f == 'number':
                        bits = bin(int(p_data[:val_s].encode('hex'), base=16))[2:]
                        bits = ('0'*8*val_s)[:8*val_s-len(bits)] + bits
                        log('---- bits', bits)
                        d_data.append({val_n: int(bits, 2)})

                    else:
                        val_data = struct.unpack(val_fp, p_data[:val_s])[0]
                        if val_d:
                            val_data *= val_d
                        d_data.append({val_n: val_data})

                    p_data = p_data[val_s:]

            else:
                a = ''  # Add null bytes
                if f == 'int' and s < 4:
                    a = '000000'.decode('hex')[4-s]

                d_data = list(struct.unpack(fp, p_data[1:1+s] + a))

                if d:
                    for i, res in enumerate(d_data):
                        d_data[i] *= d

                if len(d_data) == 1:
                    d_data = d_data[0]

                p_data = p_data[1+s:]

            if n == 'TIMESTAMP':
                d_data += 946684800

                if convert_timestamp:
                    d_data = dt_from_ts(d_data)

            log('DATA: ', d_data)
            data_part[n] = d_data

        if data_part:
            good_data.append(data_part)

    if events:
        good_data.append({'EVENTS': events})
    return good_data


def hex_decoder(data, meter_type=0):
    """
    data - Data to be decrypted
    meter_type - 0 Orion
                 1 Vzlet
    return decrypted data
    """
    if meter_type == 0:
        if data[:2] == '77':
            return meter_decode(data)
        elif data[:2] == '78':
            return arzamas_decoder(data)

    elif meter_type == 1:
        return vzlet.parser(data)
